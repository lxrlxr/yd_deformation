import json
import os
import os.path as osp
import queue
import threading
import cv2
import time
import yaml
import shutil
import sys
import numpy as np
import matplotlib.path as mpltPath

from abc import ABC, abstractmethod

from paho.mqtt import client as mqtt_client
from utils.config import GetConfig
from utils.logger import setup_logger, clear_data
from utils.cal_deformation import Analyse_image
from utils.ftps_image import get_image, upload_image

# 设置项目目录路径
dirPath = osp.dirname(osp.abspath(__file__))
dirPath = osp.dirname(dirPath)
sys.path.append(osp.join(dirPath, 'pixeltovoxel', 'lib'))

logger = setup_logger(project_name='deformation')


class Deformation:
    def __init__(self, config_name):
        """
        初始化 Deformation 对象
        参数:
            config_name (str): 配置文件名
        """
        try:
            self.cfg = self.load_config(config_name)
            self.lock = threading.RLock()
            self.queue = queue.Queue()
            self.mqtt_client = None

            if not self.cfg.TESTMODE:
                self.init_mqtt()

        except Exception:
            logger.error("初始化失败", exc_info=True)

    def load_config(self, config_name):
        """
        加载配置文件
        参数:
            config_name (str): 配置文件名
        返回:
            配置对象
        """
        try:
            config_path = osp.join(dirPath, 'config', config_name)
            return GetConfig(config_path)
        except Exception as e:
            logger.error(f"加载配置文件失败: {e}", exc_info=True)
            raise

    def init_mqtt(self):
        """
        初始化 MQTT 客户端
        """
        self.mqtt_client = self.connect_mqtt()
        if self.mqtt_client:
            self.mqtt_client.loop_start()
            threading.Thread(target=self.receive_msg).start()
        else:
            raise ConnectionError("MQTT服务器连接失败")

    def keep_loop(self):
        """
        保持 MQTT 循环连接
        """
        threading.Thread(target=self.receive_msg).start()
        timeout = time.time() + 8
        while True:
            logger.debug(f'保持循环连接, 时间:{time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())}')
            if time.time() > timeout:
                logger.debug("连接超时, 正在断开连接.")
                self.reconnect_mqtt()
                timeout = time.time() + 8
            time.sleep(1)


    def reconnect_mqtt(self):
        """
        重新连接MQTT服务器
        """
        try:
            if self.mqtt_client:
                self.mqtt_client.disconnect()
                self.mqtt_client.loop_stop()

            logger.debug("重新连接MQTT")
            self.mqtt_client = self.connect_mqtt()
            if self.mqtt_client:
                self.mqtt_client.loop_start()
                threading.Thread(target=self.receive_msg).start()
            else:
                raise ConnectionError("重新连接MQTT服务器失败")

        except Exception as e:
            logger.error(f"MQTT重新连接失败: {e}", exc_info=True)

    def connect_mqtt(self) -> mqtt_client.Client:
        """
        连接MQTT服务器
        返回:
            mqtt_client.Client: 成功连接时返回MQTT客户端对象，否则返回False
        """
        def on_connect(client, userdata, flags, rc):
            if rc != 0:
                logger.error(f"连接失败, 返回码 {rc}")

        def on_disconnect(client, userdata, rc):
            if rc != 0:
                logger.warning("意外断开连接。MQTT连接断开，将重启本程序MQTT")

        try:
            client = mqtt_client.Client(self.cfg.MQTT.NODENAME)
            client.username_pw_set(self.cfg.MQTT.USERNAME, self.cfg.MQTT.PASSWORD)
            client.on_connect = on_connect
            client.on_disconnect = on_disconnect
            client.connect(self.cfg.MQTT.HOST, self.cfg.MQTT.PORT, 60)
            logger.info(f"已连接到MQTT代理! 客户端ID: {self.cfg.MQTT.NODENAME}")
            return client
        except Exception as e:
            logger.error("MQTT客户端初始化和连接失败", exc_info=True)
            return False

    def load_message(self, camare_receive_msg):
        """
        将接收到的mqtt消息中的属性值存入本地变量
        参数:
            camare_receive_msg (dict): 接收到的mqtt消息
        返回:
            tuple: 包含消息各个属性的元组
        """
        try:
            image_id = camare_receive_msg["data"]["watchPointId"]
            ftps_ref_image = camare_receive_msg['data']['originalImage']
            ftps_new_image = camare_receive_msg['data']['filePath']
            image_area = json.loads(camare_receive_msg['data']['detectionArea'])
            image_area = [[int(x[0]), int(x[1])] for x in image_area]
            update_time = camare_receive_msg['data']['updateTime']
            config_param = json.loads(camare_receive_msg['data']['configParam'])
            taskCode = camare_receive_msg['data']['taskCode']
            dataCode = camare_receive_msg['data']['dataCode']
            robotId = camare_receive_msg['data']['robotId']
            stopPoint = camare_receive_msg['data']['stopPoint']
            presetId = camare_receive_msg['data']['preset']
            tagId = camare_receive_msg['data']['tagId']
            pointId = camare_receive_msg['data']['pointId']
            taskPatrolledId = camare_receive_msg['data']['taskPatrolledId']
            calibrationData = camare_receive_msg['data']['calibrationData']
            return True, image_id, ftps_ref_image, ftps_new_image, image_area, \
                update_time, config_param, taskCode, dataCode, robotId, presetId, tagId, pointId, taskPatrolledId, calibrationData, stopPoint
        except Exception:
            logger.error("将接收到的mqtt消息中的属性值存入本地变量失败", exc_info=True)
            return False, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None


    def refresh_param(self, config_param):
        """
        将mqtt消息中的参数配置写入内存
        参数:
            config_param (dict): mqtt消息中的参数配置
        返回:
            bool: 成功返回True，失败返回False
        """
        try:
            self.cfg.MODEL.DEVICE = config_param['MODEL.DEVICE']
            self.cfg.MODEL.RESIZE_W = int(config_param['MODEL.RESIZE_W'])
            self.cfg.MODEL.RESIZE_H = int(config_param['MODEL.RESIZE_H'])
            self.cfg.MODEL.SCALE = int(config_param['MODEL.SCALE'])

            self.cfg.MODEL.SUPERPOINT.NMS_RADIUS = int(config_param['MODEL.SUPERPOINT.NMS_RADIUS'])
            self.cfg.MODEL.SUPERPOINT.KEYPOINT_THRESHOLD = float(config_param['MODEL.SUPERPOINT.KEYPOINT_THRESHOLD'])
            self.cfg.MODEL.SUPERPOINT.MAX_KEYPOINTS = int(config_param['MODEL.SUPERPOINT.MAX_KEYPOINTS'])

            self.cfg.MODEL.SUPERGLUE.WEIGHTS = config_param['MODEL.SUPERGLUE.WEIGHTS']
            self.cfg.MODEL.SUPERGLUE.SINKHORN_ITERATIONS = int(config_param['MODEL.SUPERGLUE.SINKHORN_ITERATIONS'])
            self.cfg.MODEL.SUPERGLUE.MATCH_THRESHOLD = float(config_param['MODEL.SUPERGLUE.MATCH_THRESHOLD'])
            self.cfg.SEGMENTATION.EXTEND = int(config_param['SEGMENTATION.EXTEND'])

            self.cfg.CLUSTER.ERRORCOUNT = int(config_param['CLUSTER.ERRORCOUNT'])
            self.cfg.CLUSTER.ERRORTHRESH = float(config_param['CLUSTER.ERRORTHRESH'])
            self.cfg.THRESHOLD = float(config_param['THRESHOLD'])

            self.cfg.VOXEL.EXTEND = int(config_param['VOXEL.EXTEND'])
            self.cfg.VOXEL.THRESHOLD = float(config_param['VOXEL.THRESHOLD'])
            self.cfg.VOXEL.ERODE_PIXELS = int(config_param['VOXEL.ERODE_PIXELS'])
            return True
        except Exception:
            logger.error("将mqtt消息中的参数配置写入内存失败", exc_info=True)
            return False

    def save_param(self, image_path, ftps_ori_image, update_time, config_param, image_area):
        """
        将mqtt消息中的配置参数写入本地文件
        参数:
            image_path (str): 图像路径
            ftps_ori_image (str): 图像的FTPS URL
            update_time (str): 更新时间
            config_param (dict): 各项配置参数
            image_area (list): 检测区域
        返回:
            bool: 成功返回True，失败返回False
        """
        try:
            image_area_line = ''
            for x in image_area:
                image_area_line += f"{x[0]},{x[1]} "
            image_area_line = image_area_line[:-1]

            local_image_info_file_path = image_path.replace('.jpg', '.txt')
            with open(local_image_info_file_path, 'w') as file:
                file.writelines((
                    ftps_ori_image + os.linesep,
                    str(update_time) + os.linesep,
                    config_param['MODEL.DEVICE'] + os.linesep,
                    config_param['MODEL.RESIZE_W'] + os.linesep,
                    config_param['MODEL.RESIZE_H'] + os.linesep,
                    config_param['MODEL.SUPERPOINT.NMS_RADIUS'] + os.linesep,
                    config_param['MODEL.SUPERPOINT.KEYPOINT_THRESHOLD'] + os.linesep,
                    config_param['MODEL.SUPERPOINT.MAX_KEYPOINTS'] + os.linesep,
                    config_param['MODEL.SUPERGLUE.WEIGHTS'] + os.linesep,
                    config_param['MODEL.SUPERGLUE.SINKHORN_ITERATIONS'] + os.linesep,
                    config_param['MODEL.SUPERGLUE.MATCH_THRESHOLD'] + os.linesep,
                    config_param['SEGMENTATION.EXTEND'] + os.linesep,
                    config_param['CLUSTER.ERRORCOUNT'] + os.linesep,
                    config_param['CLUSTER.ERRORTHRESH'] + os.linesep,
                    config_param['THRESHOLD'] + os.linesep,
                    config_param['VOXEL.EXTEND'] + os.linesep,
                    config_param['VOXEL.THRESHOLD'] + os.linesep,
                    config_param['VOXEL.ERODE_PIXELS'] + os.linesep,
                    image_area_line
                ))
            logger.info('完成写入txt配置文件数据')
            return True
        except Exception:
            logger.error("将mqtt消息中的配置参数写入本地文件失败", exc_info=True)
            return False


    def del_old_files(self, image_path, del_image_flag: bool = True):
        """
        删除（本地图片）、本地npy、配置文件txt
        参数:
            image_path (str): 本地图片路径
            del_image_flag (bool): 是否删除图片
        返回:
            无
        """
        try:
            if del_image_flag:
                if osp.exists(image_path):
                    os.remove(image_path)
            npy_suffixes = ['-keypoint.npy', '-scores.npy', '-descriptors.npy']
            for suffix in npy_suffixes:
                npy_path = image_path.replace('.jpg', suffix)
                if osp.exists(npy_path):
                    os.remove(npy_path)
            txt_path = image_path.replace('.jpg', '.txt')
            if osp.exists(txt_path):
                os.remove(txt_path)
            logger.info(f'完成删除旧文件: {image_path}、npy、txt等数据')
        except Exception:
            logger.error(f"删除旧文件失败: {image_path}、npy、txt等数据", exc_info=True)

    def check_file(self, image_path):
        """
        检查本地文件
        参数:
            image_path (str): 本地图片路径
        返回:
            list: [bool, bool]。第一项代表图像是否正常，第二项代表其他是否正常
        """
        if not osp.exists(image_path):
            logger.info(f'没有找到图片: {image_path}')
            return False, True
        elif (not osp.exists(image_path.replace('.jpg', '-keypoint.npy'))
              or not osp.exists(image_path.replace('.jpg', '-scores.npy'))
              or not osp.exists(image_path.replace('.jpg', '-descriptors.npy'))
              or not osp.exists(image_path.replace('.jpg', '.txt'))):
            logger.info(f'本地配置文件txt或图片特征点数据文件缺失: {image_path}')
            return True, False
        return True, True

    def check_param(self, image_path, ftps_ori_image, update_time, config_param, image_area):
        """
        检查接收的mqtt消息中的图像url、更新时间、各项参数、检测区域是否与本地各项相同
        参数:
            image_path (str): 图像路径
            ftps_ori_image (str): 图像的FTPS URL
            update_time (str): 更新时间
            config_param (dict): 各项配置参数
            image_area (list): 检测区域
        返回:
            list: [bool, bool]。第一项代表图像是否相同，第二项代表其他是否相同。两项False表示错误
        """
        try:
            local_image_info_file_path = image_path.replace('.jpg', '.txt')
            with open(local_image_info_file_path, 'r') as file:
                local_ftps_ori_image = file.readline().strip()
                local_update_time = file.readline().strip()
                local_MODEL_DEVICE = file.readline().strip()
                local_MODEL_RESIZE_W = file.readline().strip()
                local_MODEL_RESIZE_H = file.readline().strip()
                local_MODEL_SCALE = file.readline().strip()

                local_MODEL_SUPERPOINT_NMS_RADIUS = file.readline().strip()
                local_MODEL_SUPERPOINT_KEYPOINT_THRESHOLD = file.readline().strip()
                local_MODEL_SUPERPOINT_MAX_KEYPOINTS = file.readline().strip()
                local_MODEL_SUPERGLUE_WEIGHTS = file.readline().strip()
                local_MODEL_SUPERGLUE_SINKHORN_ITERATIONS = file.readline().strip()
                local_MODEL_SUPERGLUE_MATCH_THRESHOLD = file.readline().strip()

                local_SEGMENTATION_EXTEND = file.readline().strip()

                local_CLUSTER_ERRORCOUNT = file.readline().strip()
                local_CLUSTER_ERRORTHRESH = file.readline().strip()
                local_THRESHOLD = file.readline().strip()

                local_VOXELL_EXTEND = file.readline().strip()
                local_VOXEL_THRESHOLD = file.readline().strip()
                local_VOXEL_ERODE_PIXELS = file.readline().strip()

                local_area = file.readline().strip()

            if local_ftps_ori_image != ftps_ori_image:
                logger.info(f"图片ftps url发生改变，local_ftps_ori_image：{local_ftps_ori_image}，ftps_ori_image:{ftps_ori_image}")
                return False, True
            elif local_update_time != update_time:
                logger.info(f"图片update_time发生改变，local_update_time：{local_update_time}，update_time:{update_time}")
                return True, False
            elif (local_MODEL_DEVICE != config_param['MODEL.DEVICE']
                  or local_MODEL_RESIZE_W != config_param['MODEL.RESIZE_W']
                  or local_MODEL_RESIZE_H != config_param['MODEL.RESIZE_H']
                  or local_MODEL_SCALE != config_param['MODEL.SCALE']
                  or local_MODEL_SUPERPOINT_NMS_RADIUS != config_param['MODEL.SUPERPOINT.NMS_RADIUS']
                  or local_MODEL_SUPERPOINT_KEYPOINT_THRESHOLD != config_param['MODEL.SUPERPOINT.KEYPOINT_THRESHOLD']
                  or local_MODEL_SUPERPOINT_MAX_KEYPOINTS != config_param['MODEL.SUPERPOINT.MAX_KEYPOINTS']
                  or local_MODEL_SUPERGLUE_WEIGHTS != config_param['MODEL.SUPERGLUE.WEIGHTS']
                  or local_MODEL_SUPERGLUE_SINKHORN_ITERATIONS != config_param['MODEL.SUPERGLUE.SINKHORN_ITERATIONS']
                  or local_MODEL_SUPERGLUE_MATCH_THRESHOLD != config_param['MODEL.SUPERGLUE.MATCH_THRESHOLD']
                  or local_SEGMENTATION_EXTEND != config_param['SEGMENTATION.EXTEND']
                  or local_CLUSTER_ERRORCOUNT != config_param['CLUSTER.ERRORCOUNT']
                  or local_CLUSTER_ERRORTHRESH != config_param['CLUSTER.ERRORTHRESH']
                  or local_THRESHOLD != config_param['THRESHOLD']
                  or local_VOXELL_EXTEND != config_param['VOXEL.EXTEND']
                  or local_VOXEL_THRESHOLD != config_param['VOXEL.THRESHOLD']
                  or local_VOXEL_ERODE_PIXELS != config_param['VOXEL.ERODE_PIXELS']):
                logger.info(f"配置文件发生改变，local_update_time：{local_update_time}，update_time:{update_time}")
                return True, False
            else:
                local_image_area = []
                for pair in local_area.split(' '):
                    xy = pair.split(',')
                    local_image_area.append([int(xy[0]), int(xy[1])])
                if local_image_area != image_area:
                    logger.info(f"检测区域发生改变，local_image_area：{local_image_area}，image_area:{image_area}")
                    return True, False
            logger.info("数据校验完成，图片url相同，update time相同，配置参数相同")
            return True, True
        except Exception:
            logger.error("检查接收的mqtt消息失败", exc_info=True)
            return False, False


    def check(self, image_path, ftps_ori_image, update_time, config_param, image_area):
        """
        Args:
            image_path:图像路径
            ftps_ori_image:图像ftps url
            update_time:更新实际
            config_param:各项参数
            image_area:检测区域
        Returns:
            [bool,bool]。第一项代表图像校验结果，第二项代表其他校验结果。两项False表示错误。
        校验
        """
        check_image_flag, param_check_flag = self.check_file(image_path)
        if not check_image_flag:
            self.del_old_files(image_path)
        elif not param_check_flag:
            self.del_old_files(image_path, False)
        else:
            check_image_flag, param_check_flag = self.check_param(image_path, ftps_ori_image, update_time, config_param,
                                                                  image_area)
            if not check_image_flag:
                self.del_old_files(image_path)
            elif not param_check_flag:
                self.del_old_files(image_path, False)
        return check_image_flag, param_check_flag

    def run_sg(self):
        pcd_path = osp.join(dirPath, 'database', 'pointcloud', self.cfg.CLOUDNAME)
        import py3d
        self.adjustor = py3d.Init(pcd_path)

        if self.cfg.TESTMODE:
            threading.Thread(target=self.test).start()
        else:
            # threading.Thread(target=self.receive_msg).start()
            ...
        while True:
            try:
                # if not self.cfg.TESTMODE:
                #     self.mqtt_client.loop()
                if self.queue.empty():
                    time.sleep(0.5)
                    continue
                with self.lock:
                    camare_receive_msg = self.queue.get()
                global logger
                logger = clear_data(logger)
                logger.info("开始检测形变")

                load_message_flag, image_id, ftps_ori_image, ftps_new_image, image_area, \
                    update_time, config_param, taskCode, dataCode, robotId, presetId, tagId, pointId, \
                    taskPatrolledId, calibrationData, stopPoint = self.load_message(camare_receive_msg)
                if not load_message_flag:
                    continue

                image_dir_path = osp.join(dirPath, 'database', 'image')
                ori_image_path = osp.join(image_dir_path, f'{image_id}.jpg')

                yaml_path = osp.join(image_dir_path, f'{image_id}.yaml')
                with open(yaml_path, 'w') as outfile:
                    outfile.write(calibrationData)

                if not self.refresh_param(config_param):
                    continue
                # logger.info(f'analyse image cfg:{str(self.cfg)}')
                analyse_image = Analyse_image(self.cfg.MODEL.DEVICE, self.cfg.MODEL.SUPERPOINT.NMS_RADIUS,
                                              self.cfg.MODEL.SUPERPOINT.KEYPOINT_THRESHOLD,
                                              self.cfg.MODEL.SUPERPOINT.MAX_KEYPOINTS,
                                              self.cfg.MODEL.SUPERGLUE.WEIGHTS,
                                              self.cfg.MODEL.SUPERGLUE.SINKHORN_ITERATIONS,
                                              self.cfg.MODEL.SUPERGLUE.MATCH_THRESHOLD,
                                              self.cfg.CLUSTER.ERRORCOUNT, self.cfg.CLUSTER.ERRORTHRESH,
                                              image_area, self.cfg.MODEL.RESIZE_W, self.cfg.MODEL.RESIZE_H,
                                              self.cfg.MODEL.SCALE, self.cfg.SEGMENTATION.EXTEND,
                                              self.cfg.VOXEL.EXTEND, self.cfg.VOXEL.THRESHOLD
                                              )
                origin_data, origin_image = None, None

                image_check_flag, param_check_flag = self.check(ori_image_path, ftps_ori_image, update_time,
                                                                config_param, image_area)
                if not image_check_flag:
                    if not param_check_flag:
                        continue
                    if self.cfg.TESTMODE:
                        ori_image_path = osp.join(image_dir_path, '001.jpg')
                    else:
                        if not get_image(ori_image_path, ftps_ori_image, host=self.cfg.FTPS.HOST,
                                         username=self.cfg.FTPS.USERNAME,
                                         pwd=self.cfg.FTPS.PASSWORD, port=self.cfg.FTPS.PORT):
                            continue
                    origin_data, origin_image = analyse_image.cal_keypoints(cv2.imread(ori_image_path),
                                                                            first_image_flag=True)
                    analyse_image.save_keypoints(origin_data, ori_image_path)
                    logger.info('生成写入本地特征点文件成功')
                    if not self.save_param(ori_image_path, ftps_ori_image, update_time, config_param, image_area):
                        continue
                elif not param_check_flag:
                    origin_data, origin_image = analyse_image.cal_keypoints(cv2.imread(ori_image_path),
                                                                            first_image_flag=True)
                    analyse_image.save_keypoints(origin_data, ori_image_path)
                    logger.info('生成写入本地特征点文件成功')
                    if not self.save_param(ori_image_path, ftps_ori_image, update_time, config_param, image_area):
                        continue
                else:
                    origin_image = analyse_image.resize_image(cv2.imread(ori_image_path),
                                                              first_image_flag=False)
                    origin_data = analyse_image.read_npy(ori_image_path)
                    logger.info('读取本地特征点文件成功')
                origin_data = {**{}, **{k + '0': v for k, v in origin_data.items()}}

                if self.cfg.TESTMODE:
                    new_image_path = osp.join(image_dir_path,
                                              f'{image_id}-{time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())}.jpg')
                    cv2.imwrite(new_image_path, cv2.imread(osp.join(image_dir_path, '002.jpg')))
                else:
                    new_image_path = osp.join(image_dir_path,
                                              f'{image_id}-{time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())}.jpg')
                    if not get_image(new_image_path, ftps_new_image, host=self.cfg.FTPS.HOST,
                                     username=self.cfg.FTPS.USERNAME,
                                     pwd=self.cfg.FTPS.PASSWORD, port=self.cfg.FTPS.PORT):
                        continue

                out_min_dfmt, out_max_dfmt, out_average_dfmt, min_3d, max_3d, average_3d = analyse_image.deformation(
                    new_image_path, ori_image_path, origin_data,
                    origin_image, yaml_path, py3d,
                    self.adjustor,
                    self.cfg.VOXEL.ERODE_PIXELS,
                    self.cfg.THRESHOLD)
                if not self.cfg.TESTMODE:
                    if out_max_dfmt <= self.cfg.THRESHOLD:
                        logger.info(f'形变小于阈值，不发送消息')
                        continue
                    if not self.deformation_pub(out_min_dfmt, out_max_dfmt, out_average_dfmt, ftps_ori_image,
                                                ftps_new_image,
                                                analyse_image.min_x, analyse_image.min_y, analyse_image.max_x,
                                                analyse_image.max_y, taskCode,
                                                dataCode,
                                                robotId,
                                                presetId,
                                                tagId,
                                                pointId,
                                                taskPatrolledId, stopPoint, min_3d, max_3d, average_3d):
                        logger.info('发送形变结果失败')
                        return False
                    else:
                        logger.info('发送形变结果成功')
            except Exception:
                logger.error("error", exc_info=True)

    def get_area_xy(self, image_area):
        xs = [x[0] for x in image_area]
        ys = [x[1] for x in image_area]
        x_min, y_min, x_max, y_max = min(xs), min(ys), max(xs), max(ys)
        return x_min, y_min, x_max, y_max

    def get_valid_result(self, image_area, displacement):
        # 创建 Path 对象
        poly_path = mpltPath.Path(image_area)
        out_displacement = []

        for x, y, delta_x, delta_y in displacement:
            point = (int(x), int(y))
            if not poly_path.contains_point(point):
                ...
            else:
                out_displacement.append([x, y, delta_x, delta_y])

        return np.array(out_displacement)

    def run_nocrr(self):
        sys.path.append(osp.join(dirPath, 'ncorr', 'lib'))
        import pyncorr
        from utils.superglue_and_Hmatrix import transimagebysuper

        if self.cfg.TESTMODE:
            threading.Thread(target=self.test).start()
        else:
            # threading.Thread(target=self.receive_msg).start()
            ...
        while True:
            try:
                # if not self.cfg.TESTMODE:
                #     self.mqtt_client.loop()
                if self.queue.empty():
                    time.sleep(0.5)
                    continue

                t0 = time.time()
                with self.lock:
                    camare_receive_msg = self.queue.get()
                global logger
                logger = clear_data(logger)
                logger.info("开始dic检测")
                # time.sleep(40)

                # 解析消息
                load_message_flag, image_id, ftps_ori_image, ftps_new_image, image_area, \
                    update_time, config_param, taskCode, dataCode, robotId, presetId, tagId, pointId, \
                    taskPatrolledId, calibrationData, stopPoint = self.load_message(camare_receive_msg)
                if not load_message_flag:
                    logger.info("接收到的mqtt消息格式错误，请检查消息")
                    continue
                distance = config_param["distance"]
                logger.info("mqtt消息格式检验完成")

                image_dir_path = osp.join(dirPath, 'database', 'image')

                # 保存标定的theta参数
                yaml_path = osp.join(image_dir_path, f'{image_id}.yaml')
                with open(yaml_path, 'w') as outfile:
                    outfile.write(calibrationData)

                logger.info(f"calibrationData已保存:{calibrationData}")
                try:
                    yaml_content = cv2.FileStorage(yaml_path, cv2.FILE_STORAGE_READ)
                    theta_0 = yaml_content.getNode("theta_0").real()
                    theta_1 = yaml_content.getNode("theta_1").real()
                    power = yaml_content.getNode("power").real()
                except Exception:
                    logger.error("读取calibrationData已保存失败，请检查calibrationData内容", exc_info=True)
                    continue

                x_min, y_min, x_max, y_max = self.get_area_xy(image_area)

                # 获取原始图片
                if not self.cfg.TESTMODE:
                    ori_image_path = osp.join(image_dir_path, f'{image_id}.jpg')
                    if not get_image(ori_image_path, ftps_ori_image, host=self.cfg.FTPS.HOST,
                                     username=self.cfg.FTPS.USERNAME,
                                     pwd=self.cfg.FTPS.PASSWORD, port=self.cfg.FTPS.PORT):
                        logger.error("下载ftp原始图片失败！")
                        continue
                    logger.info("下载ftp原始图片完成！")
                else:
                    ori_image_path = "/home/lxr/database/dic/shaoxing/5073/5033/2024525095421@003004.jpg"

                # 获取实时图片
                if not self.cfg.TESTMODE:
                    current_image_path = osp.join(image_dir_path,
                                                  f'{image_id}-{time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())}.jpg')
                    if not get_image(current_image_path, ftps_new_image, host=self.cfg.FTPS.HOST,
                                     username=self.cfg.FTPS.USERNAME,
                                     pwd=self.cfg.FTPS.PASSWORD, port=self.cfg.FTPS.PORT):
                        logger.error("下载ftp实时图片失败！")
                        continue
                    logger.info("下载ftp实时图片完成！")
                else:
                    current_image_path = "/home/lxr/database/dic/shaoxing/5073/5033/2024525095502@002003.jpg"

                t1 = time.time()
                logger.info(f"数据预处理耗时：{t1 - t0}秒")

                # 校正实时图片和原图
                targetimage = cv2.imread(ori_image_path)
                srcimage = cv2.imread(current_image_path)

                try:
                    im_transform, max_delta_xy, mkpts0, mkpts1 = transimagebysuper(srcimage, targetimage, x_min, x_max,
                                                                                   y_min, y_max)
                    _, max_delta_xy, mkpts0, mkpts1 = transimagebysuper(im_transform, targetimage, x_min, x_max,
                                                                                   y_min, y_max)
                except Exception:
                    logger.error("图片校正失败！", exc_info=True)
                    continue

                transformed_image_path = osp.join(image_dir_path,
                                                  f'{image_id}-{time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())}_tsf.jpg')
                cv2.imwrite(transformed_image_path, im_transform)
                t2 = time.time()
                logger.info(f"图片校正耗时：{t2 - t1}秒")

                # dic计算
                if not self.cfg.TESTMODE:
                    # 断开mqtt
                    self.mqtt_client.disconnect()
                    self.mqtt_client.loop_stop()
                # c++ ncorr计算
                out = pyncorr.ncorr_surface_real(ori_image_path, transformed_image_path, x_min, x_max, y_min, y_max)
                if not self.cfg.TESTMODE:
                    # 重连mqtt
                    self.mqtt_client = self.connect_mqtt()
                    self.mqtt_client.loop_start()
                    threading.Thread(target=self.receive_msg).start()
                # 判断dic计算结果flag
                if out < 0:
                    logger.error("dic计算结果失败！")
                    continue
                t3 = time.time()
                logger.info(f"dic计算及存储耗时：{t3 - t2}秒")

                dic_txt = osp.join(dirPath, "output.txt")
                with open(dic_txt, 'r') as file:
                    lines = file.readlines()
                dfs = [x.replace("\n", "").split(" ")[2:] for x in lines]

                dfs = np.array(dfs).astype(np.float128)

                # 新增功能：为方便调试，将像素位移信息txt保存到对应的database文件下
                output_save_path = transformed_image_path.replace("_tsf.jpg", "_output.txt")
                shutil.copy(dic_txt, output_save_path)

                print(f"step 1 dfs[4]:{dfs[4]}")
                ############################################   由于部分情况下，dic的输出疑似超过框图界限，新增功能   #############################
                # # 找到绝对值大于m的x元素，并将这些元素赋值为0
                # dfs[np.abs(dfs[:, 0]) > (x_max -x_min), :] = 0
                #
                # # 找到绝对值大于n的y元素，并将这些元素赋值为0
                # dfs[np.abs(dfs[:, 1]) > (y_max - y_min), :] = 0
                # 计算每个向量的模
                norms = np.linalg.norm(dfs, axis=1)

                # 将模大于阈值的向量设置为 [0, 0]
                max_delta_xy += 3
                dfs[norms > max_delta_xy] = [0, 0]
                ############################################   END 由于部分情况下，dic的输出疑似超过框图界限，新增功能   #############################

                print(f"step 2 dfs[4]:{dfs[4]}")
                dfs = np.around(dfs, 6)
                print(f"distance:{distance} theta_0:{theta_0} theta_1:{theta_1} power:{power}")
                distance = np.float128(distance)
                theta_0 = np.float128(theta_0)
                theta_1 = np.float128(theta_1)
                alpha = distance * theta_1 + theta_0
                print(f"before round alpha:{alpha}")
                alpha = np.around(alpha, 8)
                # theta_0 = round(theta_0, 6)
                # theta_1 = round(theta_1, 6)
                print(f"after round alpha:{alpha}")
                dfs[:, :] = dfs[:, :] * alpha

                print(f"step 4 dfs[4]:{dfs[4]}")

                dic_world_txt = osp.join(dirPath, "output_world.txt")
                with open(dic_world_txt, 'w') as file:
                    for i in range(len(dfs)):
                        params = lines[i].replace("\n", "").split(" ")
                        file.write(f"{params[0]} {params[1]} {dfs[i][0]} {dfs[i][1]}\n")

                row_norm = np.linalg.norm(dfs, axis=1)
                out_min_dfmt, out_max_dfmt, out_average_dfmt = np.min(row_norm), np.max(row_norm), np.mean(row_norm)
                t4 = time.time()
                logger.info(f"数据分析耗时：{t4 - t3}秒")

                if not self.cfg.TESTMODE:
                    timename = time.strftime("%Y%m%d%H%M%S", time.localtime()) + '.txt'
                    yearname = timename[:4]
                    monthname = int(timename[4:6])
                    dayname = int(timename[6:8])
                    address = f"{yearname}/{monthname}/{dayname}/{taskPatrolledId}/CCD/{pointId}_{robotId}_{timename}"
                    upload_image(dic_world_txt, address, self.cfg.FTPS.HOST, self.cfg.FTPS.USERNAME,
                                 self.cfg.FTPS.PASSWORD)
                    if not self.deformation_pub(out_min_dfmt, out_max_dfmt, out_average_dfmt, ftps_ori_image,
                                                ftps_new_image,
                                                x_min, y_min, x_max,
                                                y_max, taskCode,
                                                dataCode,
                                                robotId,
                                                presetId,
                                                tagId,
                                                pointId,
                                                taskPatrolledId, stopPoint, [0, 0, 0], [0, 0, 0], [0, 0, 0],
                                                file_path=address):
                        logger.info('发送形变结果失败')
                    else:
                        logger.info('发送形变结果成功')
                t5 = time.time()
                logger.info(f"全程耗时：{t5 - t0}秒")
            except Exception:
                logger.error("error", exc_info=True)

    def run_dic(self):
        from utils.superglue_and_Hmatrix import transimagebysuper

        # import pycuda.autoinit
        from src.dic import DICAnalyzer
        analyzer = DICAnalyzer(30, 3)

        if self.cfg.TESTMODE:
            threading.Thread(target=self.test).start()
        else:
            # threading.Thread(target=self.receive_msg).start()
            ...
        while True:
            try:
                # if not self.cfg.TESTMODE:
                #     self.mqtt_client.loop()
                if self.queue.empty():
                    time.sleep(0.5)
                    continue

                t0 = time.time()
                with self.lock:
                    camare_receive_msg = self.queue.get()
                global logger
                logger = clear_data(logger)
                logger.info("开始dic检测")
                # time.sleep(40)

                # 解析消息
                load_message_flag, image_id, ftps_ori_image, ftps_new_image, image_area, \
                    update_time, config_param, taskCode, dataCode, robotId, presetId, tagId, pointId, \
                    taskPatrolledId, calibrationData, stopPoint = self.load_message(camare_receive_msg)
                if not load_message_flag:
                    logger.info("接收到的mqtt消息格式错误，请检查消息")
                    continue
                distance = config_param["distance"]
                logger.info("mqtt消息格式检验完成")

                image_dir_path = osp.join(dirPath, 'database', 'image')

                # 保存标定的theta参数
                yaml_path = osp.join(image_dir_path, f'{image_id}.yaml')
                with open(yaml_path, 'w') as outfile:
                    outfile.write(calibrationData)

                logger.info(f"calibrationData已保存:{calibrationData}")
                try:
                    yaml_content = cv2.FileStorage(yaml_path, cv2.FILE_STORAGE_READ)
                    theta_0 = yaml_content.getNode("theta_0").real()
                    theta_1 = yaml_content.getNode("theta_1").real()
                    power = yaml_content.getNode("power").real()
                except Exception:
                    logger.error("读取calibrationData已保存失败，请检查calibrationData内容", exc_info=True)
                    continue

                x_min, y_min, x_max, y_max = self.get_area_xy(image_area)

                # 获取原始图片
                if not self.cfg.TESTMODE:
                    ori_image_path = osp.join(image_dir_path, f'{image_id}.jpg')
                    if not get_image(ori_image_path, ftps_ori_image, host=self.cfg.FTPS.HOST,
                                     username=self.cfg.FTPS.USERNAME,
                                     pwd=self.cfg.FTPS.PASSWORD, port=self.cfg.FTPS.PORT):
                        logger.error("下载ftp原始图片失败！")
                        continue
                    logger.info("下载ftp原始图片完成！")
                else:
                    ori_image_path = "/home/lxr/workdir/yd_deformation/error_data/image_9702_9702/446.jpg"

                # 获取实时图片
                if not self.cfg.TESTMODE:
                    current_image_path = osp.join(image_dir_path,
                                                  f'{image_id}-{time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())}.jpg')
                    if not get_image(current_image_path, ftps_new_image, host=self.cfg.FTPS.HOST,
                                     username=self.cfg.FTPS.USERNAME,
                                     pwd=self.cfg.FTPS.PASSWORD, port=self.cfg.FTPS.PORT):
                        logger.error("下载ftp实时图片失败！")
                        continue
                    logger.info("下载ftp实时图片完成！")
                else:
                    current_image_path = "/home/lxr/workdir/yd_deformation/error_data/image_9702_9702/446-.jpg"

                t1 = time.time()
                logger.info(f"数据预处理耗时：{t1 - t0}秒")

                # 校正实时图片和原图
                targetimage = cv2.imread(ori_image_path)
                srcimage = cv2.imread(current_image_path)

                try:
                    im_transform, max_delta_xy, mkpts0, mkpts1 = transimagebysuper(srcimage, targetimage, x_min, x_max,
                                                                                   y_min, y_max)
                except Exception:
                    logger.error("图片校正失败！", exc_info=True)
                    continue

                transformed_image_path = osp.join(image_dir_path,
                                                  f'{image_id}-{time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())}_tsf.jpg')
                cv2.imwrite(transformed_image_path, im_transform)
                t2 = time.time()
                logger.info(f"图片校正耗时：{t2 - t1}秒")

                # dic计算
                # 断开mqtt
                self.mqtt_client.disconnect()
                self.mqtt_client.loop_stop()
                # dic计算
                out = analyzer.compute_displacement(ori_image_path, transformed_image_path, x_min, x_max, y_min, y_max)
                # 重连mqtt
                self.mqtt_client = self.connect_mqtt()
                self.mqtt_client.loop_start()
                threading.Thread(target=self.receive_msg).start()
                # 判断dic计算结果flag
                if out <= 0:
                    continue
                t3 = time.time()
                logger.info(f"dic计算及存储耗时：{t3 - t2}秒")

                dic_txt = osp.join(dirPath, "output.txt")
                with open(dic_txt, 'r') as file:
                    lines = file.readlines()
                xy_dfs = [x.replace("\n", "").split(" ") for x in lines]

                xy_dfs = np.array(xy_dfs).astype(np.float128)
                valid_xy_dfs = self.get_valid_result(image_area, xy_dfs)
                dfs = valid_xy_dfs[...,2:]

                dfs = np.array(dfs).astype(np.float128)

                # 新增功能：为方便调试，将像素位移信息txt保存到对应的database文件下
                output_save_path = transformed_image_path.replace("_tsf.jpg", "_output.txt")
                shutil.copy(dic_txt, output_save_path)

                print(f"step 1 dfs[4]:{dfs[4]}")
                ############################################   由于部分情况下，dic的输出疑似超过框图界限，新增功能   #############################
                # # 找到绝对值大于m的x元素，并将这些元素赋值为0
                # dfs[np.abs(dfs[:, 0]) > (x_max -x_min), :] = 0
                #
                # # 找到绝对值大于n的y元素，并将这些元素赋值为0
                # dfs[np.abs(dfs[:, 1]) > (y_max - y_min), :] = 0
                # 计算每个向量的模
                norms = np.linalg.norm(dfs, axis=1)

                # 将模大于阈值的向量设置为 [0, 0]
                max_delta_xy += 3
                dfs[norms > max_delta_xy] = [0, 0]
                ############################################   END 由于部分情况下，dic的输出疑似超过框图界限，新增功能   #############################

                print(f"step 2 dfs[4]:{dfs[4]}")
                dfs = np.around(dfs, 6)
                print(f"distance:{distance} theta_0:{theta_0} theta_1:{theta_1} power:{power}")
                distance = np.float128(distance)
                theta_0 = np.float128(theta_0)
                theta_1 = np.float128(theta_1)
                alpha = distance * theta_1 + theta_0
                print(f"before round alpha:{alpha}")
                alpha = np.around(alpha, 8)
                # theta_0 = round(theta_0, 6)
                # theta_1 = round(theta_1, 6)
                print(f"after round alpha:{alpha}")
                dfs[:, :] = dfs[:, :] * alpha

                print(f"step 4 dfs[4]:{dfs[4]}")

                dic_world_txt = osp.join(dirPath, "output_world.txt")
                valid_xy = valid_xy_dfs[..., :2].astype(int)
                with open(dic_world_txt, 'w') as file:
                    for i in range(len(dfs)):
                        params = valid_xy[i]
                        file.write(f"{params[0]} {params[1]} {dfs[i][0]} {dfs[i][1]}\n")

                row_norm = np.linalg.norm(dfs, axis=1)
                out_min_dfmt, out_max_dfmt, out_average_dfmt = np.min(row_norm), np.max(row_norm), np.mean(row_norm)
                t4 = time.time()
                logger.info(f"数据分析耗时：{t4 - t3}秒")

                if not self.cfg.TESTMODE:
                    timename = time.strftime("%Y%m%d%H%M%S", time.localtime()) + '.txt'
                    yearname = timename[:4]
                    monthname = int(timename[4:6])
                    dayname = int(timename[6:8])
                    address = f"{yearname}/{monthname}/{dayname}/{taskPatrolledId}/CCD/{pointId}_{robotId}_{timename}"
                    upload_image(dic_world_txt, address, self.cfg.FTPS.HOST, self.cfg.FTPS.USERNAME,
                                 self.cfg.FTPS.PASSWORD)
                    if not self.deformation_pub(out_min_dfmt, out_max_dfmt, out_average_dfmt, ftps_ori_image,
                                                ftps_new_image,
                                                x_min, y_min, x_max,
                                                y_max, taskCode,
                                                dataCode,
                                                robotId,
                                                presetId,
                                                tagId,
                                                pointId,
                                                taskPatrolledId, stopPoint, [0, 0, 0], [0, 0, 0], [0, 0, 0],
                                                file_path=address):
                        logger.info('发送形变结果失败')
                    else:
                        logger.info('发送形变结果成功')
                t5 = time.time()
                logger.info(f"全程耗时：{t5 - t0}秒")
            except Exception:
                logger.error("error", exc_info=True)

        analyzer.clean_up()

    def receive_msg(self):
        def on_message(client, userdata, msg):
            try:
                logger.info(f"接收到mqtt检测形变任务，Received `{msg.payload.decode()}` from `{msg.topic}` topic")
                camare_receive_msg = json.loads(msg.payload.decode())
                with self.lock:
                    self.queue.put(camare_receive_msg)
                    self.queue.get() if self.queue.qsize() > 1 else time.sleep(0.01)
            except Exception:
                logger.error("error", exc_info=True)

        if self.mqtt_client is None:
            time.sleep(1)

        self.mqtt_client.subscribe(self.cfg.TOPIC.TASK)
        self.mqtt_client.on_message = on_message

    def deformation_pub(self, out_min_dfmt, out_max_dfmt, out_average_dfmt, ori_image_path, new_image_path, min_x,
                        min_y, max_x, max_y, task_code, data_code,
                        robotId, presetId, tagId, pointId, taskPatrolledId, stopPoint, min_3d, max_3d, average_3d,
                        file_path=""):
        data = {
            "logId": "",
            "gatewayId": "",
            "timestamp": "",
            "type": 0,
            "version": "v1.0",
            "data": {
                "robotId": f'{robotId}',
                "preset": f'{presetId}',
                "stopPoint": f'{stopPoint}',
                "tagId": f'{tagId}',
                "originalImage": ori_image_path,
                "deformationImage": new_image_path,
                "maxOffset": f'{out_max_dfmt}',
                "minOffset": f'{out_min_dfmt}',
                "meanOffset": f'{out_average_dfmt}',
                "reconTime": time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()),
                "baseLt": f'{min_x},{min_y}',
                "baseRb": f'{max_x},{max_y}',
                "targetLt": f'{min_x},{min_y}',
                "targetRb": f'{max_x},{max_y}',
                "type": 1,
                "deformationDesc": "",
                "dataCode": data_code,
                "taskCode": task_code,
                "taskPatrolledId": f'{taskPatrolledId}',
                "pointId": f'{pointId}',
                "maxOffsetPosition": f'{max_3d[0]},{max_3d[1]},{max_3d[2]}',
                "minOffsetPosition": f'{min_3d[0]},{min_3d[1]},{min_3d[2]}',
                "meanOffsetPosition": f'{average_3d[0]},{average_3d[1]},{average_3d[2]}',
                "file_path": f"/{file_path}"
            }
        }
        if file_path == "":
            data["data"]["file_path"] = ""
        logger.info(f'返回MQTT消息：{data}')
        param = json.dumps(data)
        if self.mqtt_client is None:
            time.sleep(1)
        result = self.mqtt_client.publish(self.cfg.TOPIC.RESULT, param)
        return True if result[0] == 0 else False

    def test(self):
        from utils.camera_receive_msg import camare_receive_msg
        while True:
            self.queue.put(camare_receive_msg)
            self.queue.get() if self.queue.qsize() > 1 else time.sleep(0.01)
            time.sleep(2)

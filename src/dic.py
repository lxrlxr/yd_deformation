"""
create time:2023-11
author:lxr

"""
import sys
import os.path as osp
dirPath = osp.dirname(osp.abspath(__file__))
dirPath = osp.dirname(dirPath)
sys.path.append(dirPath)

import cv2
import time
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt
from utils.logger import setup_logger
from src.gpucorrel import GPUCorrel

logger = setup_logger(project_name='dic')


class DICAnalyzer:
    def __init__(self, area_size, step):
        self.area_size = area_size
        self.step = step
        self.dic = GPUCorrel((area_size, area_size))

    @staticmethod
    def fill_missing_values(matrix):
        matrix = np.array(matrix)
        x, y = np.meshgrid(np.arange(matrix.shape[1]), np.arange(matrix.shape[0]))
        known_x, known_y = np.where(~np.isnan(matrix))
        unknown_x, unknown_y = np.where(np.isnan(matrix))

        values = matrix[~np.isnan(matrix)]
        interp_func = interpolate.LinearNDInterpolator(list(zip(known_x, known_y)), values)
        matrix[unknown_x, unknown_y] = interp_func(unknown_x, unknown_y)

        return matrix

    @staticmethod
    def save_displacement_to_txt(displacement, x_min, y_min):
        file_path = osp.join(dirPath, "output.txt")
        h, w, _ = displacement.shape
        with open(file_path, 'w') as file:
            for y in range(h):
                for x in range(w):
                    adjusted_x = x + x_min
                    adjusted_y = y + y_min
                    dx, dy = displacement[y, x]
                    if not np.isnan(dx) and not np.isnan(dy):
                        file.write(f"{adjusted_x} {adjusted_y} {dx} {dy}\n")
                    else:
                        # logger.info(f"x:{x},y:{y}")
                        # file.write(f"{adjusted_x} {adjusted_y} 0 0\n")
                        ...

    def compute_displacement(self, ref_image_path, target_image_path, x_min, x_max, y_min, y_max):
        try:
            ref_image = cv2.imread(ref_image_path, 0)
            target_image = cv2.imread(target_image_path, 0)

            ref_image = ref_image[y_min:y_max, x_min:x_max]
            target_image = target_image[y_min:y_max, x_min:x_max]

            h, w = ref_image.shape
            displacement = np.zeros(ref_image.shape + (2,), dtype=np.float32)

            t_step1 = time.time()
            for x in range(self.area_size // 2, w - self.area_size // 2, self.step):
                for y in range(self.area_size // 2, h - self.area_size // 2, self.step):
                    part_ref_image = ref_image[y - self.area_size // 2:y + self.area_size // 2,
                                     x - self.area_size // 2:x + self.area_size // 2]
                    part_target_image = target_image[y - self.area_size // 2:y + self.area_size // 2,
                                        x - self.area_size // 2:x + self.area_size // 2]

                    self.dic.set_ref(part_ref_image)
                    self.dic.prepare()

                    dp_update = self.dic.compute(part_target_image)
                    displacement[y, x, :] = dp_update

            nan_indexes = np.where(displacement == 0)
            displacement[nan_indexes] = np.nan
            displacement[..., 0] = self.fill_missing_values(displacement[..., 0])
            displacement[..., 1] = self.fill_missing_values(displacement[..., 1])

            self.save_displacement_to_txt(displacement, x_min, y_min)
        except Exception:
            logger.error("DIC计算失败！", exc_info=True)
            return False

        return True

    def plot_displacement_magnitude(self, displacement, save_path=None):
        displacement_magnitude = np.linalg.norm(displacement, axis=2)

        plt.imshow(displacement_magnitude, cmap='jet')
        plt.colorbar()
        plt.title('Displacement Magnitude')

        if save_path:
            plt.savefig(save_path)
            plt.close()
        else:
            plt.show()

    def clean_up(self):
        self.dic.clean()


if __name__ == '__main__':
    from src.dic import DICAnalyzer

    analyzer = DICAnalyzer(30, 3)
    # transformed_image_path = '/home/lxr/workdir/yd_deformation/error_data/2023-10-09/478-2023-10-09 15_29_54_tsf.jpg'
    # ori_image_path = '/home/lxr/workdir/yd_deformation/error_data/2023-10-09/478.jpg'

    ori_image_path = '/home/lxr/database/dic/guangmingcheng/guangmingcheng_image/504.jpg'
    transformed_image_path = '/home/lxr/database/dic/guangmingcheng/guangmingcheng_image/504-2023-11-16 15:39:51_tsf.jpg'

    x_min, x_max, y_min, y_max = 485, 779, 515, 651

    t0 = time.time()
    out = analyzer.compute_displacement(ori_image_path, transformed_image_path, x_min, x_max, y_min, y_max)
    logger.info(f"time cost:{time.time() - t0}秒")

    analyzer.clean_up()

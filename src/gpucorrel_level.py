# coding:utf-8

from math import ceil
import numpy as np
import pycuda.driver as cuda
from pycuda.compiler import SourceModule
import pycuda.gpuarray as gpuarray
from pycuda.reduction import ReductionKernel
import cv2
import os.path as osp

class Correl_level:
    def __init__(self, img_size):
        self.h, self.w = img_size
        self._ready = False
        self.nb_iter = 4
        self.show_diff = False
        if self.show_diff:
            import cv2
            cv2.namedWindow("Residual", cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)
        self.mul = 3
        self.rX, self.rY = -1, -1
        self.grid = (int(ceil(self.w / 32)),
                     int(ceil(self.h / 32)))
        self.block = (int(ceil(self.w / self.grid[0])),
                      int(ceil(self.h / self.grid[1])), 1)

        fields = ['x', 'y']
        self.fields_count = len(fields)

        self.devG = []
        self.devFieldsX = []
        self.devFieldsY = []
        for i in range(self.fields_count):
            self.devG.append(gpuarray.empty(img_size, np.float32))
            self.devFieldsX.append(gpuarray.empty((self.h, self.w), np.float32))
            self.devFieldsY.append(gpuarray.empty((self.h, self.w), np.float32))
        self.H = np.zeros((self.fields_count, self.fields_count), np.float32)
        self.devHi = gpuarray.empty(
            (self.fields_count, self.fields_count), np.float32)
        self.devOut = gpuarray.empty((self.h, self.w), np.float32)
        self.devX = gpuarray.empty((self.fields_count), np.float32)
        self.devVec = gpuarray.empty((self.fields_count), np.float32)
        self.devRef = gpuarray.empty(img_size, np.float32)
        self.devGradX = gpuarray.empty(img_size, np.float32)
        self.devGradY = gpuarray.empty(img_size, np.float32)

        kernel_code = """
        #include <cuda.h>
#define WIDTH % d
#define HEIGHT % d
#define PARAMETERS % d

texture<float, cudaTextureType2D, cudaReadModeElementType> tex;
texture<float, cudaTextureType2D, cudaReadModeElementType> tex_d;
texture<float, cudaTextureType2D, cudaReadModeElementType> texMask;

__global__ void gradient(float *gradX, float *gradY)
{
    const uint x = blockIdx.x * blockDim.x + threadIdx.x;
    const uint y = blockIdx.y * blockDim.y + threadIdx.y;
    if (x < WIDTH && y < HEIGHT)
    {
        gradX[x + WIDTH * y] = (tex2D(tex, (x + 1.5f) / WIDTH, (float)y / HEIGHT) + tex2D(tex, (x + 1.5f) / WIDTH, (y + 1.f) / HEIGHT) - tex2D(tex, (x - .5f) / WIDTH, (float)y / HEIGHT) - tex2D(tex, (x - .5f) / WIDTH, (y + 1.f) / HEIGHT));
        gradY[x + WIDTH * y] = (tex2D(tex, (float)x / WIDTH, (y + 1.5f) / HEIGHT) + tex2D(tex, (x + 1.f) / WIDTH, (y + 1.5f) / HEIGHT) - tex2D(tex, (float)x / WIDTH, (y - .5f) / HEIGHT) - tex2D(tex, (x + 1.f) / WIDTH, (y - .5f) / HEIGHT));
    }
}

__global__ void resampleR(float *out, const int w, const int h)
{
    const int idx = threadIdx.x + blockIdx.x * blockDim.x;
    const int idy = threadIdx.y + blockIdx.y * blockDim.y;
    if (idx < w && idy < h)
        out[idx + w * idy] = tex2D(tex, (float)idx / w, (float)idy / h);
}

__global__ void resample(float *out, const int w, const int h)
{
    const int idx = threadIdx.x + blockIdx.x * blockDim.x;
    const int idy = threadIdx.y + blockIdx.y * blockDim.y;
    if (idx < w && idy < h)
        out[idx + w * idy] = tex2D(tex_d, (float)idx / w, (float)idy / h);
}

__global__ void makeG(float *G, const float *gradX, const float *gradY,
                      const float *fieldX, const float *fieldY)
{
    const int idx = threadIdx.x + blockIdx.x * blockDim.x;
    const int idy = threadIdx.y + blockIdx.y * blockDim.y;
    if (idx < WIDTH && idy < HEIGHT)
    {
        int id = idx + WIDTH * idy;
        G[id] = gradX[id] * fieldX[id] + gradY[id] * fieldY[id];
    }
}

__global__ void makeDiff(float *out, const float *param,
                         const float *fieldsX, const float *fieldsY)
{
    const int idx = threadIdx.x + blockIdx.x * blockDim.x;
    const int idy = threadIdx.y + blockIdx.y * blockDim.y;
    if (idx < WIDTH && idy < HEIGHT)
    {
        const int id = idx + WIDTH * idy;
        float ox = .5f;
        float oy = .5f;
        for (uint i = 0; i < PARAMETERS; i++)
        {
            ox += param[i] * fieldsX[WIDTH * HEIGHT * i + id];
            oy += param[i] * fieldsY[WIDTH * HEIGHT * i + id];
        }
        out[id] = (tex2D(tex, (idx + .5f) / WIDTH, (idy + .5f) / HEIGHT) - tex2D(tex_d, (idx + ox) / WIDTH, (idy + oy) / HEIGHT)) * tex2D(texMask, idx + .5f, idy + .5f);
    }
}

__global__ void myDot(const float *M, float *v)
{
    uint id = threadIdx.x;
    __shared__ float sh_v[PARAMETERS];
    float val = 0;
    sh_v[id] = v[id];
    __syncthreads();
    for (uint i = 0; i < PARAMETERS; i++)
    {
        val += M[id * PARAMETERS + i] * sh_v[i];
    }
    v[id] = val;
}

__global__ void kadd(float *v, const float k, const float *v2)
{
    v[threadIdx.x] += k * v2[threadIdx.x];
}
"""

        self.mod = SourceModule(kernel_code % (self.w, self.h, self.fields_count))

        self._resampleRefKrnl = self.mod.get_function('resampleR')
        self._resampleKrnl = self.mod.get_function('resample')
        self._gradientKrnl = self.mod.get_function('gradient')
        self._makeGKrnl = self.mod.get_function('makeG')
        self._makeDiff = self.mod.get_function('makeDiff')
        self._dotKrnl = self.mod.get_function('myDot')
        self._addKrnl = self.mod.get_function('kadd')
        self._mulRedKrnl = ReductionKernel(np.float32, neutral="0",
                                           reduce_expr="a+b", map_expr="x[i]*y[i]",
                                           arguments="float *x, float *y")
        self._leastSquare = ReductionKernel(np.float32, neutral="0",
                                            reduce_expr="a+b", map_expr="x[i]*x[i]",
                                            arguments="float *x")

        self.tex = self.mod.get_texref('tex')
        self.tex_d = self.mod.get_texref('tex_d')
        self.texMask = self.mod.get_texref('texMask')
        for t in [self.tex, self.tex_d]:
            t.set_flags(cuda.TRSF_NORMALIZED_COORDINATES)
        for t in [self.tex, self.tex_d, self.texMask]:
            t.set_filter_mode(cuda.filter_mode.LINEAR)
            t.set_address_mode(0, cuda.address_mode.BORDER)
            t.set_address_mode(1, cuda.address_mode.BORDER)

        self._resampleRefKrnl.prepare("Pii", texrefs=[self.tex])
        self._resampleKrnl.prepare("Pii", texrefs=[self.tex_d])
        self._gradientKrnl.prepare("PP", texrefs=[self.tex])
        self._makeDiff.prepare("PPPP", texrefs=[self.tex, self.tex_d, self.texMask])
        self._addKrnl.prepare("PfP")

    def set_fields(self, fieldsX, fieldsY):
        self.devFieldsX = fieldsX
        self.devFieldsY = fieldsY
        self.fields = True

    def set_image(self, img_d):
        if isinstance(img_d, np.ndarray):
            self.array_d = cuda.matrix_to_array(img_d, "C")
        elif isinstance(img_d, gpuarray.GPUArray):
            self.array_d = cuda.gpuarray_to_array(img_d, "C")
        self.tex_d.set_array(self.array_d)
        self.devX.set(np.zeros(self.fields_count, dtype=np.float32))

    def set_mask(self, mask):
        if not mask.dtype == np.float32:
            mask = mask.astype(np.float32)
        if isinstance(mask, np.ndarray):
            self.maskArray = cuda.matrix_to_array(mask, 'C')
        elif isinstance(mask, gpuarray.GPUArray):
            self.maskArray = cuda.gpuarray_to_array(mask, 'C')
        self.texMask.set_array(self.maskArray)

    def setDisp(self, X):
        if isinstance(X, gpuarray.GPUArray):
            self.devX = X
        elif isinstance(X, np.ndarray):
            self.devX.set(X)

    def set_ref(self, img):
        self.devRef.set(img)
        self.update_ref()

    def update_ref(self):
        self.array = cuda.gpuarray_to_array(self.devRef, 'C')
        self.tex.set_array(self.array)
        self._gradientKrnl.prepared_call(self.grid, self.block,
                                         self.devGradX.gpudata, self.devGradY.gpudata)
        self._ready = False

    def prepare(self):
        if not hasattr(self, 'maskArray'):
            mask = np.zeros((self.h, self.w), np.float32)
            mask[self.h // 20:-self.h // 20, self.w // 20:-self.w // 20] = 1
            self.set_mask(mask)
        if not self._ready:
            self._makeG()
            self._makeH()
            self._ready = True

    def _makeG(self):
        for i in range(self.fields_count):
            self._makeGKrnl(self.devG[i].gpudata, self.devGradX.gpudata,
                            self.devGradY.gpudata,
                            self.devFieldsX[i], self.devFieldsY[i],
                            block=self.block, grid=self.grid)

    def _makeH(self):
        '''
        0 0
        1 0
        1 1
        '''
        for i in range(self.fields_count):
            for j in range(i + 1):
                self.H[i, j] = self._mulRedKrnl(self.devG[i], self.devG[j]).get()
                if i != j:
                    self.H[j, i] = self.H[i, j]
        self.devHi.set(np.linalg.inv(self.H))

    def resample_ref(self, newY, newX, devOut):
        self._resampleRefKrnl.prepared_call(self.grid, self.block,
                                            devOut.gpudata, np.int32(newX), np.int32(newY))

    def resampleD(self, newY, newX):
        if (self.rX, self.rY) != (np.int32(newX), np.int32(newY)):
            self.rGrid = (int(ceil(newX / 32)), int(ceil(newY / 32)))
            self.rBlock = (int(ceil(newX / self.rGrid[0])),
                           int(ceil(newY / self.rGrid[1])), 1)
            self.rX, self.rY = np.int32(newX), np.int32(newY)
            self.devROut = gpuarray.empty((newY, newX), np.float32)
        self._resampleKrnl.prepared_call(self.rGrid, self.rBlock,
                                         self.devROut.gpudata,
                                         self.rX, self.rY)
        return self.devROut

    def compute(self):
        # 求残差，取了两幅图之间像素差值，并且只使 中间部分有效，输出保存在devOut中
        self._makeDiff.prepared_call(self.grid, self.block,
                                     self.devOut.gpudata,
                                     self.devX.gpudata,
                                     self.devFieldsX.gpudata,
                                     self.devFieldsY.gpudata)

        # 平方和作为res
        self.res = self._leastSquare(self.devOut).get()

        for i in range(self.nb_iter):
            for i in range(self.fields_count):
                self.devVec[i] = self._mulRedKrnl(self.devG[i], self.devOut)
            self._dotKrnl(self.devHi, self.devVec,
                          grid=(1, 1), block=(self.fields_count, 1, 1))
            self._addKrnl.prepared_call((1, 1), (self.fields_count, 1, 1),
                                        self.devX.gpudata, self.mul,
                                        self.devVec.gpudata)

            self._makeDiff.prepared_call(self.grid, self.block,
                                         self.devOut.gpudata,
                                         self.devX.gpudata,
                                         self.devFieldsX.gpudata,
                                         self.devFieldsY.gpudata)
            oldres = self.res
            self.res = self._leastSquare(self.devOut).get()
            if self.res >= oldres:
                self._addKrnl.prepared_call((1, 1), (self.fields_count, 1, 1),
                                            self.devX.gpudata,
                                            -self.mul,
                                            self.devVec.gpudata)
                self.res = oldres
                break
        if self.show_diff:
            cv2.imshow("Residual", (self.devOut.get() + 128).astype(np.uint8))
            cv2.waitKey(1)
        return self.devX.get()

# coding:utf-8

from math import ceil
import numpy as np
import pycuda.driver as cuda
from pycuda.compiler import SourceModule
import pycuda.gpuarray as gpuarray

from .gpucorrel_level import Correl_level

context = None


def interpNearest(ary, ny, nx):
    if ary.shape == (ny, nx):
        return ary
    y, x = ary.shape
    rx = x / nx
    ry = y / ny
    out = np.empty((ny, nx), dtype=np.float32)
    for j in range(ny):
        for i in range(nx):
            out[j, i] = ary[int(ry * j + .5), int(rx * i + .5)]
    return out


class GPUCorrel:
    def __init__(self, img_size):
        cuda.init()
        from pycuda.tools import make_default_context
        global context
        context = make_default_context()

        self.levels = 3
        self.loop = 0
        self.resamplingFactor = 2
        h, w = img_size

        self.h, self.w = [], []
        for i in range(self.levels):
            self.h.append(int(round(h / (self.resamplingFactor ** i))))
            self.w.append(int(round(w / (self.resamplingFactor ** i))))

        fields = ['x', 'y']
        self.fields_count = len(fields)

        self.correl = []
        for i in range(self.levels):
            self.correl.append(Correl_level((self.h[i], self.w[i])))

        s = """
texture<float, cudaTextureType2D, cudaReadModeElementType> texFx{0};
texture<float, cudaTextureType2D, cudaReadModeElementType> texFy{0};
__global__ void resample{0}(float* outX, float* outY, const int x, const int y)
{{
  const int idx = blockIdx.x*blockDim.x+threadIdx.x;
  const int idy = blockIdx.y*blockDim.y+threadIdx.y;
  if(idx < x && idy < y)
  {{
    outX[idy*x+idx] = tex2D(texFx{0},(float)idx/x, (float)idy/y);
    outY[idy*x+idx] = tex2D(texFy{0},(float)idx/x, (float)idy/y);
  }}
}}
    """

        self.src = ""
        for i in range(self.fields_count):
            self.src += s.format(i)

        self.mod = SourceModule(self.src)

        self.texFx = []
        self.texFy = []
        self.resampleF = []
        for i in range(self.fields_count):
            self.texFx.append(self.mod.get_texref("texFx%d" % i))
            self.texFy.append(self.mod.get_texref("texFy%d" % i))
            self.resampleF.append(self.mod.get_function("resample%d" % i))
            self.resampleF[i].prepare("PPii", texrefs=[self.texFx[i], self.texFy[i]])

        for t in self.texFx + self.texFy:
            t.set_flags(cuda.TRSF_NORMALIZED_COORDINATES)
            t.set_filter_mode(cuda.filter_mode.LINEAR)
            t.set_address_mode(0, cuda.address_mode.BORDER)
            t.set_address_mode(1, cuda.address_mode.BORDER)

        self.set_fields(fields)

    def get_fields(self, y, x):
        outX = gpuarray.empty((self.fields_count, y, x), np.float32)
        outY = gpuarray.empty((self.fields_count, y, x), np.float32)
        grid = (int(ceil(x / 32)), int(ceil(y / 32)))
        block = (int(ceil(x / grid[0])), int(ceil(y / grid[1])), 1)
        for i in range(self.fields_count):
            self.resampleF[i].prepared_call(grid, block,
                                            outX[i, :, :].gpudata,
                                            outY[i, :, :].gpudata,
                                            np.int32(x), np.int32(y))
        return outX, outY

    def set_ref(self, img):
        img = img.astype(np.float32)

        self.correl[0].set_ref(img)
        for i in range(1, self.levels):
            self.correl[i - 1].resample_ref(self.h[i], self.w[i],
                                            self.correl[i].devRef)
            self.correl[i].update_ref()

    def set_fields(self, fields):
        o = cuda.matrix_to_array(np.ones((self.h[0], self.w[0]), dtype=np.float32), "C")
        z = cuda.matrix_to_array(np.zeros((self.h[0], self.w[0]), dtype=np.float32), "C")

        for i, f in enumerate(fields):
            if f == "x":
                self.texFx[i].set_array(o)
                self.texFy[i].set_array(z)
            elif f == "y":
                self.texFx[i].set_array(z)
                self.texFy[i].set_array(o)

        for i in range(self.levels):
            self.correl[i].set_fields(*self.get_fields(self.h[i], self.w[i]))

    def prepare(self):
        for c in self.correl:
            c.prepare()

    def set_image(self, img_d):
        img_d = img_d.astype(np.float32)
        self.correl[0].set_image(img_d)
        for i in range(1, self.levels):
            self.correl[i].set_image(
                self.correl[i - 1].resampleD(self.correl[i].h, self.correl[i].w))

    def compute(self, img_d=None):
        if img_d is not None:
            self.set_image(img_d)
        try:
            disp = self.last / (self.resamplingFactor ** self.levels)
        except AttributeError:
            disp = np.array([0] * self.fields_count, dtype=np.float32)
        for i in reversed(range(self.levels)):
            disp *= self.resamplingFactor
            self.correl[i].setDisp(disp)
            disp = self.correl[i].compute()
            self.last = disp
        return disp

    def clean(self):
        context.pop()

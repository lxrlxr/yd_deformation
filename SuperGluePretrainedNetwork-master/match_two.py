#! /usr/bin/env python3
"""
个人写的最简单的测试两张图片，检测特征点并匹配，并图像显示出来。
不包括从本地读取特征点信息等功能。
"""

import cv2
import time
import os
import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

import numpy as np
from mpl_toolkits.mplot3d import Axes3D

import torch
from models.matching import Matching
from models.utils import (make_matching_plot_fast, frame2tensor)

torch.set_grad_enabled(False)


class analyse_image:
    def __init__(self):
        self.device = 'cuda:0'
        config = {
            'superpoint': {
                'nms_radius': 1,
                'keypoint_threshold': 0.0005,
                'max_keypoints': 8000
            },
            'superglue': {
                'weights': 'outdoor',
                'sinkhorn_iterations': 20,
                'match_threshold': 0.2,
            }
        }
        self.matching = Matching(config).eval().to(self.device)
        self.keys = ['keypoints', 'scores', 'descriptors']

    def fram_to_tensor(self, frame):
        frame_tensor = frame2tensor(frame, self.device)
        return frame_tensor

    def resize_image(self, input_image):
        return input_image

    # def visualize_matches_3d(self, image1, image2, mkpts0, mkpts1):
    #     # 将灰度图转换为RGB图像
    #     image1_rgb = cv2.cvtColor(image1, cv2.COLOR_GRAY2RGB)
    #     image2_rgb = cv2.cvtColor(image2, cv2.COLOR_GRAY2RGB)
    #
    #     # 创建一个新的图像，将两个图像拼接在一起
    #     h1, w1 = image1_rgb.shape[:2]
    #     h2, w2 = image2_rgb.shape[:2]
    #     new_image = np.zeros((max(h1, h2), w1 + w2, 3), dtype=np.uint8)
    #     new_image[:h1, :w1, :] = image1_rgb
    #     new_image[:h2, w1:w1 + w2, :] = image2_rgb
    #
    #     # 计算匹配点之间的距离
    #     distances = np.linalg.norm(mkpts0 - mkpts1, axis=1)
    #
    #     # 创建3D图像
    #     fig = plt.figure(figsize=(10, 7))
    #     ax = fig.add_subplot(111, projection='3d')
    #
    #     # 将两幅图像平铺在X-Y平面上
    #     ax.imshow(new_image, extent=[0, w1 + w2, 0, max(h1, h2)], aspect='auto')
    #
    #     # 绘制匹配点
    #     for (x1, y1), (x2, y2), distance in zip(mkpts0, mkpts1, distances):
    #         x2 += w1  # 调整x2以匹配拼接后的图像坐标
    #         ax.plot([x1, x2], [y1, y2], [0, distance], 'r-', linewidth=1)
    #         ax.scatter([x1, x2], [y1, y2], [0, distance], c='yellow', s=10)
    #         # 显示距离
    #         ax.text((x1 + x2) / 2, (y1 + y2) / 2, distance / 2, f'{distance:.2f}', color='blue', fontsize=8)
    #
    #     ax.set_xlabel('X axis')
    #     ax.set_ylabel('Y axis')
    #     ax.set_zlabel('Distance')
    #     plt.show()

    def visualize_matches_3d(self, mkpts0, mkpts1):
        # 计算匹配点之间的距离
        distances = np.linalg.norm(mkpts0 - mkpts1, axis=1)

        # 创建3D图像
        fig = plt.figure(figsize=(10, 7))
        ax = fig.add_subplot(111, projection='3d')

        # 绘制匹配点
        xs, ys = mkpts0[:, 0], mkpts0[:, 1]
        zs = distances
        ax.scatter(xs, ys, zs, c='yellow', s=10)

        for x, y, z in zip(xs, ys, zs):
            ax.text(x, y, z, f'{z:.2f}', color='blue', fontsize=8)

        ax.set_xlabel('X axis')
        ax.set_ylabel('Y axis')
        ax.set_zlabel('Distance')
        plt.show()

    # def visualize_matches_3d(self, mkpts0, mkpts1):
    #     # 计算匹配点之间的距离
    #     distances = np.linalg.norm(mkpts0 - mkpts1, axis=1)
    #
    #     # 创建图像
    #     fig = plt.figure(figsize=(14, 7))
    #
    #     # 3D散点图
    #     ax1 = fig.add_subplot(121, projection='3d')
    #     xs, ys = mkpts0[:, 0], mkpts0[:, 1]
    #     zs = distances
    #     scatter = ax1.scatter(xs, ys, zs, c='yellow', s=10)
    #
    #     for x, y, z in zip(xs, ys, zs):
    #         ax1.text(x, y, z, f'{z:.2f}', color='blue', fontsize=8)
    #
    #     ax1.set_xlabel('X axis')
    #     ax1.set_ylabel('Y axis')
    #     ax1.set_zlabel('Distance')
    #     ax1.view_init(elev=20., azim=-60)  # 调整视角，使x、y方向与图像一致
    #
    #     # 立体直方图
    #     ax2 = fig.add_subplot(122, projection='3d')
    #
    #     # 计算直方图数据
    #     hist, xedges, yedges = np.histogram2d(xs, ys, bins=30, weights=zs, density=False)
    #
    #     # Construct arrays for the anchor positions of the bars.
    #     xpos, ypos = np.meshgrid(xedges[:-1] + 0.25, yedges[:-1] + 0.25, indexing="ij")
    #     xpos = xpos.ravel()
    #     ypos = ypos.ravel()
    #     zpos = np.zeros_like(xpos)
    #
    #     # Construct arrays with the dimensions for the bars.
    #     dx = dy = 0.5 * np.ones_like(zpos)
    #     dz = hist.ravel()
    #
    #     ax2.bar3d(xpos, ypos, zpos, dx, dy, dz, zsort='average', color='blue', alpha=0.7)
    #
    #     ax2.set_xlabel('X axis')
    #     ax2.set_ylabel('Y axis')
    #     ax2.set_zlabel('Distance')
    #     ax2.set_title('3D Histogram of Distances')
    #
    #     plt.show()

    def compare_blocks(self, input_image1, input_image2):
        """
        比较两张图像的特征点
        :param input_image1: 输入图像1
        :param input_image2: 输入图像2
        :return: keypoints 和 matches
        """
        t0 = time.time()

        # 处理第一张图像
        # input_image1 = self.resize_image(input_image1)
        # frame_tensor = self.fram_to_tensor(input_image1)
        last_data = self.matching.superpoint({'image': input_image1})
        last_data = {k + '0': last_data[k] for k in self.keys}
        last_data['image0'] = input_image1

        # 处理第二张图像
        # input_image2 = self.resize_image(input_image2)
        # frame_tensor = self.fram_to_tensor(input_image2)

        # 进行匹配
        pred = self.matching({**last_data, 'image1': input_image2})

        # 提取关键点和匹配信息
        kpts0 = last_data['keypoints0'][0].cpu().numpy()
        kpts1 = pred['keypoints1'][0].cpu().numpy()
        matches = pred['matches0'][0].cpu().numpy()

        # 过滤有效匹配
        valid = matches > -1
        mkpts0 = kpts0[valid]
        mkpts1 = kpts1[matches[valid]]

        print(f'{self.current_out_image_path}  cost time: {time.time() - t0}')

        return kpts0, kpts1, mkpts0, mkpts1

    def compare_images_with_blocks(self, input_image1, input_image2, block_size=500, overlap=50):
        """
        使用分块匹配和窗口重叠的方法比较两张图像的特征点
        :param input_image1: 输入图像1
        :param input_image2: 输入图像2
        :param block_size: 分块大小
        :param overlap: 窗口重叠大小
        :return: 合并后的kpts0, kpts1, mkpts0, mkpts1
        """
        t0 = time.time()

        # Resize and convert images to tensor
        input_image1 = self.resize_image(input_image1)
        frame_tensor1 = self.fram_to_tensor(input_image1)

        input_image2 = self.resize_image(input_image2)
        frame_tensor2 = self.fram_to_tensor(input_image2)

        # Initialize lists to collect keypoints and matches
        all_kpts0 = []
        all_kpts1 = []
        # all_matches = []
        all_matches0 = []
        all_matches1 = []

        # Define function to get blocks
        def get_blocks(image_tensor, block_size, overlap):
            """
            获取图像张量的分块
            :param image_tensor: 图像张量
            :param block_size: 分块大小
            :param overlap: 窗口重叠大小
            :return: 分块列表和对应的坐标
            """
            blocks = []
            coords = []
            h, w = image_tensor.shape[2], image_tensor.shape[3]
            step = block_size - overlap
            for y in range(0, h, step):
                for x in range(0, w, step):
                    y_end = min(y + block_size, h)
                    x_end = min(x + block_size, w)
                    blocks.append(image_tensor[:, :, y:y_end, x:x_end])
                    coords.append((y, x))
            return blocks, coords

        # Get blocks for image1 and image2
        blocks1, coords1 = get_blocks(frame_tensor1, block_size, overlap)
        blocks2, coords2 = get_blocks(frame_tensor2, block_size, overlap)

        # Match each block of image2 with the corresponding block of image1
        for (block1, coord1), (block2, coord2) in zip(zip(blocks1, coords1), zip(blocks2, coords2)):
            kpts0, kpts1, mkpts0, mkpts1 = self.compare_blocks(block1, block2)

            # Adjust coordinates for the block position
            kpts0[:, 0] += coord1[1]
            kpts0[:, 1] += coord1[0]
            kpts1[:, 0] += coord2[1]
            kpts1[:, 1] += coord2[0]
            mkpts0[:, 0] += coord1[1]
            mkpts0[:, 1] += coord1[0]
            mkpts1[:, 0] += coord2[1]
            mkpts1[:, 1] += coord2[0]

            all_kpts0.append(kpts0)
            all_kpts1.append(kpts1)
            # all_matches.append(mkpts0)
            all_matches0.append(mkpts0)
            all_matches1.append(mkpts1)

        # # Concatenate keypoints and matches
        # kpts0 = np.concatenate(all_kpts0, axis=0)
        # kpts1 = np.concatenate(all_kpts1, axis=0)
        # mkpts0 = np.concatenate(all_matches, axis=0)
        # mkpts1 = kpts1[np.concatenate(all_matches, axis=0)]

        # Concatenate keypoints and matches if they are not empty
        if all_kpts0:
            kpts0 = np.concatenate(all_kpts0, axis=0)
        else:
            kpts0 = np.empty((0, 2))

        if all_kpts1:
            kpts1 = np.concatenate(all_kpts1, axis=0)
        else:
            kpts1 = np.empty((0, 2))

        if all_matches0:
            mkpts0 = np.concatenate(all_matches0, axis=0)
        else:
            mkpts0 = np.empty((0, 2))

        if all_matches1:
            mkpts1 = np.concatenate(all_matches1, axis=0)
        else:
            mkpts1 = np.empty((0, 2))

        # 去除重复的特征点
        kpts0, kpts1, mkpts0, mkpts1 = self.remove_duplicates(kpts0, kpts1, mkpts0, mkpts1)

        print(f'{self.current_out_image_path}  cost time: {time.time() - t0}')

        return kpts0, kpts1, mkpts0, mkpts1

    def remove_duplicates(self, kpts0, kpts1, mkpts0, mkpts1):
        """
        去除重复的特征点和匹配对
        :param kpts0: 特征点0
        :param kpts1: 特征点1
        :param mkpts0: 匹配的特征点0
        :param mkpts1: 匹配的特征点1
        :return: 去重后的特征点和匹配对
        """
        # 根据坐标去重
        kpts0_set = set(map(tuple, mkpts0))
        kpts1_set = set(map(tuple, mkpts1))
        unique_mkpts0 = []
        unique_mkpts1 = []

        for pt0, pt1 in zip(mkpts0, mkpts1):
            if tuple(pt0) in kpts0_set and tuple(pt1) in kpts1_set:
                unique_mkpts0.append(pt0)
                unique_mkpts1.append(pt1)
                kpts0_set.remove(tuple(pt0))
                kpts1_set.remove(tuple(pt1))

        unique_mkpts0 = np.array(unique_mkpts0)
        unique_mkpts1 = np.array(unique_mkpts1)

        return kpts0, kpts1, unique_mkpts0, unique_mkpts1

    def compare_images(self, input_image1, input_image2):
        t0 = time.time()
        input_image1 = self.resize_image(input_image1)
        frame_tensor = self.fram_to_tensor(input_image1)
        last_data = self.matching.superpoint({'image': frame_tensor})
        last_data = {k + '0': last_data[k] for k in self.keys}
        last_data['image0'] = frame_tensor

        print(f'caculate one image key points cost time: {time.time() - t0}')

        input_image2 = self.resize_image(input_image2)
        frame_tensor = self.fram_to_tensor(input_image2)

        pred = self.matching({**last_data, 'image1': frame_tensor})

        kpts0 = last_data['keypoints0'][0].cpu().numpy()
        kpts1 = pred['keypoints1'][0].cpu().numpy()
        matches = pred['matches0'][0].cpu().numpy()

        valid = matches > -1
        mkpts0 = kpts0[valid]
        mkpts1 = kpts1[matches[valid]]

        print(f'{self.current_out_image_path}  cost time: {time.time() - t0}')

        # kpts0, kpts1, mkpts0, mkpts1 = self.compare_images_with_blocks(input_image1, input_image2, block_size=500, overlap=50)

        """
        临时新增测试功能：测试super point + glue的单应性矩阵（Homography）结果
        """
        import numpy as np
        rows, cols = input_image2.shape[:2]
        # 转为整数
        pts_src = np.int32(mkpts0)
        pts_dst = np.int32(mkpts1)

        # 计算单应性矩阵
        h, status = cv2.findHomography(pts_dst, pts_src, cv2.RANSAC, 5.0)

        # 执行图像变换
        im_transform = cv2.warpPerspective(input_image2, h, (cols, rows))

        cv2.imshow("im_transform", im_transform)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        import matplotlib.pyplot as plt

        # 可视化匹配点
        def draw_matches(img1, img2, pts1, pts2, status):
            img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
            img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
            matched_img = np.hstack((img1, img2))
            for (x1, y1), (x2, y2), s in zip(pts1, pts2, status):
                if s:
                    cv2.circle(matched_img, (int(x1), int(y1)), 5, (0, 255, 0), 1)
                    cv2.circle(matched_img, (int(x2) + img1.shape[1], int(y2)), 5, (0, 255, 0), 1)
                    cv2.line(matched_img, (int(x1), int(y1)), (int(x2) + img1.shape[1], int(y2)), (255, 0, 0), 1)
            plt.imshow(matched_img)
            plt.show()

        # 示例代码
        draw_matches(input_image1, input_image2, pts_src, pts_dst, status)

        # 重新检测特征点并匹配
        frame_tensor = self.fram_to_tensor(im_transform)
        pred = self.matching({**last_data, 'image1': frame_tensor})
        kpts1 = pred['keypoints1'][0].cpu().numpy()
        matches = pred['matches0'][0].cpu().numpy()

        valid = matches > -1
        mkpts0 = kpts0[valid]
        mkpts1 = kpts1[matches[valid]]

        confidence = pred['matching_scores0'][0].cpu().numpy()

        self.visualize_matches_3d(mkpts0, mkpts1)

        self.show_images(confidence, valid, input_image1, im_transform, kpts0, kpts1, mkpts0, mkpts1)

    def show_images(self, confidence, valid, last_frame, frame, kpts0, kpts1, mkpts0, mkpts1):
        # windowsname = f'{self.imagename1}--{self.imagename2}'
        # cv2.namedWindow(windowsname, cv2.WINDOW_NORMAL)
        # cv2.resizeWindow(windowsname, (1920 * 2, 1080))
        # color = cm.jet(confidence[valid])
        color = plt.cm.viridis(confidence[valid])
        text = [
            'SuperGlue',
            'Keypoints: {}:{}'.format(len(kpts0), len(kpts1)),
            'Matches: {}'.format(len(mkpts0))
        ]
        k_thresh = self.matching.superpoint.config['keypoint_threshold']
        m_thresh = self.matching.superglue.config['match_threshold']
        small_text = [
            'Keypoint Threshold: {:.4f}'.format(k_thresh),
            'Match Threshold: {:.2f}'.format(m_thresh),
            'Image Pair: {:06}:{:06}'.format(0, 1),
        ]
        # out = make_matching_plot_fast(
        #     last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
        #     path=None, show_keypoints=True, small_text=small_text)
        # cv2.imshow('SuperGlue matches', out)
        # cv2.imwrite(self.test_out_dir + '_00.jpg', out)
        out = make_matching_plot_fast(
            last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
            path=None, show_keypoints=True, small_text=small_text, show_line=True)
        # cv2.imshow(windowsname, out)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        cv2.imwrite(self.current_out_image_path, out)

    def process_images_in_folders(self, base_path):
        folder_names = os.listdir(base_path)
        folder_names.sort()
        for folder_name in folder_names:
            folder_path = os.path.join(base_path, folder_name)
            if not os.path.isdir(folder_path):
                continue

            image_path1 = os.path.join(folder_path, f"{folder_name}_image_1.png")
            image_path2 = os.path.join(folder_path, f"{folder_name}_image_2.png")

            if not os.path.exists(image_path1) or not os.path.exists(image_path2):
                print(f"Skipping pair: {image_path1}, {image_path2} (one or both files do not exist)")
                continue

            imagename1 = f"image_1.png"
            imagename2 = f"image_2.png"

            image1 = cv2.imread(image_path1)
            image2 = cv2.imread(image_path2)

            self.current_out_image_path = os.path.join(folder_path, f"{imagename1}_{imagename2}_SuperGlue.png")

            image1 = cv2.cvtColor(image1, cv2.COLOR_RGB2GRAY)
            image2 = cv2.cvtColor(image2, cv2.COLOR_RGB2GRAY)

            print(f"Processed pair: {image_path1}, {image_path2}")
            self.compare_images(image1, image2)

    def match_two(self):
        # folder_path = "/home/lxr/workdir/yd_deformation/script/feature_matching"
        # image_path1 = os.path.join(folder_path, f"497.jpg")
        # image_path2 = os.path.join(folder_path, f"497-2023-11-08 10:21:54.jpg")

        folder_path = "/home/lxr/database/dic/shaoxing/5073/5033"
        image_path1 = os.path.join(folder_path, f"2024525095421@003004.jpg")
        image_path2 = os.path.join(folder_path, f"2024525095502@002003.jpg")

        if not os.path.exists(image_path1) or not os.path.exists(image_path2):
            print(f"Skipping pair: {image_path1}, {image_path2} (one or both files do not exist)")

        imagename1 = f"image_1.png"
        imagename2 = f"image_2.png"

        image1 = cv2.imread(image_path1)
        image2 = cv2.imread(image_path2)

        self.current_out_image_path = os.path.join(folder_path, f"{imagename1}_{imagename2}_SuperGlue.png")

        image1 = cv2.cvtColor(image1, cv2.COLOR_RGB2GRAY)
        image2 = cv2.cvtColor(image2, cv2.COLOR_RGB2GRAY)

        # image1 = image1[:500,:500]
        # image2 = image2[:500,:500]

        print(f"Processed pair: {image_path1}, {image_path2}")
        self.compare_images(image1, image2)


if __name__ == '__main__':
    _analyse_image = analyse_image()
    base_path = '/home/lxr/workdir/yd_defect/shaoxing/database'
    base_path = "/home/lxr/workdir/yd_defect/shaoxing/pengshui"
    # _analyse_image.process_images_in_folders(base_path)
    _analyse_image.match_two()

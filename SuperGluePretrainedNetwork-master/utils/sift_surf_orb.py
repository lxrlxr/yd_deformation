import cv2
import numpy as np
import matplotlib.pyplot as plt


def load_images(img1_path, img2_path):
    img1 = cv2.imread(img1_path, cv2.IMREAD_GRAYSCALE)
    img2 = cv2.imread(img2_path, cv2.IMREAD_GRAYSCALE)
    return img1, img2


def sift_feature_matching(img1, img2):
    sift = cv2.SIFT_create()
    keypoints1, descriptors1 = sift.detectAndCompute(img1, None)
    keypoints2, descriptors2 = sift.detectAndCompute(img2, None)

    bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)
    matches = bf.match(descriptors1, descriptors2)
    matches = sorted(matches, key=lambda x: x.distance)

    img_matches = cv2.drawMatches(img1, keypoints1, img2, keypoints2, matches[:50], None,
                                  flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
    return img_matches


def surf_feature_matching(img1, img2):
    try:
        surf = cv2.xfeatures2d.SURF_create()
    except AttributeError:
        print("SURF is not available in your OpenCV installation.")
        return None

    keypoints1, descriptors1 = surf.detectAndCompute(img1, None)
    keypoints2, descriptors2 = surf.detectAndCompute(img2, None)

    bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)
    matches = bf.match(descriptors1, descriptors2)
    matches = sorted(matches, key=lambda x: x.distance)

    img_matches = cv2.drawMatches(img1, keypoints1, img2, keypoints2, matches[:50], None,
                                  flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
    return img_matches


def orb_feature_matching(img1, img2):
    orb = cv2.ORB_create()
    keypoints1, descriptors1 = orb.detectAndCompute(img1, None)
    keypoints2, descriptors2 = orb.detectAndCompute(img2, None)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(descriptors1, descriptors2)
    matches = sorted(matches, key=lambda x: x.distance)

    img_matches = cv2.drawMatches(img1, keypoints1, img2, keypoints2, matches[:50], None,
                                  flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
    return img_matches


def plot_matches(img1, img2, method='sift'):
    if method == 'sift':
        img_matches = sift_feature_matching(img1, img2)
    elif method == 'surf':
        img_matches = surf_feature_matching(img1, img2)
        if img_matches is None:
            return
    elif method == 'orb':
        img_matches = orb_feature_matching(img1, img2)
    else:
        raise ValueError("Method must be 'sift', 'surf', or 'orb'")

    plt.figure(figsize=(12, 6))
    plt.imshow(img_matches)
    plt.title(f'{method.upper()} Feature Matching')
    plt.show()

if __name__ == "__main__":
    img1_path = '/home/lxr/workdir/yd_defect/shaoxing/pengshui/1/1_image_1.png'
    img2_path = '/home/lxr/workdir/yd_defect/shaoxing/pengshui/1/1_image_2.png'

    img1, img2 = load_images(img1_path, img2_path)

    # Display SIFT matches
    plot_matches(img1, img2, method='sift')

    # Display SURF matches
    plot_matches(img1, img2, method='surf')

    # Display ORB matches
    plot_matches(img1, img2, method='orb')

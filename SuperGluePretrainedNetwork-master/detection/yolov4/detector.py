# -*- coding=utf-8 -*-
from ctypes import *
import math
import random
import time
import cv2
import numpy as np
import re
import os
import sys
import os.path as osp

dirPath = osp.dirname(osp.abspath(__file__))
dirPath = osp.dirname(dirPath)
dirPath = osp.dirname(dirPath)
if dirPath not in sys.path:
    sys.path.append(dirPath)

from utils.logger import setup_logger

logger = setup_logger(project_name='detector')


def sample(probs):
    s = sum(probs)
    probs = [a / s for a in probs]
    r = random.uniform(0, 1)
    for i in range(len(probs)):
        r = r - probs[i]
        if r <= 0:
            return i
    return len(probs) - 1


def c_array(ctype, values):
    arr = (ctype * len(values))()
    arr[:] = values
    return arr


class BOX(Structure):
    _fields_ = [("x", c_float),
                ("y", c_float),
                ("w", c_float),
                ("h", c_float)]


class DETECTION(Structure):
    _fields_ = [("bbox", BOX),
                ("classes", c_int),
                ("prob", POINTER(c_float)),
                ("mask", POINTER(c_float)),
                ("objectness", c_float),
                ("sort_class", c_int),
                ("uc", POINTER(c_float)),
                ("points", c_int),
                ("embeddings", POINTER(c_float)),
                ("embedding_size", c_int),
                ("sim", c_float),
                ("track_id", c_int)]


class DETNUMPAIR(Structure):
    _fields_ = [("num", c_int),
                ("dets", POINTER(DETECTION))]


class IMAGE(Structure):
    _fields_ = [("w", c_int),
                ("h", c_int),
                ("c", c_int),
                ("data", POINTER(c_float))]


class METADATA(Structure):
    _fields_ = [("classes", c_int),
                ("names", POINTER(c_char_p))]


lib = CDLL(dirPath + "/config/libdarknet.so", RTLD_GLOBAL)
hasGPU = True

lib.network_width.argtypes = [c_void_p]
lib.network_width.restype = c_int
lib.network_height.argtypes = [c_void_p]
lib.network_height.restype = c_int

copy_image_from_bytes = lib.copy_image_from_bytes
copy_image_from_bytes.argtypes = [IMAGE, c_char_p]


def network_width(net):
    return lib.network_width(net)


def network_height(net):
    return lib.network_height(net)


predict = lib.network_predict_ptr
predict.argtypes = [c_void_p, POINTER(c_float)]
predict.restype = POINTER(c_float)

if hasGPU:
    set_gpu = lib.cuda_set_device
    set_gpu.argtypes = [c_int]

init_cpu = lib.init_cpu

make_image = lib.make_image
make_image.argtypes = [c_int, c_int, c_int]
make_image.restype = IMAGE

get_network_boxes = lib.get_network_boxes
get_network_boxes.argtypes = [c_void_p, c_int, c_int, c_float, c_float, POINTER(c_int), c_int, POINTER(c_int), c_int]
get_network_boxes.restype = POINTER(DETECTION)

make_network_boxes = lib.make_network_boxes
make_network_boxes.argtypes = [c_void_p]
make_network_boxes.restype = POINTER(DETECTION)

free_detections = lib.free_detections
free_detections.argtypes = [POINTER(DETECTION), c_int]

free_batch_detections = lib.free_batch_detections
free_batch_detections.argtypes = [POINTER(DETNUMPAIR), c_int]

free_ptrs = lib.free_ptrs
free_ptrs.argtypes = [POINTER(c_void_p), c_int]

network_predict = lib.network_predict_ptr
network_predict.argtypes = [c_void_p, POINTER(c_float)]

reset_rnn = lib.reset_rnn
reset_rnn.argtypes = [c_void_p]

load_net = lib.load_network
load_net.argtypes = [c_char_p, c_char_p, c_int]
load_net.restype = c_void_p

load_net_custom = lib.load_network_custom
load_net_custom.argtypes = [c_char_p, c_char_p, c_int, c_int]
load_net_custom.restype = c_void_p

do_nms_obj = lib.do_nms_obj
do_nms_obj.argtypes = [POINTER(DETECTION), c_int, c_int, c_float]

do_nms_sort = lib.do_nms_sort
do_nms_sort.argtypes = [POINTER(DETECTION), c_int, c_int, c_float]

free_image = lib.free_image
free_image.argtypes = [IMAGE]

letterbox_image = lib.letterbox_image
letterbox_image.argtypes = [IMAGE, c_int, c_int]
letterbox_image.restype = IMAGE

load_meta = lib.get_metadata
lib.get_metadata.argtypes = [c_char_p]
lib.get_metadata.restype = METADATA

load_image = lib.load_image_color
load_image.argtypes = [c_char_p, c_int, c_int]
load_image.restype = IMAGE

rgbgr_image = lib.rgbgr_image
rgbgr_image.argtypes = [IMAGE]

predict_image = lib.network_predict_image
predict_image.argtypes = [c_void_p, IMAGE]
predict_image.restype = POINTER(c_float)

predict_image_letterbox = lib.network_predict_image_letterbox
predict_image_letterbox.argtypes = [c_void_p, IMAGE]
predict_image_letterbox.restype = POINTER(c_float)

network_predict_batch = lib.network_predict_batch
network_predict_batch.argtypes = [c_void_p, IMAGE, c_int, c_int, c_int,
                                  c_float, c_float, POINTER(c_int), c_int, c_int]
network_predict_batch.restype = POINTER(DETNUMPAIR)


class yolo_surface(object):
    def __init__(self, model_name):
        cfg_path = (dirPath + f"/model/detection/{model_name}.cfg").encode('utf-8')
        weight_path = (dirPath + f"/model/detection/{model_name}.weights").encode('utf-8')
        data_path = (dirPath + f"/model/detection/{model_name}.data").encode('utf-8')
        out_lines = []
        with open(data_path, "r") as fs:
            lines = fs.readlines()
            for line in lines:
                if line.startswith("names"):
                    line = f"names = {dirPath}/model/detection/{model_name}.names\n"
                out_lines.append(line)
        with open(data_path, "w") as fs:
            fs.writelines(out_lines)
        self.net = load_net_custom(cfg_path, weight_path, 0, 1)
        self.meta = load_meta(data_path)

    def detect(self, image, thresh=.5, hier_thresh=.5, nms=.45, debug=False, multi_scale_flag=False, iou_thresh=0.75):
        """
        Performs the meat of the detection
        """
        ret = self.detect_image(image, thresh, hier_thresh, nms, debug)
        if multi_scale_flag:
            cut_boxes = self.cut_detect_image(image)
            input_boxes = ret + cut_boxes
            if len(input_boxes) > 0:
                ret = self.box_nms(input_boxes, iou_thresh)
        return ret

    def detect_image(self, image, thresh=.5, hier_thresh=.5, nms=.45, debug=False):
        width = network_width(self.net)
        height = network_height(self.net)
        im = make_image(width, height, 3)
        image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image_resized = cv2.resize(image_rgb, (width, height),
                                   interpolation=cv2.INTER_LINEAR)
        copy_image_from_bytes(im, image_resized.tobytes())
        num = c_int(0)
        pnum = pointer(num)
        predict_image(self.net, im)
        dets = get_network_boxes(self.net, im.w, im.h, 0.3, 0.01, None, 0, pnum, 0)
        num = pnum[0]
        if nms:
            do_nms_sort(dets, num, self.meta.classes, nms)
        res = []
        for j in range(num):
            for i in range(self.meta.classes):
                if dets[j].prob[i] > 0:
                    b = dets[j].bbox
                    nameTag = self.meta.names[i]
                    left = b.x - b.w / 2
                    right = b.x + b.w / 2
                    top = b.y - b.h / 2
                    bot = b.y + b.h / 2
                    if left < 0:
                        left = 0
                    if right > im.w - 1:
                        right = im.w - 1
                    if top < 0:
                        top = 0
                    if bot > im.h - 1:
                        bot = im.h - 1
                    left *= (float(image.shape[1]) / width)
                    right *= (float(image.shape[1]) / width)
                    top *= (float(image.shape[0]) / height)
                    bot *= (float(image.shape[0]) / height)
                    if right <= left or bot <= top:
                        continue
                    res.append((nameTag, dets[j].prob[i], left, top, right, bot))
        res = sorted(res, key=lambda x: - x[1])
        logger.info(res)
        free_detections(dets, num)
        free_image(im)
        # if len(res) > 0:
        #     for label, confidence, left, top, right, bottom in res:
        #         left = int(left)
        #         top = int(top)
        #         right = int(right)
        #         bottom = int(bottom)
        #         cv2.rectangle(image, (left, top), (right, bottom), (0, 255, 0),2)
        #         cv2.putText(image, "{} [{:.2f}]".format(label, float(confidence)),
        #                     (left, top - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
        #                     (0, 255, 0), 2)
        #     cv2.imshow('temp', image)
        #     cv2.waitKey()
        #     cv2.destroyAllWindows()
        return res

    def box_nms(self, bboxes, iou_thresh):
        """

        :param bboxes: 检测框列表
        :param iou_thresh: IOU阈值
        :return:
        """
        boxes = [list(bbox[2:]) for bbox in bboxes]
        boxes = np.array(boxes).astype(int)
        x1 = boxes[:, 0]
        y1 = boxes[:, 1]
        x2 = boxes[:, 2]
        y2 = boxes[:, 3]
        areas = (y2 - y1) * (x2 - x1)

        # 结果列表
        result = []
        confidences = np.array([bbox[1] for bbox in bboxes])
        index = confidences.argsort()[::-1]  # 对检测框按照置信度进行从高到低的排序，并获取索引
        # 下面的操作为了安全，都是对索引处理
        while index.size > 0:
            # 当检测框不为空一直循环
            i = index[0]
            result.append(i)  # 将置信度最高的加入结果列表

            # 计算其他边界框与该边界框的IOU
            x11 = np.maximum(x1[i], x1[index[1:]])
            y11 = np.maximum(y1[i], y1[index[1:]])
            x22 = np.minimum(x2[i], x2[index[1:]])
            y22 = np.minimum(y2[i], y2[index[1:]])
            w = np.maximum(0, x22 - x11 + 1)
            h = np.maximum(0, y22 - y11 + 1)
            overlaps = w * h
            ious = overlaps / (areas[i] + areas[index[1:]] - overlaps)
            # 只保留满足IOU阈值的索引
            idx = np.where(ious <= iou_thresh)[0]
            index = index[idx + 1]  # 处理剩余的边框
        out_boxes = [bboxes[i] for i in result]
        return out_boxes

    def cut_detect_image(self, image):
        scale = 0.5
        black_image = np.zeros_like(image)
        cut_image = cv2.resize(image, (int(image.shape[1] * scale), int(image.shape[0] * scale)),
                               interpolation=cv2.INTER_AREA)
        x_start = (image.shape[1] - int(image.shape[1] * scale)) // 2
        x_end = x_start + int(image.shape[1] * scale)
        y_start = (image.shape[0] - int(image.shape[0] * scale)) // 2
        y_end = y_start + int(image.shape[0] * scale)
        black_image[y_start:y_end, x_start:x_end] = cut_image
        cut_boxes = self.detect(black_image)

        results = []
        point = np.array([x_start, y_start, x_start, y_start])
        for cut_box in cut_boxes:
            box = np.array(cut_box[2:]) - point
            box = 2 * box
            left, top, right, bot = box.tolist()
            if left < 0:
                left = 0
            if right > image.shape[1] - 1:
                right = image.shape[1] - 1
            if top < 0:
                top = 0
            if bot > image.shape[0] - 1:
                bot = image.shape[0] - 1
            if right <= left or bot <= top:
                continue
            box = [left, top, right, bot]
            box = list(cut_box[:2]) + box
            box = tuple(box)
            results.append(box)
        return results


if __name__ == "__main__":
    rootpath = "/home/lxr/database/watch/segmentation/test/test_picture"
    detector = yolo_surface("watch_sword")
    import os

    for root, sub_folders, files in os.walk(rootpath):
        for name in files:
            if not name.endswith('.jpg'):
                continue
            if name.__contains__("model"):
                continue
            image_path = osp.join(root, name)
            image = cv2.imread(image_path)
            t0 = time.time()
            res = detector.detect(image, thresh=.15, hier_thresh=.5, nms=.45, debug=False, multi_scale_flag=True,
                                  iou_thresh=0.75)
            logger.info(f"++++++++++++++++++cal angle cost time:{time.time() - t0}")
            if len(res) > 0:
                index = 0
                for label, confidence, left, top, right, bottom in res:
                    left = int(left)
                    top = int(top)
                    right = int(right)
                    bottom = int(bottom)
                    cut_image = image[top:bottom, left:right]
                    output_image_path = image_path.replace(".jpg", f"_cut{index}.jpg")
                    cv2.imwrite(output_image_path, cut_image)
                    index += 1

#! /usr/bin/env python3
"""
个人写的最简单的测试两张图片，检测特征点并匹配，并图像显示出来。
不包括从本地读取特征点信息等功能。
"""



import cv2
# import matplotlib.cm as cm
import matplotlib.pyplot as plt
import time
import os, os.path as osp
import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import torch
from models.matching import Matching
from models.utils import (make_matching_plot_fast, frame2tensor)

torch.set_grad_enabled(False)


class analyse_image:
    def __init__(self):
        self.device = 'cuda:0'
        config = {
            'superpoint': {
                'nms_radius': 4,
                'keypoint_threshold': 0.0005,
                'max_keypoints': 8000
            },
            'superglue': {
                'weights': 'outdoor',
                'sinkhorn_iterations': 20,
                'match_threshold': 0.2,
            }
        }
        self.matching = Matching(config).eval().to(self.device)
        self.keys = ['keypoints', 'scores', 'descriptors']
        self.resize_w = 1920
        self.resize_h = 1080
        # self.resize_w = 6700 // 2
        # self.resize_h = 5440 // 2

    def fram_to_tensor(self, frame):
        frame_tensor = frame2tensor(frame, self.device)
        return frame_tensor

    def resize_image(self, input_image):
        # input_image = cv2.resize(input_image, (self.resize_w, self.resize_h),
        #                          interpolation=cv2.INTER_AREA)
        # input_image = cv2.cvtColor(input_image, cv2.COLOR_RGB2GRAY)
        return input_image

    def compare_images(self, input_image1, input_image2):
        t0 = time.time()
        input_image1 = self.resize_image(input_image1)
        input_image2 = self.resize_image(input_image2)
        frame_tensor = self.fram_to_tensor(input_image1)
        last_data = self.matching.superpoint({'image': frame_tensor})

        #################################### 新增对比FB测试   #################################################
        # # 定义x和y的边界
        # x_min, x_max = 486, 782
        # y_min, y_max = 195, 314
        #
        # # 使用布尔索引找到满足条件的元素
        # condition = (last_data["keypoints"][0][:, 0] >= x_min) & (last_data["keypoints"][0][:, 0] <= x_max) & \
        #             (last_data["keypoints"][0][:, 1] >= y_min) & (last_data["keypoints"][0][:, 1] <= y_max)
        #
        # # 获取满足条件的元素的下标
        # filtered_indices = torch.nonzero(condition).squeeze()
        # last_data["keypoints"][0] = last_data["keypoints"][0][filtered_indices]
        # last_data["scores"] = (last_data["scores"][0][filtered_indices],)
        # last_data["descriptors"][0] = last_data["descriptors"][0][:,filtered_indices]

        #################################### END 新增对比FB测试   #################################################

        last_data = {k + '0': last_data[k] for k in self.keys}
        last_data['image0'] = frame_tensor

        frame_tensor = self.fram_to_tensor(input_image2)
        pred = self.matching({**last_data, 'image1': frame_tensor})
        kpts0 = last_data['keypoints0'][0].cpu().numpy()
        kpts1 = pred['keypoints1'][0].cpu().numpy()
        matches = pred['matches0'][0].cpu().numpy()

        valid = matches > -1
        mkpts0 = kpts0[valid]
        mkpts1 = kpts1[matches[valid]]

        print(f'{self.current_out_image_path}  cost time: {time.time() - t0}')

        """
        临时新增测试功能：测试super point + glue的单应性矩阵（Homography）结果
        """
        import numpy as np
        rows, cols = input_image2.shape[:2]
        # 转为整数
        pts_src = np.int32(mkpts0)
        pts_dst = np.int32(mkpts1)

        # 计算单应性矩阵
        h, status = cv2.findHomography(pts_dst, pts_src, cv2.RANSAC, 1.0)

        # 执行图像变换
        im_transform = cv2.warpPerspective(input_image2, h, (cols, rows))

        transform_path = "/home/lxr/workdir/yd_robot/src/yd_meter_detect_recog/error_data/Re_/1344_transform.jpg"
        cv2.imwrite(transform_path, im_transform)

        confidence = pred['matching_scores0'][0].cpu().numpy()
        self.show_images(confidence, valid, input_image1, input_image2, kpts0, kpts1, mkpts0, mkpts1)

    def show_images(self, confidence, valid, last_frame, frame, kpts0, kpts1, mkpts0, mkpts1):
        windowsname = f'{self.imagename1}--{self.imagename2}'
        # cv2.namedWindow(windowsname, cv2.WINDOW_NORMAL)
        # cv2.resizeWindow(windowsname, (1920 * 2, 1080))
        # color = cm.jet(confidence[valid])
        color = plt.cm.viridis(confidence[valid])
        text = [
            'SuperGlue',
            'Keypoints: {}:{}'.format(len(kpts0), len(kpts1)),
            'Matches: {}'.format(len(mkpts0))
        ]
        k_thresh = self.matching.superpoint.config['keypoint_threshold']
        m_thresh = self.matching.superglue.config['match_threshold']
        small_text = [
            'Keypoint Threshold: {:.4f}'.format(k_thresh),
            'Match Threshold: {:.2f}'.format(m_thresh),
            'Image Pair: {:06}:{:06}'.format(0, 1),
        ]
        # out = make_matching_plot_fast(
        #     last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
        #     path=None, show_keypoints=True, small_text=small_text)
        # cv2.imshow('SuperGlue matches', out)
        # cv2.imwrite(self.test_out_dir + '_00.jpg', out)
        out = make_matching_plot_fast(
            last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
            path=None, show_keypoints=True, small_text=small_text, show_line=True)
        # cv2.imshow(windowsname, out)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        cv2.imwrite(self.current_out_image_path, out)

    def get_images(self):
        # video_path = "/home/lxr/workdir/reid/openpose/sh3.mp4"
        # cap = cv2.VideoCapture(video_path)
        # ret, frame = cap.read()
        # self.compare_images(cap.read()[1],cap.read()[1])

        # image_path1 = "/home/lxr/database/deformation/贵州--水位测试/001.jpg"
        # image_path2 = "/home/lxr/database/deformation/贵州--水位测试/002.jpg"

        # image_path1 = "/home/lxr/database/deformation/贵州--水位测试/水位监测/预置位2/10.180.28.158_01_2023021908392042.jpeg"
        # image_path2 = "/home/lxr/database/deformation/贵州--水位测试/水位监测/预置位2/10.180.28.158_01_20230219090207557.jpeg"

        # image_path1 = "/home/lxr/database/deformation/贵州--水位测试/水位监测/预置位3/10.180.28.158_01_2023021909364221.jpeg"
        # image_path2 = "/home/lxr/database/deformation/贵州--水位测试/水位监测/预置位3/10.180.28.158_01_20230219093320807.jpeg"

        """
        对比两张图片
        """
        image_path1 = "/home/lxr/workdir/yd_robot/src/yd_meter_detect_recog/error_data/Re_/1344.jpg"
        image_path2 = "/home/lxr/workdir/yd_robot/src/yd_meter_detect_recog/error_data/Re_/1344_20230612124042.jpg"

        self.imagename1 = "1344"
        self.imagename2 = "1344_20230612124042"
        image1 = cv2.imread(image_path1)
        image2 = cv2.imread(image_path2)
        self.current_out_image_path = osp.join(osp.dirname(image_path1),
                                               self.imagename1 + "_" + self.imagename2 + "_SuperGlue.png")

        image1 = get_detection_area(image1)
        image2 = get_detection_area(image2)
        cv2.imwrite(osp.join(osp.dirname(image_path1), self.imagename1 + "_detection.jpg"), image1)
        cv2.imwrite(osp.join(osp.dirname(image_path2), self.imagename2 + "_detection.jpg"), image2)


        image1 = cv2.cvtColor(image1, cv2.COLOR_RGB2GRAY)
        image2 = cv2.cvtColor(image2, cv2.COLOR_RGB2GRAY)
        self.compare_images(image1, image2)

        """
        对比测试DIC相关功能
        """
        # image_dir = "/home/lxr/workdir/others/ncorr_2D_matlab/DIC拍摄表格+图片/10m/0.1mm"
        # image_dir = "/home/lxr/workdir/others/ncorr_2D_matlab/DIC拍摄表格+图片/10m/0.1mm-gray"
        # images = os.listdir(image_dir)
        # images = sorted(images)
        # for imagename1 in images:
        #     self.imagename1 = imagename1
        #     for imagename2 in images:
        #         self.imagename2 = imagename2
        #         if imagename1 == imagename2:
        #             continue
        #         image1 = cv2.imread(osp.join(image_dir,imagename1))[4640:10080,8780:15480,...]
        #         image2 = cv2.imread(osp.join(image_dir, imagename2))[4640:10080,8780:15480,...]
        #         self.compare_images(image1, image2)

        # import numpy as np
        # import pandas as pd
        #
        # output_dir = "/home/lxr/workdir/others/ncorr_2D_matlab/DIC拍摄表格+图片/10m/0.1mm-gray"
        # images = os.listdir(output_dir)
        # images = sorted(images)
        # for imagename1 in images:
        #     image1 = cv2.imread(osp.join(output_dir, imagename1), 0)[630:850, 1270:1500]
        #
        #     # 准备数据
        #     data_df = pd.DataFrame(image1)  # 关键1，将ndarray格式转换为DataFrame
        #
        #     # 更改表的索引
        #     # data_df.columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']  # 将第一行的0,1,2,...,9变成A,B,C,...,J
        #     # data_df.index = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
        #
        #     # 将文件写入excel表格中
        #     writer = pd.ExcelWriter(osp.join(output_dir, imagename1[:-4] + '.xlsx'))  # 关键2，创建名称为hhh的excel表格
        #     data_df.to_excel(writer, 'page_1',
        #                      float_format='%.5f')  # 关键3，float_format 控制精度，将data_df写到hhh表格的第一页中。若多个文件，可以在page_2中写入
        #     writer.save()  # 关键4

        # image_dir = "/home/lxr/workdir/others/ncorr_2D_matlab/DIC拍摄表格+图片/10m/0.1mm"
        # output_dir = "/home/lxr/workdir/others/ncorr_2D_matlab/DIC拍摄表格+图片/10m/0.1mm-gray"
        # if not os.path.exists(output_dir):
        #     os.makedirs(output_dir)
        # images = os.listdir(image_dir)
        # images = sorted(images)
        # for imagename1 in images:
        #     image1 = cv2.imread(osp.join(image_dir, imagename1),0)
        #     cv2.imwrite(osp.join(output_dir, imagename1),image1)

        # image_dir = "/home/lxr/database/deformation/verification/2022-12-16"
        # output_dir = "/home/lxr/database/deformation/verification/2022-12-16-SuperPixel"
        # if not os.path.exists(output_dir):
        #     os.makedirs(output_dir)
        # images = os.listdir(image_dir)
        # images = sorted(images)
        # for imagename1 in images:
        #     image1 = cv2.imread(osp.join(image_dir, imagename1))
        #     image1 = cv2.resize(image1, (self.resize_w * 10, self.resize_h * 10),
        #                          interpolation=cv2.INTER_AREA)
        #     cv2.imwrite(osp.join(output_dir, imagename1),image1)


        """
        用于FeatureBoost的对比测试
        """
        # # 遍历文件夹和子文件夹
        # diretory = "/home/lxr/workdir/others/FeatureBooster/hpatches_sequences/ours"
        # sub_dirs = os.listdir(diretory)
        # sub_dirs = sorted(sub_dirs)
        # for sub_dir in sub_dirs:
        #     image_path1 = osp.join(diretory,sub_dir, "1.jpg")
        #     self.imagename1 = "1"
        #     for idx in range(2,7):
        #         image_path2 = osp.join(diretory,sub_dir, str(idx) + ".jpg")
        #         self.imagename2 = str(idx)
        #         self.current_out_image_path = osp.join(diretory,sub_dir, self.imagename1 + "_" + self.imagename2 + "_SuperGlue.png")
        #         image1 = cv2.imread(image_path1,0)
        #         image2 = cv2.imread(image_path2,0)
        #         self.compare_images(image1, image2)


    def process_images_in_folders(self, base_path):
        for folder_name in os.listdir(base_path):
            folder_path = os.path.join(base_path, folder_name)
            if os.path.isdir(folder_path):
                # 获取文件夹中的所有图片
                images = [f for f in os.listdir(folder_path) if f.endswith(('.png', '.jpg', '.jpeg'))]
                images.sort()  # 按名称排序

                # 检查成对图像
                for i in range(0, len(images) - 1, 2):
                    image_path1 = os.path.join(folder_path, images[i])
                    image_path2 = os.path.join(folder_path, images[i + 1])

                    if not os.path.exists(image_path1) or not os.path.exists(image_path2):
                        print(f"Skipping pair: {image_path1}, {image_path2} (one or both files do not exist)")
                        continue

                    imagename1 = os.path.splitext(images[i])[0]
                    imagename2 = os.path.splitext(images[i + 1])[0]

                    image1 = cv2.imread(image_path1)
                    image2 = cv2.imread(image_path2)

                    self.current_out_image_path = os.path.join(folder_path, f"{imagename1}_{imagename2}_SuperGlue.png")

                    cv2.imwrite(os.path.join(folder_path, f"{imagename1}_detection.jpg"), image1)
                    cv2.imwrite(os.path.join(folder_path, f"{imagename2}_detection.jpg"), image2)

                    image1 = cv2.cvtColor(image1, cv2.COLOR_RGB2GRAY)
                    image2 = cv2.cvtColor(image2, cv2.COLOR_RGB2GRAY)

                    self.compare_images(image1, image2)
                    print(f"Processed pair: {image_path1}, {image_path2}")

def get_detection_area(img):
    import numpy as np
    from detection.yolov4 import detector
    watch_detector = detector.yolo_surface("watch_sword")
    res = watch_detector.detect(img, thresh=.25, hier_thresh=.5, nms=.45, debug=False, multi_scale_flag=True,
                                iou_thresh=0.75)
    if res is None or len(res) <= 0:
        return

    # 创建一个和原图一样大小的全黑的遮罩
    mask = np.zeros_like(img)
    # 遍历每一个区域
    for _, _, left, top, right, bot in res:
        mask[int(top):int(bot), int(left):int(right)] = 1

    # 将原图中对应的遮罩部分变为黑色
    img[mask == 0] = 0
    return img

if __name__ == '__main__':
    _analyse_image = analyse_image()
    # _analyse_image.get_images()

    base_path = '/home/lxr/workdir/yd_defect/shaoxing/database'
    _analyse_image.process_images_in_folders(base_path)
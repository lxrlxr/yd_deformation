import numpy as np
import cv2
from utils.logger import setup_logger

logger = setup_logger(project_name='class_filter')


def transimagebysift(srcimage, targetimage):
    '''
    srcimage: 实时图像
    targetimage: 原图
    '''
    # 定义输出为原始图像
    im_transform = srcimage
    try:
        # sift = cv2.xfeatures2d.SIFT_create()
        sift = cv2.SIFT_create(nfeatures=5000)
        FLANN_INDEX_KDTREE = 0
        index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
        search_params = dict(checks=50)
        flann = cv2.FlannBasedMatcher(index_params, search_params)

        kp1, des1 = sift.detectAndCompute(srcimage, None)
        kp2, des2 = sift.detectAndCompute(targetimage, None)
        rows, cols = targetimage.shape[:2]

        matches = flann.knnMatch(des1, des2, k=2)
        matchesMask = [[0, 0] for i in range(len(matches))]
        good = []
        pts_src = []
        pts_dst = []
        for m, n in matches:
            if m.distance < 0.5 * n.distance:
                good.append([m])
                pts_src.append(kp1[m.queryIdx].pt)
                pts_dst.append(kp2[m.trainIdx].pt)
        pts_src = np.int32(pts_src)
        pts_dst = np.int32(pts_dst)
        h, status = cv2.findHomography(pts_src, pts_dst, cv2.RANSAC, 1.0)

        im_transform = cv2.warpPerspective(srcimage, h, (cols, rows))
    except Exception:
        logger.error("error，原图与实时图像无法匹配校正", exc_info=True)

    return im_transform


if __name__ == '__main__':
    try:
        logger.info("test tsf start")
        img_path1 = "/home/lxr/workdir/yd_deformation/database/image/001.jpg"
        img_path2 = "/home/lxr/workdir/yd_deformation/database/image/002.jpg"
        output = transimagebysift(cv2.imread(img_path2), cv2.imread(img_path1))
        # cv2.imshow("",output)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        cv2.imwrite(img_path2.replace(".jpg", "_tsf.jpg"), output)
        logger.info("test tsf done")

    except Exception:
        logger.error("error", exc_info=True)

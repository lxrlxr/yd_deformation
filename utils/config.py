# encoding: utf-8
import json
from collections import abc
class DynamicJSON:
    """
    一个只读接口，使用属性表示法访问JSON类对象
    """
    def __init__(self, mapping):
        self.__data = dict(mapping)
    def __getattr__(self, name):
        # `__getattr__`特殊方法用于重载`.`符号获取值的行为
        if hasattr(self.__data, name):
            return getattr(self.__data, name)
        else:
            return DynamicJSON.build(self.__data[name])

    def __setattr__(self, key, value):
        if hasattr(self.__data, key):
            return setattr(self.__data,key,value)
        else:
            print('set object value failed')

    @classmethod
    def build(cls, obj):
        if isinstance(obj, abc.Mapping):
            return cls(obj)
        elif isinstance(obj, abc.MutableSequence):
            return [cls.build(item) for item in obj]
        else:
            return obj

from munch import DefaultMunch
class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)

def GetConfig(config_file_path):
    with open(config_file_path) as fp:
        raw_feed = json.load(fp)
    feed = DefaultMunch.fromDict(raw_feed)
    # feed = Struct(raw_feed)
    return feed


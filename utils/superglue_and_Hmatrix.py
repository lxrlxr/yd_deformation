import cv2
import torch
import numpy as np
from models.matching import Matching
from models.utils import (frame2tensor)
from utils.logger import setup_logger

torch.set_grad_enabled(False)
logger = setup_logger(project_name='deformation')


class analyse_image:
    def __init__(self):
        self.device = 'cuda:0'
        config = {
            'superpoint': {
                'nms_radius': 4,
                'keypoint_threshold': 0.0005,
                'max_keypoints': -1
            },
            'superglue': {
                'weights': 'outdoor',
                'sinkhorn_iterations': 20,
                'match_threshold': 0.4,
            }
        }
        self.matching = Matching(config).eval().to(self.device)

        self.resize_w = 960
        self.resize_h = 540
        self.resize_w = 1920
        self.resize_h = 1080

    def fram_to_tensor(self, frame):
        print(frame.shape[:])
        frame_tensor = frame2tensor(frame, self.device)
        return frame_tensor

    def resize_image(self, input_image):
        input_image = cv2.resize(input_image, (self.resize_w, self.resize_h), interpolation=cv2.INTER_AREA)
        # input_image = cv2.resize(input_image, (self.resize_w, self.resize_h), interpolation=cv2.INTER_CUBIC)
        input_image = cv2.cvtColor(input_image, cv2.COLOR_RGB2GRAY)
        return input_image

    def cal_keypoints(self, input_image):
        input_image = self.resize_image(input_image)
        frame_tensor = self.fram_to_tensor(input_image)
        data = self.matching.superpoint({'image': frame_tensor})
        data["image"] = input_image
        return data

    def cal_match(self, data0, data1):
        data0 = {**{}, **{k + '0': v for k, v in data0.items()}}
        data1 = {**{}, **{k + '1': v for k, v in data1.items()}}
        data = {**data0, **data1}

        for k in data:
            if isinstance(data[k], (list, tuple)):
                if k.__contains__('shape'):
                    continue
                data[k] = torch.stack(data[k])

        pred = {**data0, **data1, **self.matching.superglue(data)}

        return pred

    def split_pred(self, pred):
        mkpts0 = np.array([])
        mkpts1 = np.array([])
        try:
            kpts0 = pred['keypoints0'][0].cpu().numpy()
            kpts1 = pred['keypoints1'][0].cpu().numpy()
            matches = pred['matches0'][0].cpu().numpy()
            valid = matches > -1
            mkpts0 = kpts0[valid]
            mkpts1 = kpts1[matches[valid]]

        except Exception:
            logger.error("", exc_info=True)
        return mkpts0, mkpts1

    def cal_max_delta(self, mkpts0, mkpts1):
        delta_xy = np.abs(mkpts1 - mkpts0)
        delta_xy = np.linalg.norm(delta_xy, axis=1)
        max_delta_xy = np.max(delta_xy)
        return max_delta_xy

    def match(self, srcimage, targetimage):
        src_data = self.cal_keypoints(srcimage)
        target_data = self.cal_keypoints(targetimage)
        pred = self.cal_match(src_data, target_data)
        mkpts0, mkpts1 = self.split_pred(pred)
        return mkpts0, mkpts1


def transimagebysuper(srcimage, targetimage, x_min=0, x_max=0, y_min=0, y_max=0):
    """
    图像矫正
    Args:
        srcimage: 实时图像
        targetimage: 模板图像
    Returns:

    """
    _analyse_image = analyse_image()
    mkpts0, mkpts1 = _analyse_image.match(srcimage, targetimage)

    h, status = cv2.findHomography(mkpts0, mkpts1, cv2.RANSAC, 1.0)
    rows, cols = targetimage.shape[:2]
    im_transform = cv2.warpPerspective(srcimage, h, (cols, rows))
    # cv2.imwrite("./Hmatrix_temp/im_transform.jpg", im_transform)
    # cv2.imwrite("/home/lxr/workdir/yd_deformation/dist/502_tsf.jpg", im_transform)

    mkpts0, mkpts1 = _analyse_image.match(im_transform, targetimage)
    if x_max != 0 and y_max != 0:
        # 创建条件
        condition = (mkpts0[:, 0] >= x_min) & (mkpts0[:, 0] <= x_max) & (mkpts0[:, 1] >= y_min) & (
                    mkpts0[:, 1] <= y_max)
        # 使用np.where过滤数组
        indices = np.where(condition)
        valid_mkpts0 = mkpts0[indices]
        valid_mkpts1 = mkpts1[indices]
        if len(valid_mkpts0) > 0:
            mkpts0 = valid_mkpts0
            mkpts1 = valid_mkpts1
    max_delta_xy = _analyse_image.cal_max_delta(mkpts0, mkpts1)
    logger.info(f"max_delta_xy:{max_delta_xy}")

    return im_transform, max_delta_xy, mkpts0, mkpts1


if __name__ == '__main__':
    srcimage = "/home/lxr/database/dic/shaoxing/5073/5033/2024525095502@002003.jpg"
    targetimage = "/home/lxr/database/dic/shaoxing/5073/5033/2024525095421@003004.jpg"
    # srcimage = '/home/lxr/workdir/yd_deformation/dist/502-2023-11-21 11:10:57.jpg'
    # targetimage = '/home/lxr/workdir/yd_deformation/dist/502.jpg'
    img1 = cv2.imread(srcimage)
    img2 = cv2.imread(targetimage)
    # img1 = img1[:360,:640,...]
    # img2 = img2[:360,:640,...]
    cv2.imshow("img1", img1)
    cv2.waitKey(0)
    cv2.imshow("img2", img2)
    cv2.waitKey(0)
    # cv2.destroyAllWindows()
    im_transform, max_delta_xy, mkpts0, mkpts1 = transimagebysuper(img1, img2)
    cv2.imshow("im_transform", im_transform)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

#! /usr/bin/env python3
import sys


def isPointWithinBox(point, sbox, toler=0.0001):
    '''
    判断是否在框上
    Args:
        point:
        sbox:
        toler:

    Returns:True/False

    '''
    if point[0] > sbox[0][0] and point[0] < sbox[1][0] and point[1] > sbox[0][1] and point[1] < sbox[1][1]:
        return True
    if toler > 0:
        pass
    return False


def isRayIntersectsSegment(point, s_point, e_point):
    '''
    判断是否在框内
    Args:
        point:
        s_point:
        e_point:

    Returns:True/False

    '''
    if s_point[1] == e_point[1]:
        return False
    if s_point[1] > point[1] and e_point[1] > point[1]:
        return False
    if s_point[1] < point[1] and e_point[1] < point[1]:
        return False
    if s_point[1] == point[1] and e_point[1] > point[1]:
        return False
    if e_point[1] == point[1] and s_point[1] > point[1]:
        return False
    if s_point[0] < point[0] and e_point[1] < point[1]:
        return False
    xseg = e_point[0] - (e_point[0] - s_point[0]) * (e_point[1] - point[1]) / (e_point[1] - s_point[1])  # 求交
    if xseg < point[0]:
        return False
    return True


def isPointWithinSimplePoly(point, image_area, tolerance=0.0001):
    '''
    简单判断是否在框内。（简单判断已足够使用需要）
    Args:
        point:
        tolerance:

    Returns:True/False

    '''
    max_x = max_item = max(row[0] for row in image_area)
    min_x = max_item = min(row[0] for row in image_area)
    max_y = max_item = max(row[1] for row in image_area)
    min_y = max_item = min(row[1] for row in image_area)
    if not isPointWithinBox(point, [[min_x, min_y], [max_x, max_y]], tolerance):
        return False

    polylen = len(image_area)
    sinsc = 0
    for i in range(polylen - 1):
        s_point = image_area[i]
        e_point = image_area[i + 1]
        if isRayIntersectsSegment(point, s_point, e_point):
            sinsc += 1
    return True if sinsc % 2 == 1 else False


def isInterArea(point, image_area):  # testpoint为待测点[x,y]
    LBpoint = image_area[0]  # image_area为按顺时针顺序的4个点[[x1,y1],[x2,y2],[x3,y3],[x4,y4]]
    LTpoint = image_area[1]
    RTpoint = image_area[2]
    RBpoint = image_area[3]
    a = (LTpoint[0] - LBpoint[0]) * (point[1] - LBpoint[1]) - (LTpoint[1] - LBpoint[1]) * (
            point[0] - LBpoint[0])
    b = (RTpoint[0] - LTpoint[0]) * (point[1] - LTpoint[1]) - (RTpoint[1] - LTpoint[1]) * (
            point[0] - LTpoint[0])
    c = (RBpoint[0] - RTpoint[0]) * (point[1] - RTpoint[1]) - (RBpoint[1] - RTpoint[1]) * (
            point[0] - RTpoint[0])
    d = (LBpoint[0] - RBpoint[0]) * (point[1] - RBpoint[1]) - (LBpoint[1] - RBpoint[1]) * (
            point[0] - RBpoint[0])
    # print(a,b,c,d)
    if (a > 0 and b > 0 and c > 0 and d > 0) or (a < 0 and b < 0 and c < 0 and d < 0):
        return True
    else:
        return False

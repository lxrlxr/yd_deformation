#! /usr/bin/env python3
# coding=utf-8
import time
import numpy as np
import cv2
import math
import torch
from sklearn.cluster import KMeans
import matplotlib.cm as cm
from models.matching import Matching
from models.utils import (make_matching_plot_fast, frame2tensor)
from utils.transimage import transimagebysift

from utils.logger import setup_logger

logger = setup_logger(project_name='cal_deformation')
torch.set_grad_enabled(False)


class Analyse_image:
    def __init__(self, device, superpoint_nms, superpoint_thresh, superpoint_max_keypoints, superglue_weights,
                 superglue_sinkhorn_iterations, superglue_thresh, cluster_error_count, cluster_error_thresh,
                 image_area, resize_w, resize_h, scale, extend, voxel_extend, voxel_thresh):
        self.device = device
        config = {
            'superpoint': {
                'nms_radius': superpoint_nms,
                'keypoint_threshold': superpoint_thresh,
                'max_keypoints': superpoint_max_keypoints
            },
            'superglue': {
                'weights': superglue_weights,
                'sinkhorn_iterations': superglue_sinkhorn_iterations,
                'match_threshold': superglue_thresh,
            }
        }
        self.matching = Matching(config).eval().to(self.device)
        self.keys = ['keypoints', 'scores', 'descriptors']
        self.resize_w = resize_w
        self.resize_h = resize_h
        self.max_x = 0
        self.min_x = 0
        self.max_y = 0
        self.min_y = 0
        self.image_area = image_area
        self.scale = scale
        self.extend = extend
        self.cluster_error_count = cluster_error_count
        self.cluster_error_thresh = cluster_error_thresh
        self.cloud_extend = voxel_extend
        self.cloud_thresh = voxel_thresh

    def fram_to_tensor(self, frame):
        frame_tensor = frame2tensor(frame, self.device)
        return frame_tensor

    def resize_image(self, input_image, first_image_flag: bool = False, turn_color: bool = True):
        max_x = int(max(row[0] for row in self.image_area) + self.extend)
        min_x = int(min(row[0] for row in self.image_area) - self.extend)
        max_y = int(max(row[1] for row in self.image_area) + self.extend)
        min_y = int(min(row[1] for row in self.image_area) - self.extend)
        self.max_x = min(max_x, input_image.shape[1])
        self.min_x = max(min_x, 0)
        self.max_y = min(max_y, input_image.shape[0])
        self.min_y = max(min_y, 0)
        input_image = input_image[self.min_y:self.max_y, self.min_x:self.max_x]
        logger.info(f'选择区域：min x:{self.min_x},min y:{self.min_y},max x:{self.max_x},max y:{self.max_y}')
        if turn_color:
            input_image = cv2.cvtColor(input_image, cv2.COLOR_RGB2GRAY)
        if first_image_flag:
            t0 = time.time()
            import matplotlib.path as mpltPath
            path = mpltPath.Path(self.image_area)
            input_array = np.array([[i + self.min_x, j + self.min_y] for j in range(input_image.shape[0]) for i in
                                    range(input_image.shape[1])])
            inside = path.contains_points(input_array)
            # from utils.area import isInterArea
            for i in range(input_image.shape[1]):
                for j in range(input_image.shape[0]):
                    # if not isInterArea([i + self.min_x, j + self.min_y], self.image_area):
                    if not inside[j * input_image.shape[1] + i]:
                        input_image[j][i] = 255
            logger.info(f'area filter cost time:{time.time() - t0}')
        if self.resize_w == 0 and self.resize_h == 0 and self.scale == 1:
            self.resize_w = input_image.shape[1]
            self.resize_h = input_image.shape[0]
        else:
            self.resize_w = input_image.shape[1] if self.resize_w == 0 else self.resize_w
            self.resize_h = input_image.shape[0] if self.resize_h == 0 else self.resize_h
            self.resize_w = int(self.scale * self.resize_w)
            self.resize_h = int(self.scale * self.resize_h)
            input_image = cv2.resize(input_image, (self.resize_w, self.resize_h),
                                     interpolation=cv2.INTER_AREA)
        logger.info(f'图片缩放比例：{self.scale}，最终输出size：x:{self.resize_w},y:{self.resize_h}')
        return input_image

    def cal_keypoints(self, input_image, first_image_flag: bool = False):
        input_image = self.resize_image(input_image, first_image_flag)
        frame_tensor = self.fram_to_tensor(input_image)
        data = self.matching.superpoint({'image': frame_tensor})
        data['shape'] = (0, 0, self.resize_w, self.resize_h)
        return data, input_image

    def save_keypoints(self, data, image_path):
        keypoints = data['keypoints'][0].cpu().detach().numpy()
        np.save(image_path.replace('.jpg', '-keypoint.npy'), keypoints)
        scores = data['scores'][0].cpu().detach().numpy()
        np.save(image_path.replace('.jpg', '-scores.npy'), scores)
        descriptors = data['descriptors'][0].cpu().detach().numpy()
        np.save(image_path.replace('.jpg', '-descriptors.npy'), descriptors)

    def read_npy(self, image_path):
        np_path = image_path.replace('.jpg', '/')
        np_keypoints = np.load(image_path.replace('.jpg', '-keypoint.npy'))
        keypoints = torch.from_numpy(np_keypoints).to(self.device)
        np_scores = np.load(image_path.replace('.jpg', '-scores.npy'))
        scores = torch.from_numpy(np_scores).to(self.device)
        np_descriptors = np.load(image_path.replace('.jpg', '-descriptors.npy'))
        descriptors = torch.from_numpy(np_descriptors).to(self.device)
        self.resize_image(cv2.imread(image_path))
        data = {
            'keypoints': [keypoints],
            'scores': (scores,),
            'descriptors': [descriptors],
            'shape': (0, 0, self.resize_w, self.resize_h)
        }
        return data

    def save_result_images(self, confidence, valid, last_frame, frame, kpts0, kpts1, mkpts0, mkpts1,
                           output_image_path: str = None):
        color = cm.jet(confidence[valid])
        text = [
            'SuperGlue',
            'Keypoints: {}:{}'.format(len(kpts0), len(kpts1)),
            'Matches: {}'.format(len(mkpts0))
        ]
        k_thresh = self.matching.superpoint.config['keypoint_threshold']
        m_thresh = self.matching.superglue.config['match_threshold']
        small_text = [
            'Keypoint Threshold: {:.4f}'.format(k_thresh),
            'Match Threshold: {:.2f}'.format(m_thresh),
            'Image Pair: {:06}:{:06}'.format(0, 1),
        ]
        out = make_matching_plot_fast(
            last_frame, frame, kpts0, kpts1, mkpts0, mkpts1, color, text,
            path=None, show_keypoints=True, small_text=small_text, show_line=True)
        cv2.imwrite(output_image_path.replace('.jpg', '-result.jpg'), out)

    def cal_scale(self, cloud, x, y, scale_extend, thresh):
        points = []
        for point in cloud:
            if abs(point[0] - x) >= scale_extend or abs(point[1] - y) >= scale_extend:
                continue
            if point[2] == 0 and point[3] == 0 and point[4] == 0:
                continue
            points.append((point[2], point[3], point[4], point[0], point[1]))
        # for i in range(-scale_extend, scale_extend + 1):
        #     for j in range(-scale_extend, scale_extend + 1):
        #         if cloud[y + j][x + i][0] == 0 and cloud[y + j][x + i][1] == 0 and cloud[y + j][x + i][2] == 0:
        #             continue
        #         points.append([cloud[y + j][x + i][0], cloud[y + j][x + i][1], cloud[y + j][x + i][2], i, j])
        scale = 0
        cnt = 0
        for point in points:
            for point1 in points:
                if point == point1:
                    continue
                length = math.sqrt(
                    (point[0] - point1[0]) ** 2 + (point[1] - point1[1]) ** 2 + (point[2] - point1[2]) ** 2)
                if length >= thresh:
                    continue
                scale += length / math.sqrt((point[3] - point1[3]) ** 2 + (point[4] - point1[4]) ** 2)
                cnt += 1
        scale = scale / cnt if cnt > 0 else 0
        if scale == 0:
            output_3d = [0, 0, 0]
        else:
            output_3d = [points[0][0], points[0][1], points[0][2]]
        return scale, output_3d

    def deformation(self, new_image_path, ori_image_path, origin_data, input_image1, yaml_path, py3d, adjustor,
                    erode_pixels,
                    deformation_thresh):
        # 定义输出
        out_min_dfmt, out_max_dfmt, out_average_dfmt = 0, 0, 0
        min_3d, max_3d, average_3d = [0, 0, 0], [0, 0, 0], [0, 0, 0]

        try:
            or_image2 = cv2.imread(new_image_path)
            # or_image2 = transimagebysift(or_image2, cv2.imread(ori_image_path))
            from utils.superglue_and_Hmatrix import transimagebysuper
            or_image2, max_delta_xy, mkpts0, mkpts1 = transimagebysuper(or_image2,cv2.imread(ori_image_path))
            image = or_image2[self.min_y:self.max_y, self.min_x:self.max_x]
            image = cv2.resize(image, (self.resize_w, self.resize_h), interpolation=cv2.INTER_AREA)
            # image = self.resize_image(or_image2, turn_color=False)
            image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
            frame_tensor = self.fram_to_tensor(image)
            data1 = self.matching.superpoint({'image': frame_tensor})
            data1['shape'] = (0, 0, self.resize_w, self.resize_h)
            data1 = {**{}, **{k + '1': v for k, v in data1.items()}}

            data = {**origin_data, **data1}
            logger.info(f'keypoints0 num:{data["keypoints0"][0].size()}')
            logger.info(f'keypoints1 num:{data["keypoints1"][0].size()}')
            for k in data:
                if isinstance(data[k], (list, tuple)):
                    if k.__contains__('shape'):
                        continue
                    data[k] = torch.stack(data[k])

            pred = {**data1, **self.matching.superglue(data)}

            kpts0 = origin_data['keypoints0'][0].cpu().detach().numpy()
            kpts1 = pred['keypoints1'][0].cpu().detach().numpy()
            matches = pred['matches0'][0].cpu().detach().numpy()

            valid = matches > -1
            mkpts0 = kpts0[valid]
            mkpts1 = kpts1[matches[valid]]

            if mkpts0.shape[0] <= 0:
                logger.info(f"没有匹配上的特征点")
                return out_min_dfmt, out_max_dfmt, out_average_dfmt, min_3d, max_3d, average_3d

            confidence = pred['matching_scores0'][0].cpu().detach().numpy()

            self.save_result_images(confidence, valid, input_image1, image, kpts0, kpts1, mkpts0, mkpts1,
                                    output_image_path=new_image_path)

            tmp_mkpts0, tmp_mkpts1 = [], []
            for i in range(mkpts0.shape[0]):
                # if not isPoiWithinSimplePoly([mkpts0[i][0], mkpts0[i][1]], self.image_area):
                #     continue
                x = mkpts0[i][0] - mkpts1[i][0]
                y = mkpts0[i][1] - mkpts1[i][1]
                if math.sqrt(x ** 2 + y ** 2) <= deformation_thresh:
                    continue
                tmp_mkpts0.append(mkpts0[i])
                tmp_mkpts1.append(mkpts1[i])
            mkpts0, mkpts1 = np.array(tmp_mkpts0), np.array(tmp_mkpts1)

            logger.info('py3d.get_3d_mat')
            x_scale = (self.max_x - self.min_x) / image.shape[1]
            y_scale = (self.max_y - self.min_y) / image.shape[0]
            mkpts0[:, 0] *= x_scale
            mkpts0[:, 1] *= y_scale
            mkpts1[:, 0] *= x_scale
            mkpts1[:, 1] *= y_scale
            mkpts0[:, 0] += self.min_x
            mkpts0[:, 1] += self.min_y
            mkpts1[:, 0] += self.min_x
            mkpts1[:, 1] += self.min_y
            inputs = mkpts0.tolist()
            inputs += mkpts1.tolist()
            outputs = py3d.get_3d_mat(adjustor, yaml_path, self.min_x, self.min_y, self.max_x, self.max_y, inputs,
                                      erode_pixels)
            start_index = len(outputs) - 2 * mkpts0.shape[0]
            cloud_3d = outputs[:start_index]
            mkpts0 = np.array(outputs[start_index:start_index + mkpts0.shape[0]])
            mkpts1 = np.array(outputs[start_index + mkpts0.shape[0]:])
            logger.info('py3d.get_3d_mat suc')

            # distortion to undistortion
            # for mkpt0 in mkpts0:
            #     # mkpt0[0], mkpt0[1] = py3d.get_distortion_point(adjustor, int(mkpt0[0]), int(mkpt0[1]))
            #     x, y = py3d.get_distortion_point(adjustor,yaml_path, mkpt0[0], mkpt0[1])
            # for mkpt1 in mkpts1:
            #     # mkpt1[0], mkpt1[1] = py3d.get_distortion_point(adjustor, int(mkpt1[0]), int(mkpt1[1]))
            #     x, y = py3d.get_distortion_point(adjustor,yaml_path, mkpt1[0], mkpt1[1])

            distances = []
            for i in range(mkpts0.shape[0]):
                # if not isPoiWithinSimplePoly([mkpts0[i][0], mkpts0[i][1]], self.image_area):
                #     continue
                x = mkpts0[i][0] - mkpts1[i][0]
                y = mkpts0[i][1] - mkpts1[i][1]
                distances.append(math.sqrt(x ** 2 + y ** 2))

            if len(distances) <= self.cluster_error_count:
                logger.info(f'匹配数目：{len(distances)}')
                return out_min_dfmt, out_max_dfmt, out_average_dfmt, min_3d, max_3d, average_3d
            c_sorted_id = sorted(range(len(distances)), key=lambda k: distances[k])
            distances = sorted(distances)
            logger.info(f'distances:{distances}')
            np_c = np.array(distances)
            # max_c = max(np_c)
            # min_c = min(np_c)
            # len_c = (max_c - min_c) / 2
            # np_c = (np_c - min_c) / len_c - 1
            np_c = np_c.reshape(-1, 1)
            km = KMeans(n_clusters=2)
            m = km.fit(np_c)
            logger.info(f'km.cluster_centers_:{km.cluster_centers_.tolist()}')
            counts = []
            for cluster in range(km.cluster_centers_.shape[0]):
                count = np.where(km.labels_ == cluster)[0].shape[0]
                counts.append(count)
            logger.info(f'counts:{counts}')

            sorted_id = sorted(range(len(counts)), key=lambda k: counts[k], reverse=True)
            cluster_count = 0
            out_min_dfmt = 2 ** 16
            min_id, max_id, average_id = 0, 0, 0
            logger.info(
                f"len(distances) * self.cluster_error_thresh:{len(distances) * self.cluster_error_thresh},    self.cluster_error_count:{self.cluster_error_count}")
            for i in range(len(sorted_id)):
                if counts[sorted_id[i]] < len(distances) * self.cluster_error_thresh \
                        or counts[sorted_id[i]] < self.cluster_error_count:
                    continue
                # temp_dfmt = (km.cluster_centers_[sorted_id[i]][0] + 1) * len_c
                temp_dfmt = km.cluster_centers_[sorted_id[i]][0]
                if out_min_dfmt > temp_dfmt:
                    out_min_dfmt = temp_dfmt
                    min_id = sorted_id[i]
                if out_max_dfmt < temp_dfmt:
                    out_max_dfmt = temp_dfmt
                    max_id = sorted_id[i]
                average_id = sorted_id[i]
                if temp_dfmt < 1:
                    continue
                cluster_count += 1
                out_average_dfmt += temp_dfmt
            if cluster_count != 0:
                out_average_dfmt /= cluster_count
            out_min_dfmt = 0 if out_min_dfmt == 2 ** 16 else out_min_dfmt

            logger.info('start compute scale')
            min_scale, max_scale, average_scale = 0, 0, 0

            index = 0
            for label in km.labels_:
                if label == min_id:
                    x = int(mkpts0[c_sorted_id[index]][0])
                    y = int(mkpts0[c_sorted_id[index]][1])
                    min_scale, min_3d = self.cal_scale(cloud_3d, x, y, self.cloud_extend, self.cloud_thresh)
                    if min_scale > 0:
                        break
                index += 1
            index = 0
            for label in km.labels_:
                if label == max_id:
                    x = int(mkpts0[c_sorted_id[index]][0])
                    y = int(mkpts0[c_sorted_id[index]][1])
                    max_scale, max_3d = self.cal_scale(cloud_3d, x, y, self.cloud_extend, self.cloud_thresh)
                    if max_scale > 0:
                        break
                index += 1
            index = 0
            for label in km.labels_:
                if label == average_id:
                    x = int(mkpts0[c_sorted_id[index]][0])
                    y = int(mkpts0[c_sorted_id[index]][1])
                    average_scale, average_3d = self.cal_scale(cloud_3d, x, y, self.cloud_extend, self.cloud_thresh)
                    if average_scale > 0:
                        break
                index += 1

            logger.info(
                f'形变大小out_min_dfmt:{out_min_dfmt},out_max_dfmt:{out_max_dfmt},out_average_dfmt:{out_average_dfmt}')
            min_scale, max_scale, average_scale = min_scale * 1000, max_scale * 1000, average_scale * 1000
            logger.info(
                f'比例尺min_scale:{min_scale},max_scale:{max_scale},average_scale:{average_scale}')
            out_min_dfmt, out_max_dfmt, out_average_dfmt = min_scale * out_min_dfmt, max_scale * out_max_dfmt, average_scale * out_average_dfmt
            out_min_dfmt, out_max_dfmt, out_average_dfmt = abs(round(out_min_dfmt, 2)), abs(
                round(out_max_dfmt, 2)), abs(
                round(out_average_dfmt, 2))
            logger.info(
                f'输出形变大小out_min_dfmt:{out_min_dfmt},out_max_dfmt:{out_max_dfmt},out_average_dfmt:{out_average_dfmt}')
        except Exception:
            logger.error("error", exc_info=True)

        return out_min_dfmt, out_max_dfmt, out_average_dfmt, min_3d, max_3d, average_3d

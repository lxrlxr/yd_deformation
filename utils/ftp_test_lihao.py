import ftplib
import ssl
import socket


class FTP_TLS(ftplib.FTP_TLS):
    # def __init__(self, host='', user='', passwd='', acct='', keyfile=None, certfile=None, timeout=180):
    def __init__(self, host='', user='', passwd='', acct='', keyfile=None, certfile=None, context=None, timeout=180, source_address=None):
        ftplib.FTP_TLS.__init__(self, host, user, passwd,
                                acct, keyfile, certfile, context, timeout)

    def connect(self, host='', port=0, timeout=-999):
        if host != '':
            self.host = host
        if port > 0:
            self.port = port
        if timeout != -999:
            self.timeout = timeout

        try:
            self.sock = socket.create_connection(
                (self.host, self.port), self.timeout)
            self.sock = ssl.wrap_socket(
                self.sock, self.keyfile, self.certfile, ssl_version=ssl.PROTOCOL_TLSv1_2)
            self.af = self.sock.family
            self.file = self.sock.makefile('rb')
            self.welcome = self.getresp()
        except Exception as e:
            print(e)
        return self.welcome

    # def storbinary(self, cmd, fp, blocksize=8192, callback=None, rest=None):
    #     self.voidcmd('TYPE I')
    #     conn = self.transfercmd(cmd, rest)
    #     try:
    #         while 1:
    #             buf = fp.read(blocksize)
    #             if not buf:
    #                 break
    #             conn.sendall(buf)
    #             if callback:
    #                 callback(buf)
    #         # shutdown ssl layer
    #         if isinstance(conn, ssl.SSLSocket):
    #             pass
    #             # conn.unwrap()
    #     finally:
    #         conn.close()
    #     return self.voidresp()


if __name__ == '__main__':
    ftps = FTP_TLS()
    ftps.set_debuglevel(2)
    #ftps.connect('192.168.1.237', 21)
    #ftps.login('ftp_test', '1')
    ftps.connect('192.168.1.35', 21)
    print('connect suc')
    ftps.login('ydrobot', '123qweasd')
    # ftps.prot_p()
    # ftps.set_pasv(0)
    # filename = "/home/yd/20220329153234.jpg"
    # remote_file = '/home/ydrobot/20220329153234.jpg'
    # myfile = open(filename, 'rb')
    # ftps.storbinary('STOR /home/ydrobot/20220329153234.jpg',
    #                 open(filename, 'rb'))
    ftps.quit()
    myfile.close()

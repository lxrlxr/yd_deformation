# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
import functools
import logging
import os
import sys
import time
from collections import Counter
from .file_io import PathManager
from termcolor import colored
import os.path as osp
from os.path import join, getsize

plain_formatter = logging.Formatter(
    "<HR Size=1><HR COLOR=blue>时间：%(asctime)s 行数：%(lineno)d %(name)s <BR>\r\n日志等级：%(levelname)s <BR>\r\n进程名称：%(threadName)s 当前执行程序名称：%(filename)s 当前执行函数名称：%(funcName)s <BR>\r\n日志信息：%(message)s <BR>")

logger_list = []


def getdirsize(dir):
    size = 0
    for root, dirs, files in os.walk(dir):
        size += sum([getsize(join(root, name)) for name in files])
    return size


def del_old_image(logger, dir_path):
    try:
        time0 = time.time()
        picturepathdir = osp.join(dir_path, 'database')
        picture_size = getdirsize(picturepathdir)
        if picture_size > 2 * 1024 * 1024 * 1024:
            dir_list = os.listdir(picturepathdir)
            dir_list = filter(lambda x: len(str(x).split('-')) >= 4, dir_list)
            dir_list = sorted(dir_list, key=lambda x: os.path.getctime(
                os.path.join(picturepathdir, x)))
            index = 0
            for file in dir_list:
                os.remove(os.path.join(picturepathdir, file))
                index += 1
                if index > dir_list.__len__() / 2:
                    break

            logger.info('删除过量图片数据耗时：%s' % str(time.time() - time0))
    except Exception:
        logger.error("error", exc_info=True)


def get_last_log(dir_path):
    dir_log_path = osp.join(dir_path, 'log')
    log_path_list = os.listdir(dir_log_path)
    max_index = 0
    max_index_name = f'{time.strftime("%Y%m%d", time.localtime())}_001.html'
    log_name_head = time.strftime("%Y%m%d", time.localtime())
    for log_path in log_path_list:
        if log_path.startswith(log_name_head):
            log_params = osp.splitext(log_path)[0].split('_')
            log_index = int(log_params[1])
            if max_index <= log_index:
                max_index = log_index
                max_index_name = log_path
    return max_index_name, max_index


def rename_log(logger, dir_path):
    try:
        if f'{time.strftime("%Y%m%d", time.localtime())}' != \
                osp.basename(logger_list[0].handlers[1].stream.name).split('_')[0]:
            new_log_path = osp.join(dir_path, 'log', f'{time.strftime("%Y%m%d", time.localtime())}_001.html')
            for logger_t in logger_list:
                logger_t.removeHandler(logger_t.handlers[1])
                fh = logging.StreamHandler(_cached_log_stream(new_log_path))
                fh.setLevel(logging.DEBUG)
                fh.setFormatter(plain_formatter)
                logger_t.addHandler(fh)
            return
        last_log_name, last_index = get_last_log(dir_path)
        last_log_name = osp.join(dir_path, 'log', last_log_name)
        log_size = getsize(last_log_name)
        if log_size > 500 * 1024:
            new_index = str(last_index + 1)
            while len(new_index) < 3:
                new_index = '0' + new_index
            new_log_path = osp.join(dir_path, 'log', f'{time.strftime("%Y%m%d", time.localtime())}_{new_index}.html')
            for logger_t in logger_list:
                logger_t.removeHandler(logger_t.handlers[1])
                fh = logging.StreamHandler(_cached_log_stream(new_log_path))
                fh.setLevel(logging.DEBUG)
                fh.setFormatter(plain_formatter)
                logger_t.addHandler(fh)
    except Exception:
        logger.error("error", exc_info=True)


def del_old_log(logger, dir_path):
    try:
        time0 = time.time()
        logpathdir = osp.join(dir_path, 'log')
        dir_list = os.listdir(logpathdir)
        dir_list = sorted(dir_list, key=lambda x: os.path.getctime(
            os.path.join(logpathdir, x)))
        for file in dir_list:
            interval = time0 - os.path.getctime(os.path.join(logpathdir, file))
            interval = interval / 60
            interval = interval / 60
            interval = interval / 24
            if interval > 7:
                os.remove(os.path.join(logpathdir, file))
        logger.info('删除过量日志数据耗时：%s' % str(time.time() - time0))
    except Exception:
        logger.error("error", exc_info=True)


def clear_data(logger):
    dir_path = osp.dirname(osp.dirname(osp.abspath(__file__)))
    del_old_image(logger, dir_path)
    del_old_log(logger, dir_path)
    rename_log(logger, dir_path)
    return logger


class _ColorfulFormatter(logging.Formatter):
    def __init__(self, *args, **kwargs):
        self._root_name = kwargs.pop("root_name") + "."
        self._abbrev_name = kwargs.pop("abbrev_name", "")
        if len(self._abbrev_name):
            self._abbrev_name = self._abbrev_name + "."
        super(_ColorfulFormatter, self).__init__(*args, **kwargs)

    def formatMessage(self, record):
        record.name = record.name.replace(self._root_name, self._abbrev_name)
        log = super(_ColorfulFormatter, self).formatMessage(record)
        if record.levelno == logging.WARNING:
            prefix = colored("WARNING", "red", attrs=["blink"])
        elif record.levelno == logging.ERROR or record.levelno == logging.CRITICAL:
            prefix = colored("ERROR", "red", attrs=["blink", "underline"])
        else:
            return log
        return prefix + " " + log


@functools.lru_cache()  # so that calling setup_logger multiple times won't add many handlers
def setup_logger(
        output=None, distributed_rank=0, *, color=True, project_name="lxr", module_name=None
):
    """
    Args:
        output (str): a file name or a directory to save log. If None, will not save log file.
            If ends with ".txt" or ".log", assumed to be a file name.
            Otherwise, logs will be saved to `output/log.txt`.
        project_name (str): the root module name of this logger
        module_name (str): an abbreviation of the module, to avoid long names in logs.
            Set to "" to not log the root module in logs.
            By default, will abbreviate "detectron2" to "d2" and leave other
            modules unchanged.
    """
    dir_path = osp.dirname(osp.dirname(osp.abspath(__file__)))
    filename, last_index = get_last_log(dir_path)
    filename = osp.join(dir_path, 'log', filename)
    logger = logging.getLogger(project_name)
    logger.setLevel(logging.DEBUG)
    logger.propagate = False

    if module_name is None:
        module_name = "lxr" if project_name == "lxr" else project_name

    # stdout logging: master only
    if distributed_rank == 0:
        ch = logging.StreamHandler(stream=sys.stdout)
        ch.setLevel(logging.DEBUG)
        if color:
            formatter = _ColorfulFormatter(
                colored("[%(asctime)s %(name)s]: ", "green") + "%(message)s",
                datefmt="%m/%d %H:%M:%S",
                root_name=project_name,
                abbrev_name=str(module_name),
            )
        else:
            formatter = plain_formatter
        ch.setFormatter(formatter)
        logger.addHandler(ch)

    PathManager.mkdirs(os.path.dirname(filename))

    fh = logging.StreamHandler(_cached_log_stream(filename))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(plain_formatter)
    logger.addHandler(fh)

    global logger_list
    logger_list.append(logger)
    return logger


# cache the opened file object, so that different calls to `setup_logger`
# with the same file name can safely write to the same file.
@functools.lru_cache(maxsize=None)
def _cached_log_stream(filename):
    return PathManager.open(filename, "a")


"""
Below are some other convenient logging methods.
They are mainly adopted from
https://github.com/abseil/abseil-py/blob/master/absl/logging/__init__.py
"""


def _find_caller():
    """
    Returns:
        str: module name of the caller
        tuple: a hashable key to be used to identify different callers
    """
    frame = sys._getframe(2)
    while frame:
        code = frame.f_code
        if os.path.join("utils", "logger.") not in code.co_filename:
            mod_name = frame.f_globals["__name__"]
            if mod_name == "__main__":
                mod_name = "detectron2"
            return mod_name, (code.co_filename, frame.f_lineno, code.co_name)
        frame = frame.f_back


_LOG_COUNTER = Counter()
_LOG_TIMER = {}


def log_first_n(lvl, msg, n=1, *, name=None, key="caller"):
    """
    Log only for the first n times.
    Args:
        lvl (int): the logging level
        msg (str):
        n (int):
        name (str): name of the logger to use. Will use the caller's module by default.
        key (str or tuple[str]): the string(s) can be one of "caller" or
            "message", which defines how to identify duplicated logs.
            For example, if called with `n=1, key="caller"`, this function
            will only log the first call from the same caller, regardless of
            the message content.
            If called with `n=1, key="message"`, this function will log the
            same content only once, even if they are called from different places.
            If called with `n=1, key=("caller", "message")`, this function
            will not log only if the same caller has logged the same message before.
    """
    if isinstance(key, str):
        key = (key,)
    assert len(key) > 0

    caller_module, caller_key = _find_caller()
    hash_key = ()
    if "caller" in key:
        hash_key = hash_key + caller_key
    if "message" in key:
        hash_key = hash_key + (msg,)

    _LOG_COUNTER[hash_key] += 1
    if _LOG_COUNTER[hash_key] <= n:
        logging.getLogger(name or caller_module).log(lvl, msg)


def log_every_n(lvl, msg, n=1, *, name=None):
    """
    Log once per n times.
    Args:
        lvl (int): the logging level
        msg (str):
        n (int):
        name (str): name of the logger to use. Will use the caller's module by default.
    """
    caller_module, key = _find_caller()
    _LOG_COUNTER[key] += 1
    if n == 1 or _LOG_COUNTER[key] % n == 1:
        logging.getLogger(name or caller_module).log(lvl, msg)


def log_every_n_seconds(lvl, msg, n=1, *, name=None):
    """
    Log no more than once per n seconds.
    Args:
        lvl (int): the logging level
        msg (str):
        n (int):
        name (str): name of the logger to use. Will use the caller's module by default.
    """
    caller_module, key = _find_caller()
    last_logged = _LOG_TIMER.get(key, None)
    current_time = time.time()
    if last_logged is None or current_time - last_logged >= n:
        logging.getLogger(name or caller_module).log(lvl, msg)
        _LOG_TIMER[key] = current_time

# def create_small_table(small_dict):
#     """
#     Create a small table using the keys of small_dict as headers. This is only
#     suitable for small dictionaries.
#     Args:
#         small_dict (dict): a result dictionary of only a few items.
#     Returns:
#         str: the table as a string.
#     """
#     keys, values = tuple(zip(*small_dict.items()))
#     table = tabulate(
#         [values],
#         headers=keys,
#         tablefmt="pipe",
#         floatfmt=".3f",
#         stralign="center",
#         numalign="center",
#     )
#     return table

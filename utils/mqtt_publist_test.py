import random
import time

from paho.mqtt import client as mqtt_client

broker = 'broker.emqx.io'
port = 1883
topic = "/python/mqtt"
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 1000)}'


def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


data = {
    "logId": "b17f24ff026d40949c85a24f4f375d42",
    "gatewayId": "1001",
    "timestamp": "1618992468",
    "type": 0,
    "version": "v1.0",
    "data": {
        "robotId": 1001,
        "action": 1,
        "direction": "left",
        "step": 1,
        "horizontal": 10,
        "vertical": 10,
        "focus": 10
    }
}

def deformation_pub(self, dfmt, image_url):
data = {
    "logId": "b17f24ff026d40949c85a24f4f375d42",
    "gatewayId": "1001",
    "timestamp": "1618992468",
    "type": 0,
    "version": "v1.0",
    "data": {
        "robotId": 1,
        "originalImage": originUrl,
        "deformationImage": image_url,
        "offset": str(dfmt),
        "position": "xxx",
        "reconTime": "2021-08-27 10:17:22",
        "pos1": "3.83815,0.692948,0.597678",
        "pos2": "3.85745,0.815381,0.586892",
        "baseLt": str(_analyse_image.min_x),
        "baseRb": str(_analyse_image.min_y),
        "targetLt": str(_analyse_image.max_x),
        "targetRb": str(_analyse_image.max_y),
        "baseMax": "4.9,2,1.463",
        "baseMin": "1.48,-1.978,-0.25",
        "targetMax": "4.9,2,1.463",
        "targetMin": "1.48,-1.978,-0.25"
    }
}
param = json.dumps(data)
result = self.pub_client.publish(self.deformation_topic, param)
if result[0] != 0:
    return False
return True
        
import json
param = json.dumps(data)


def publish(client):
    msg_count = 0
    while True:
        time.sleep(1)
        msg = f"messages: {msg_count}"
        result = client.publish(topic, param)
        # result: [0, 1]
        status = result[0]
        if status == 0:
            print(f"Send `{msg}` to topic `{topic}`")
        else:
            print(f"Failed to send message to topic {topic}")
        msg_count += 1


def run():
    client = connect_mqtt()
    client.loop_start()
    publish(client)


if __name__ == '__main__':
    run()

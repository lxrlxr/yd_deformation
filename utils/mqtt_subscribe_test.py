import random
import json
from paho.mqtt import client as mqtt_client
import threading
import time

broker = 'broker.emqx.io'
port = 1883
topic = "/python/mqtt"
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 100)}'
flag = False


def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client: mqtt_client):
    def on_subscribe(client, userdata, mid, granted_qos):
        print("On Subscribed: qos = %d" % granted_qos)

    def on_message(client, userdata, msg):
        print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
        global flag
        flag = True

    client.subscribe(topic)
    client.on_message = on_message
    client.on_subscribe = on_subscribe


def pub():
    global client_id
    client_id = f'python-mqtt-{random.randint(0, 100)}'
    client = connect_mqtt()
    client.loop_start()
    lock = threading.RLock()
    while True:
        global flag
        if not flag:
            time.sleep(0.1)
            continue
        with lock:
            flag = False
        data = {
            "logId": "b17f24ff026d40949c85a24f4f375d42",
            "gatewayId": "1001",
            "timestamp": "1618992468",
            "type": 0,
            "version": "v1.0",
            "data": "data"
        }
        param = json.dumps(data)
        result = client.publish("self.camera_control_topic", param)
        if result[0] != 0:
            return False


def sub():
    global client_id
    client_id = f'python-mqtt-{random.randint(100, 1000)}'
    client = connect_mqtt()
    subscribe(client)
    client.loop_forever()


def run():
    thread_hi = threading.Thread(target=pub)
    thread_hi.start()

    thread_sub = threading.Thread(target=sub)
    thread_sub.start()

    while True:
        time.sleep(1)
        a = 1


if __name__ == '__main__':
    run()

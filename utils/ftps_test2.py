# coding=utf-8
import os
import os.path as osp
import sys


import traceback

prefix = "/"

ftps_image_path = "/yd_deformation/database/4-7-14160000.jpg"
mm = osp.join("/home/lxr/workdir", ftps_image_path[1:])

def logint_ftp(host, username, pwd, port=21):
    implicit_flag = 1
    debugLevel = 0
    passive = 0
    protect = 1
    login_custom = 1

    if not implicit_flag:
        import ssl
        from ftplib import FTP_TLS

        # 创建一个 SSL 上下文对象，可以接受较小的密钥
        ssl_context = ssl.create_default_context()
        ssl_context.set_ciphers('DEFAULT@SECLEVEL=1')

        # 创建 FTP_TLS 对象，并使用上面创建的 SSL 上下文
        ftp = FTP_TLS(context=ssl_context)
    else:
        import utils.ftp as ftp
        import socket
        import ssl
        class FTP_TLS(ftp.FTP_TLS):
            def __init__(self, host='', user='', passwd='', acct='', keyfile=None, certfile=None, context=None,
                         timeout=180, source_address=None):
                if context is None:
                    context = ssl.create_default_context()
                    context.check_hostname = False  # 不检查主机名
                    context.verify_mode = ssl.CERT_NONE  # 不验证证书
                    context.set_ciphers('DEFAULT:@SECLEVEL=1')  # 降低SSL安全级别
                self.context = context  # 保存上下文到实例变量
                ftp.FTP_TLS.__init__(self, host, user, passwd, acct, keyfile, certfile, context, timeout)

            def connect(self, host='', port=0, timeout=-999):
                if host != '':
                    self.host = host
                if port > 0:
                    self.port = port
                if timeout != -999:
                    self.timeout = timeout

                try:
                    self.sock = socket.create_connection((self.host, self.port), self.timeout)
                    # 使用SSL上下文来包装socket
                    self.sock = self.context.wrap_socket(self.sock, server_hostname=self.host)
                    self.af = self.sock.family
                    self.file = self.sock.makefile('rb')
                    self.welcome = self.getresp()
                except Exception:
                    print("ftps连接失败", exc_info=True)
                return self.welcome


    ftps = FTP_TLS()
    ftps.set_debuglevel(debugLevel)  # 打开调试级别2，显示详细信息
    if passive:
        ftps.set_pasv(True)
    ftps.connect(host, port)
    if login_custom:
        ftps.login(username, pwd)
    else:
        ftps.sendcmd(f'USER {username}')
        ftps.sendcmd(f'PASS {pwd}')
    if protect:
        ftps.prot_p()
    return ftps


def get_image(image_save_path, ftps_image_path, host, username, pwd, port=21):
    try:
        ftps = logint_ftp(host, username, pwd, port)

        # os.chdir(localpath)  # 切换工作路径到下载目录
        ftps_image_path = osp.join(prefix, ftps_image_path[1:])

        ftps_dir = osp.dirname(osp.dirname(ftps_image_path)) + '/'
        ftps.cwd(ftps_dir)  # 要登录的ftp目录
        files = ftps.nlst()  # 获取目录下的文件
        files = filter(lambda x: len(x.split(".")) <= 1, files)
        # print(f'ftps_dir:{ftps_dir}')

        ftps_dir = osp.dirname(ftps_image_path) + '/'
        ftps.cwd(ftps_dir)  # 要登录的ftp目录
        files = ftps.nlst()  # 获取目录下的文件
        files = filter(lambda x: len(x.split(".")) <= 1, files)
        # print(f'ftps_dir:{ftps_dir}')

        file_handle = open(image_save_path, "wb").write  # 以写模式在本地打开文件
        file_name = osp.basename(ftps_image_path)
        print(f'file_name:{file_name}')
        ftps.retrbinary('RETR %s' % file_name, file_handle, blocksize=1024)  # 下载ftp文件
        # ftp.delete（filename）  # 删除ftp服务器上的文件
        # ftps.set_debuglevel(0)  # 关闭调试
        ftps.quit()
        return True
    except Exception:
        if osp.exists(image_save_path):
            os.remove(image_save_path)
        print(f"ftps地址：{ftps_image_path},image_save_path:{image_save_path},下载图片失败", exc_info=True)
        return False


def upload_image(filename, address, host, username, pwd, port=21):
    try:
        ftps = logint_ftp(host, username, pwd, port)

        root_path = prefix
        part_path = root_path
        ftps.cwd(part_path)
        for part in address.split('/')[:-1]:
            files = ftps.nlst()
            if part not in files:
                ftps.mkd(part)
            part_path = osp.join(part_path, part)
            ftps.cwd(part_path)

        myfile = open(filename, 'rb')
        ftps.storbinary('STOR ' + osp.join(root_path, address), open(filename, 'rb'))
        ftps.quit()
        myfile.close()
    except Exception:
        print("ftp image file upload error", exc_info=True)


if __name__ == '__main__':
    try:
        get_image(image_save_path='/home/yd/code/lixingren/yd_deformation/database/4-7-14160000.jpg',
                  ftps_image_path='/xingbian/1117.jpg', host="192.168.5.15", username='ydrobot',
                  pwd='123qweasd', port=21)
        pass
    except Exception:
        traceback.print_exc()

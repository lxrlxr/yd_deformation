camare_receive_msg = {
    "logId": "b17f24ff026d40949c85a24f4f375d42",
    "timestamp": "1618992468",
    "type": 0,
    "version": "v1.0",
    "data": {
        "taskCode": "xxxx",
        "taskName": "xxxx",
        "taskPatrolledId": "xxx",
        "dataCode": "xxx",
        "pointId": 11011,
        "stopPoint": 999,
        "robotId": 1,
        "cameraId": 2,
        "preset": "1",
        "tagId": "1111",
        "watchPointId": 1,
        "calibrationData": '''%YAML:1.0
---
CameraExtrinsicMat: !!opencv-matrix
   rows: 4
   cols: 4
   dt: d
   data: [ -3.0865440145134926e-02, -3.6863459446654484e-01,
       9.2906191715849229e-01, 2.0009778976440423e+01,
       -9.9930226802825950e-01, -8.1781601196896404e-03,
       -3.6443917661875713e-02, 3.1546998023986830e+00,
       2.1032504737377167e-02, -9.2953845009626368e-01,
       -3.6812493632672577e-01, -1.3271162414550781e+02, 0., 0., 0., 1. ]
CameraMat: !!opencv-matrix
   rows: 3
   cols: 3
   dt: d
   data: [ 1.9387885360893063e+03, 0., 9.1868334483484091e+02, 0.,
       1.9544454948604734e+03, 5.5780043682102598e+02, 0., 0., 1. ]
DistCoeff: !!opencv-matrix
   rows: 5
   cols: 1
   dt: d
   data: [ -7.4313995356602214e-02, -3.6926224428592497e-01,
       6.2147894754583245e-04, -4.1935346984603254e-04,
       3.3176656167883889e-01 ]
ImageSize: [ 1920, 1080 ]
ReprojectionError: 0.
DistModel: plumb_bob''',
        "originalImage": "/opt/srv/xxx2.jpg",
        "filePath": "/opt/srv/xxx.jpg",
        "detectionArea": "[[1375,758],[1558,758],[1558,933],[1375,933]]",
        "configParam": "{\"MODEL.DEVICE\":\"cpu\",\"MODEL.RESIZE_W\":\"0\",\"MODEL.RESIZE_H\":\"0\",\"MODEL.SCALE\":\"1\",\"MODEL.SUPERPOINT.NMS_RADIUS\":\"4\",\"MODEL.SUPERPOINT.KEYPOINT_THRESHOLD\":\"0.005\",\"MODEL.SUPERPOINT.MAX_KEYPOINTS\":\"-1\",\"MODEL.SUPERGLUE.WEIGHTS\":\"outdoor\",\"MODEL.SUPERGLUE.SINKHORN_ITERATIONS\":\"20\",\"MODEL.SUPERGLUE.MATCH_THRESHOLD\":\"0.2\",\"SEGMENTATION.EXTEND\":\"30\",\"CLUSTER.ERRORCOUNT\":\"2\",\"CLUSTER.ERRORTHRESH\":\"0.05\",\"THRESHOLD\":\"3\",\"VOXEL.EXTEND\":\"5\",\"VOXEL.THRESHOLD\":\"0.1\",\"VOXEL.ERODE_PIXELS\":\"6\"}",
        "updateTime": "1651111001541",
        "distance": "10"
    }
}

camare_receive_msg = {
    "logId": "b17f24ff026d40949c85a24f4f375d42",
    "timestamp": "1618992468",
    "type": 0,
    "version": "v1.0",
    "data": {
        "taskCode": "xxxx",
        "taskName": "xxxx",
        "taskPatrolledId": "xxx",
        "dataCode": "xxx",
        "pointId": 11011,
        "stopPoint": 999,
        "robotId": 1,
        "cameraId": 2,
        "preset": "1",
        "tagId": "1111",
        "watchPointId": 1,
        "calibrationData": '''%YAML:1.0
---
theta_0: 1.5828371047973633e-02
theta_1: 2.7703665182343684e-05
power: 2''',
        "originalImage": "/opt/srv/xxx2.jpg",
        "filePath": "/opt/srv/xxx.jpg",
        "detectionArea": "[[1375,758],[1558,758],[1558,933],[1375,933]]",
        "configParam": "{\"distance\": 10,\"MODEL.DEVICE\":\"cpu\",\"MODEL.RESIZE_W\":\"0\",\"MODEL.RESIZE_H\":\"0\",\"MODEL.SCALE\":\"1\",\"MODEL.SUPERPOINT.NMS_RADIUS\":\"4\",\"MODEL.SUPERPOINT.KEYPOINT_THRESHOLD\":\"0.005\",\"MODEL.SUPERPOINT.MAX_KEYPOINTS\":\"-1\",\"MODEL.SUPERGLUE.WEIGHTS\":\"outdoor\",\"MODEL.SUPERGLUE.SINKHORN_ITERATIONS\":\"20\",\"MODEL.SUPERGLUE.MATCH_THRESHOLD\":\"0.2\",\"SEGMENTATION.EXTEND\":\"30\",\"CLUSTER.ERRORCOUNT\":\"2\",\"CLUSTER.ERRORTHRESH\":\"0.05\",\"THRESHOLD\":\"3\",\"VOXEL.EXTEND\":\"5\",\"VOXEL.THRESHOLD\":\"0.1\",\"VOXEL.ERODE_PIXELS\":\"6\"}",
        "updateTime": "1651111001541"

    }
}

# # 2023-08-10 测试服务器速度慢的原因
# camare_receive_msg = {"logId": "4681dbe4e56d4134a89f3a572d508ea1", "timestamp": 1691640112808, "type": 0,
#                       "version": "v1.0", "data": {
#         "configParam": "{\"MODEL.DEVICE\":\"cpu\",\"MODEL.SUPERGLUE.WEIGHTS\":\"\",\"目标区域size\":\"\",\"MODEL.RESIZE_W\":\"\",\"MODEL.SUPERGLUE.SINKHORN_ITERATIONS\":\"\",\"MODEL.RESIZE_H\":\"\",\"MODEL.SUPERGLUE.MATCH_THRESHOLD\":\"\",\"MODEL.SUPERPOINT.NMS_RADIUS\":\"\",\"SEGMENTATION.EXTEND\":\"\",\"MODEL.SUPERPOINT.KEYPOINT_THRESHOLD\":\"\",\"CLUSTER.LAYER\":\"\",\"MODEL.SUPERPOINT.MAX_KEYPOINTS\":\"\",\"MODEL.SCALE\":\"\",\"THRESHOLD\":\"\",\"CLUSTER.ERRORCOUNT\":\"\",\"CLUSTER.ERRORTHRESH\":\"\",\"VOXEL.EXTEND\":\"\",\"VOXEL.THRESHOLD\":\"\",\"VOXEL.ERODE_PIXELS\":\"\",\"distance\":22.34851586951581}",
#         "tagId": "54", "filePath": "/2023/08/10/49314/Img/383.jpg", "updateTime": 1691482361797, "preset": 0,
#         "robotId": 7, "dataCode": "", "taskPatrolledId": "49314",
#         "calibrationData": "%YAML:1.0\n---\ntheta_0: 1.5828371047973633e-02\ntheta_1: 2.7018537366529927e-05\npower: 2\n\n\n",
#         "taskCode": "", "pointId": 331440040, "cameraId": 11,
#         "detectionArea": "[[701.2765957446808,533.1914893617021],[1143.8297872340424,533.1914893617021],[1143.8297872340424,682.9787234042553],[701.2765957446808,682.9787234042553]]",
#         "watchPointId": 383, "taskName": "", "stopPoint": 0,
#         "originalImage": "/2023/08/08/49262/ImgRef/1691458394000.jpg"}}
#
# camare_receive_msg = {"logId": "4681dbe4e56d4134a89f3a572d508ea1", "timestamp": 1691640112808, "type": 0,
#                       "version": "v1.0", "data": {
#         "configParam": "{\"MODEL.DEVICE\":\"cpu\",\"MODEL.SUPERGLUE.WEIGHTS\":\"\",\"目标区域size\":\"\",\"MODEL.RESIZE_W\":\"\",\"MODEL.SUPERGLUE.SINKHORN_ITERATIONS\":\"\",\"MODEL.RESIZE_H\":\"\",\"MODEL.SUPERGLUE.MATCH_THRESHOLD\":\"\",\"MODEL.SUPERPOINT.NMS_RADIUS\":\"\",\"SEGMENTATION.EXTEND\":\"\",\"MODEL.SUPERPOINT.KEYPOINT_THRESHOLD\":\"\",\"CLUSTER.LAYER\":\"\",\"MODEL.SUPERPOINT.MAX_KEYPOINTS\":\"\",\"MODEL.SCALE\":\"\",\"THRESHOLD\":\"\",\"CLUSTER.ERRORCOUNT\":\"\",\"CLUSTER.ERRORTHRESH\":\"\",\"VOXEL.EXTEND\":\"\",\"VOXEL.THRESHOLD\":\"\",\"VOXEL.ERODE_PIXELS\":\"\",\"distance\":22.34851586951581}",
#         "tagId": "54", "filePath": "/2023/08/10/49314/Img/383.jpg", "updateTime": 1691482361797, "preset": 0,
#         "robotId": 7, "dataCode": "", "taskPatrolledId": "49314",
#         "calibrationData": "%YAML:1.0\n---\ntheta_0: 1.5828371047973633e-02\ntheta_1: 2.7018537366529927e-05\npower: 2\n\n\n",
#         "taskCode": "", "pointId": 331440040, "cameraId": 11,
#         "detectionArea": "[[1375,758],[1558,758],[1558,933],[1375,933]]",
#         "watchPointId": 383, "taskName": "", "stopPoint": 0,
#         "originalImage": "/2023/08/08/49262/ImgRef/1691458394000.jpg"}}
#
#
# camare_receive_msg = {
#     "logId":"2ca0210e02d34abc8c517769a1c6922c",
#     "timestamp":1694507929641,
#     "type":0,
#     "version":"v1.0",
#     "data":{
#         "configParam":"{\"distance\":20504.663655818687}",
#         "tagId":"114",
#         "filePath":"/home/lxr/workdir/yd_deformation/error_data/image_9702_9702/446-.jpg",
#         "updateTime":1694506963929,
#         "preset":0,
#         "robotId":13,
#         "dataCode":"",
#         "taskPatrolledId":"51238",
#         "calibrationData":"%YAML:1.0\n---\ntheta_0: 1.5828371047973633e-02\ntheta_1: 2.7747864805860445e-05\npower: 2\n",
#         "taskCode":"",
#         "pointId":331440100,
#         "cameraId":22,
#         "detectionArea":"[[820.4255319148937,448.0851063829788],[987.2340425531919,448.0851063829788],[987.2340425531919,621.7021276595744],[820.4255319148937,621.7021276595744]]",
#         "watchPointId":446,
#         "taskName":"",
#         "stopPoint":0,
#         "originalImage":"/home/lxr/workdir/yd_deformation/error_data/image_9702_9702/446.jpg"
#     }
# }

# 测试绍兴站数据
camare_receive_msg = {
    "logId": "b17f24ff026d40949c85a24f4f375d42",
    "timestamp": "1618992468",
    "type": 0,
    "version": "v1.0",
    "data": {
        "taskCode": "xxxx",
        "taskName": "xxxx",
        "taskPatrolledId": "xxx",
        "dataCode": "xxx",
        "pointId": 11011,
        "stopPoint": 999,
        "robotId": 1,
        "cameraId": 2,
        "preset": "1",
        "tagId": "1111",
        "watchPointId": 1,
        "calibrationData": '''%YAML:1.0
---
theta_0: 1.5828371047973633e-02
theta_1: 2.7703665182343684e-05
power: 2''',
        "originalImage": "/opt/srv/xxx2.jpg",
        "filePath": "/opt/srv/xxx.jpg",
        "detectionArea": "[[553,202],[1454,202],[1454,781],[553,781]]",
        "configParam": "{\"distance\": 10,\"MODEL.DEVICE\":\"cpu\",\"MODEL.RESIZE_W\":\"0\",\"MODEL.RESIZE_H\":\"0\",\"MODEL.SCALE\":\"1\",\"MODEL.SUPERPOINT.NMS_RADIUS\":\"4\",\"MODEL.SUPERPOINT.KEYPOINT_THRESHOLD\":\"0.005\",\"MODEL.SUPERPOINT.MAX_KEYPOINTS\":\"-1\",\"MODEL.SUPERGLUE.WEIGHTS\":\"outdoor\",\"MODEL.SUPERGLUE.SINKHORN_ITERATIONS\":\"20\",\"MODEL.SUPERGLUE.MATCH_THRESHOLD\":\"0.2\",\"SEGMENTATION.EXTEND\":\"30\",\"CLUSTER.ERRORCOUNT\":\"2\",\"CLUSTER.ERRORTHRESH\":\"0.05\",\"THRESHOLD\":\"3\",\"VOXEL.EXTEND\":\"5\",\"VOXEL.THRESHOLD\":\"0.1\",\"VOXEL.ERODE_PIXELS\":\"6\"}",
        "updateTime": "1651111001541"

    }
}
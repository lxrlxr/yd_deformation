#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json
import os
import os.path as osp
import queue
import threading
import time

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

from tqdm import tqdm

from paho.mqtt import client as mqtt_client
from utils.config import GetConfig
from utils.logger import setup_logger, clear_data

dirPath = osp.dirname(osp.abspath(__file__))
logger = setup_logger(project_name='testMqtt')


class TestMqtt:
    def __init__(self):
        '''
        初始化
        '''
        pass

    def init(self):
        try:
            self.cfg = GetConfig(osp.join(dirPath, 'config', 'config.json'))
            if not self.cfg.TESTMODE:
                self.mqtt_client = self.connect_mqtt()
                if not self.mqtt_client:
                    return False
                self.mqtt_client.loop_start()
            self.lock = threading.RLock()
            self.queue = queue.Queue()

            return True
        except Exception:
            logger.error("init error", exc_info=True)
            return False

    def connect_mqtt(self) -> mqtt_client:
        '''
        Returns:
            mqtt的client或错误False
        连接mqtt
        '''

        def on_connect(client, userdata, flags, rc):
            if rc != 0:
                logger.info("Failed to connect, return code %d\n", rc)

        def on_disconnect(client, userdata, rc):
            logger.info(f"MQTT disconnect, reconnect")
            client.reconnect()

        try:
            client = mqtt_client.Client("test_pub")
            client.username_pw_set(self.cfg.MQTT.USERNAME, self.cfg.MQTT.PASSWORD)
            client.on_connect = on_connect
            client.on_disconnect = on_disconnect
            client.connect(self.cfg.MQTT.HOST, self.cfg.MQTT.PORT, 60)
            logger.info(f"Connected to MQTT Broker!client_id:{self.cfg.MQTT.NODENAME}")
            return client
        except Exception:
            logger.error("mqtt client init and connect failed", exc_info=True)
            return False

    def run(self):
        for i in tqdm(range(0, 10)):
            try:
                global logger
                logger = clear_data(logger)

                time.sleep(1)

                data = {
                    "logId": "f65101b7f4f94180929190d641099179",
                    "timestamp": 1696836592852,
                    "type": 0,
                    "version": "v1.0",
                    "data": {
                        "configParam": "{\"distance\":14089.950302323108}",
                        "tagId": "141",
                        "filePath": "/home/lxr/workdir/yd_deformation/error_data/2023-10-09/478-2023-10-09 15_29_53.jpg",
                        "updateTime": 1696835105495,
                        "preset": 0,
                        "robotId": 13,
                        "dataCode": "",
                        "taskPatrolledId": "52861",
                        "calibrationData": "%YAML:1.0\n---\ntheta_0: 1.5828371047973633e-02\ntheta_1: 3.4300104744033888e-05\npower: 2\n",
                        "taskCode": "",
                        "pointId": 331440127,
                        "cameraId": 22,
                        "detectionArea": "[[738.4,499.5],[1014.4,399.3],[1054.2,706.3],[708.2,706.1]]",
                        "watchPointId": 478,
                        "taskName": "",
                        "stopPoint": 0,
                        "originalImage": "/home/lxr/workdir/yd_deformation/error_data/2023-10-09/478.jpg"
                    }
                }
                param = json.dumps(data)
                result = self.mqtt_client.publish(self.cfg.TOPIC.TASK, param)
                logger.info(f"pub result:{result}")
                input()
            except Exception:
                logger.error("error", exc_info=True)

    def run_test(self):
        for i in tqdm(range(0, 10)):
            try:
                global logger
                logger = clear_data(logger)

                time.sleep(1)

                data = {
                    "logId": "f65101b7f4f94180929190d641099179",
                    "timestamp": 1696836592852,
                    "type": 0,
                    "version": "v1.0",
                    "data": {
                        "configParam": "{\"distance\":14089.950302323108}",
                        "tagId": "141",
                        "filePath": "/home/peter/deformation/database/478-2023-10-09_15_29_53.jpg",
                        "updateTime": 1696835105495,
                        "preset": 0,
                        "robotId": 13,
                        "dataCode": "",
                        "taskPatrolledId": "52861",
                        "calibrationData": "%YAML:1.0\n---\ntheta_0: 1.5828371047973633e-02\ntheta_1: 3.4300104744033888e-05\npower: 2\n",
                        "taskCode": "",
                        "pointId": 331440127,
                        "cameraId": 22,
                        "detectionArea": "[[738.4,499.5],[1014.4,399.3],[1054.2,706.3],[708.2,706.1]]",
                        "watchPointId": 478,
                        "taskName": "",
                        "stopPoint": 0,
                        "originalImage": "/home/peter/deformation/database/478.jpg"
                    }
                }
                param = json.dumps(data)
                result = self.mqtt_client.publish(self.cfg.TOPIC.TASK, param)
                logger.info(f"pub result:{result}")
                input()
            except Exception:
                logger.error("error", exc_info=True)


def main():
    try:
        testMqtt = TestMqtt()
        if not testMqtt.init():
            return
        testMqtt.run()
    except Exception:
        logger.error("error", exc_info=True)


if __name__ == '__main__':
    main()

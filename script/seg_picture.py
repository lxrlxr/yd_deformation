import cv2
import traceback
import tqdm
import os
from PIL import Image

# 防止字符串乱码
os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'

picture_dir = '/home/lxr/database/pre-print'
out_picture_dir = '/home/lxr/database/out-print'
out_pdf_dir = '/home/lxr/database/out-pdf'

# if not os.path.exists(out_picture_dir):
#     os.mkdir(out_picture_dir)
# else:
#     for file in os.listdir(out_picture_dir):
#         os.remove(os.path.join(out_picture_dir, file))

if not os.path.exists(out_pdf_dir):
    os.mkdir(out_pdf_dir)
else:
    for file in os.listdir(out_pdf_dir):
        os.remove(os.path.join(out_pdf_dir, file))

# height_cell = 1600
# try:
#     files = os.listdir(picture_dir)
#     files = filter(lambda x: x.endswith('.jpg'), files)
#     files = list(files)
#     files = sorted(files)
#     for file in tqdm.tqdm(files):
#         pic_path = os.path.join(picture_dir, file)
#         image = cv2.imread(pic_path)
#         height_index = 0
#         sub_image_index = 0
#         while height_index < image.shape[0]:
#             out_height = min(height_index + height_cell, image.shape[0])
#             if image.shape[0] - out_height < 300:
#                 out_height = image.shape[0]
#             out_image = image[height_index: out_height, :, :]
#             sub_image_index += 1
#             index = str(sub_image_index)
#             while len(index) < 3:
#                 index = '0' + index
#             out_path = os.path.join(out_picture_dir, file.replace('.jpg', f'_{index}.jpg'))
#             cv2.imwrite(out_path, out_image)
#             height_index = out_height
#             if out_height < image.shape[0]:
#                 height_index -= 50
# except:
#     traceback.print_exc()

try:
    file_list = os.listdir(out_picture_dir)
    for x in file_list:
        if "jpg" in x or 'png' in x or 'jpeg' in x:
            pdf_name = x.split('.')[0]
            im1 = Image.open(os.path.join(out_picture_dir, x))
            im1.save(out_pdf_dir +'/'+ pdf_name + '.pdf', "PDF", resolution=100.0)
except:
    traceback.print_exc()

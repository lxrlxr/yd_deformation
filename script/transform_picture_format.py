import cv2
import traceback
import tqdm
import os
import os.path as osp

try:
    dirPath = osp.join(osp.dirname(osp.dirname(osp.abspath(__file__))), 'database', 'image')
    print(f'dirPath:{dirPath}')
    for file in tqdm.tqdm(os.listdir(dirPath)):
        if file.endswith('.png'):
            image = cv2.imread(osp.join(dirPath,file))
            cv2.imwrite(osp.join(dirPath, file[:-4] + '.jpg'), image)
except:
    traceback.print_exc()

#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <iostream>

using namespace cv;
using namespace cv::xfeatures2d;
using namespace std;

void detectAndMatchFeatures(const Mat& img1, const Mat& img2, const string& method) {
    Ptr<Feature2D> detector;

    if (method == "SIFT") {
        detector = SIFT::create();
    } else if (method == "SURF") {
        detector = SURF::create();
    } else if (method == "ORB") {
        detector = ORB::create();
    } else {
        cerr << "Unknown method: " << method << endl;
        return;
    }

    vector<KeyPoint> keypoints1, keypoints2;
    Mat descriptors1, descriptors2;

    detector->detectAndCompute(img1, noArray(), keypoints1, descriptors1);
    detector->detectAndCompute(img2, noArray(), keypoints2, descriptors2);

    BFMatcher matcher(method == "ORB" ? NORM_HAMMING : NORM_L2, true);
    vector<DMatch> matches;
    matcher.match(descriptors1, descriptors2, matches);

   // Sort matches by score
    sort(matches.begin(), matches.end());

    // Keep only the top 500 matches
    if (matches.size() > 500) {
        matches.erase(matches.begin() + 500, matches.end());
    }

    Mat img_matches;
    drawMatches(img1, keypoints1, img2, keypoints2, matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

    imshow(method + " Feature Matching", img_matches);
    imwrite(method + " Feature Matching.jpg", img_matches);
    waitKey(0);
}

int main(int argc, char** argv) {
    if (argc != 3) {
        cout << "Usage: ./feature_matching <img1> <img2>" << endl;
        return -1;
    }

    Mat img1 = imread(argv[1], IMREAD_GRAYSCALE);
    Mat img2 = imread(argv[2], IMREAD_GRAYSCALE);

    if (img1.empty() || img2.empty()) {
        cerr << "Cannot read images!" << endl;
        return -1;
    }

    detectAndMatchFeatures(img1, img2, "SIFT");
    detectAndMatchFeatures(img1, img2, "SURF");
    detectAndMatchFeatures(img1, img2, "ORB");

    return 0;
}


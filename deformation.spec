# -*- mode: python ; coding: utf-8 -*-


block_cipher = pyi_crypto.PyiBlockCipher(key='123123')


a = Analysis(
    ['deformation.py'],
    pathex=[],
    binaries=[],
    datas=[('./log/*', './log'), ('./config/*', './config'), ('./database/image/*', './database/image'), ('./database/pointcloud/*', './database/pointcloud'), ('./weights/*', './weights'), ('./pixeltovoxel/lib/*', './pixeltovoxel/lib')],
    hiddenimports=['sklearn.utils._typedefs', 'sklearn.neighbors._partition_nodes'],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='deformation',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='deformation',
)

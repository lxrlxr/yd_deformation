# export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libffi.so.7
import os
import os.path as osp
import sys

import numpy as np


def main():
    try:
        dic_txt = "/home/yd/文档/123/deformation/output.txt"
        with open(dic_txt, 'r') as file:
            lines = file.readlines()
        dfs = [x.replace("\n", "").split(" ")[2:] for x in lines]
        dfs = np.array(dfs).astype(float)

        x_min, x_max, y_min, y_max = 510, 1085, 335, 669
        distance = 8.172841359536141
        theta_0 = 1.5828371047973633e-02
        theta_1 = 3.2830816053319722e-05
        alpha = distance * theta_1 + theta_0
        print(f"alpha:{alpha} distance:{distance} theta_0:{theta_0} theta_1:{theta_1}")

        ############################################   由于部分情况下，dic的输出疑似超过框图界限，新增功能   #############################
        # 找到绝对值大于m的x元素，并将这些元素赋值为0
        dfs[np.abs(dfs[:, 0]) > (x_max - x_min), :] = 0

        # 找到绝对值大于n的y元素，并将这些元素赋值为0
        dfs[np.abs(dfs[:, 1]) > (y_max - y_min), :] = 0
        ############################################   END 由于部分情况下，dic的输出疑似超过框图界限，新增功能   #############################

        dfs[:, :] = dfs[:, :] * (distance * theta_1 + theta_0)
        dic_world_txt = osp.join("/home/yd/文档/123/output_world.txt")
        with open(dic_world_txt, 'w') as file:
            for i in range(len(dfs)):
                params = lines[i].replace("\n", "").split(" ")
                file.write(f"{params[0]} {params[1]} {dfs[i][0]} {dfs[i][1]}\n")

        print(f"dfs[4]:{dfs[4]}")
        a = 1
    except Exception:
        ...


if __name__ == '__main__':
    main()

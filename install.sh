#!/bin/bash

# 检查是否传入了参数
if [ $# -eq 0 ]; then
    echo "No arguments supplied. Usage: $0 [superglue|ncorr|dic]"
    exit 1
fi

# 获取传入的参数
option="$1"

# 根据参数值执行不同的命令
if [ "$option" == "superglue" ]; then
    cd pixeltovoxel/lib
    rm -r *
    cd ../build
    rm -r *
    cmake ..
    make -j16
    cd ../lib

    exe="py3d.cpython-39-x86_64-linux-gnu.so"
    des="./"
    deplist=$(ldd $exe | awk  '{if (match($3,"/")){ printf("%s "),$3 } }')
    cp $deplist $des

    cd ../..

    pyinstaller deformation.py --key 123123 \
        --hidden-import sklearn.utils._typedefs \
        --hidden-import sklearn.neighbors._partition_nodes \
        --add-data "./log/*:./log" \
        --add-data "./config/*:./config" \
        --add-data "./database/image/*:./database/image" \
        --add-data "./database/pointcloud/*:./database/pointcloud" \
        --add-data "./weights/*:./weights" \
        --add-data "./pixeltovoxel/lib/*:./pixeltovoxel/lib"

    cd dist/deformation
    rm libcuda.so.1
    cd ../..
    
    cp yd_deformation_start.sh dist/deformation/

elif [ "$option" == "ncorr" ]; then
    pyinstaller deformation.py --key 123123 \
        --hidden-import sklearn.utils._typedefs \
        --hidden-import sklearn.neighbors._partition_nodes \
        --add-data "./log/*:./log" \
        --add-data "./config/*:./config" \
        --add-data "./database/image/*:./database/image" \
        --add-data "./database/pointcloud/*:./database/pointcloud" \
        --add-data "./weights/*:./weights" \
        --add-data "./pixeltovoxel/lib/*:./pixeltovoxel/lib" \
        --add-data "./ncorr/lib/*.so:./" \
        --add-data "./Hmatrix_temp/:./Hmatrix_temp" \
        --add-data "./ncorr/lib/package/:./ncorr/package"

elif [ "$option" == "dic" ]; then
    pyinstaller deformation.py --key 123123 \
        --hidden-import sklearn.utils._typedefs \
        --hidden-import sklearn.neighbors._partition_nodes \
        --add-data "./log/*:./log" \
        --add-data "./config/*:./config" \
        --add-data "./database/image/*:./database/image" \
        --add-data "./database/pointcloud/*:./database/pointcloud" \
        --add-data "./weights/*:./weights" \
        --add-data "./pixeltovoxel/lib/*:./pixeltovoxel/lib" \
        --add-data "./ncorr/lib/*.so:./" \
        --add-data "./Hmatrix_temp/:./Hmatrix_temp" \
        --add-data "./ncorr/lib/package/:./ncorr/package"

else
    echo "Invalid option. Please select 'superglue', 'ncorr', or 'dic'."
    exit 2
fi


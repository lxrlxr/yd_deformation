#!/bin/bash

# 获取脚本所在的目录
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# 获得 lib 目录
LID_DIR=${SCRIPT_DIR}/pixeltovoxel/lib
echo "LID_DIR: " $LID_DIR

# 将 lib 目录加入到 LD_LIBRARY_PATH 环境变量
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${SCRIPT_DIR}:${LID_DIR}"

# 执行当前目录下的一个可执行文件，这里假设可执行文件名为 yd_ai_node
./deformation

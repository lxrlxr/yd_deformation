#/bin/bash -e
# Copy the deps of all elf files in `ELFDIR` to `LIBDIR`.

PROGNAME=$(basename $0)
if [[ $# != 2 ]]; then
    echo "Usage: $PROGNAME ELFDIR LIBDIR" >&2
    exit 1
fi

ELFDIR=$1
LIBDIR=$2

sopaths=()

for elf in `find $ELFDIR -type f -exec file {} + | grep ELF | cut -d: -f1`; do 
    echo "Analysing $elf"
    ldd $elf
    for sopath in `ldd $elf | grep -E '.+.so.* => /' | awk '{print $3}'`; do 
        sopaths+=($sopath)
        echo "sopath:" $sopath
    done
done

echo "step 1"
echo $sopaths

sopaths=(`for i in ${sopaths[*]}; do echo $i; done | sort -u`) 

echo "step 2"

for sopath in ${sopaths[*]}; do
    echo "Processing $sopath" 
    soname=`basename $sopath`
    if [[ (! -e $LIBDIR/$soname) ]]; then
        if [[ `which dpkg 2> /dev/null` ]]; then
            owninfo=`dpkg -S $sopath 2> /dev/null ||:`
        else
            owninfo=`rpm -qf $sopath 2> /dev/null ||:`
        fi
        if [[ ! $owninfo =~ ^glibc|^libc6 ]]; then 
            cp -v $sopath $LIBDIR
        fi
    fi
done

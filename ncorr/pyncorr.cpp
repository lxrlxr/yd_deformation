#include "ncorr.h"
#include "time.h"
#include <chrono>
#include <fstream>
#include <iostream>
#include <cmath> // for std::abs
#include <opencv2/opencv.hpp>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <string>
#include <sys/time.h>
#include <vector>

using namespace std;

namespace py = pybind11;

#include "ncorr.h"
#include <chrono>
#include <fstream>
#include <iostream>

using namespace ncorr;

int print_cost_time(std::chrono::_V2::system_clock::time_point start_time, std::string strPrintContext)
{
    // 获取结束时间点
    auto end_time = std::chrono::high_resolution_clock::now();

    // 计算耗时
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();

    // 输出耗时
    std::cout << strPrintContext << "代码运行耗时: " << duration << " 微秒" << std::endl;

    return 0;
}

int load_local_image(std::string strImagePath, std::vector<Image2D> &imgs)
{
    std::ostringstream ostr;
    ostr << strImagePath;
    imgs.push_back(ostr.str());

    return 0;
}

int ncorr_surface_real(std::string strRefImage, std::string strCurImage, int x_min, int x_max, int y_min, int y_max)
{
    try
    {
        std::cout << "x_min:" << x_min << std::endl;
        std::cout << "x_max:" << x_max << std::endl;
        std::cout << "y_min:" << y_min << std::endl;
        std::cout << "y_max:" << y_max << std::endl;

        // 定义矩形框的位置和大小
        // int x_min = 1375; // 矩形框的左上角 x 坐标
        // int y_min = 758;  // 矩形框的左上角 y 坐标
        // int x_max = 1558; // 矩形框的右下角 x 坐标
        // int y_max = 933;  // 矩形框的右下角 y 坐标

        cv::Mat cv_img = cv::imread((strRefImage), cv::IMREAD_GRAYSCALE);
        Array2D<bool> roi(cv_img.rows, cv_img.cols);

        for (Image2D::difference_type p1 = x_min; p1 <= x_max; ++p1)
        {
            for (Image2D::difference_type p2 = y_min; p2 <= y_max; ++p2)
            {
                roi(p2, p1) = true;
            }
        }

        DIC_analysis_input DIC_input;
        DIC_analysis_output DIC_output;
        strain_analysis_input strain_input;
        strain_analysis_output strain_output;

        // Set images
        std::vector<Image2D> imgs;

        load_local_image(strRefImage, imgs);
        load_local_image(strCurImage, imgs);

        // 获取开始时间点
        auto start_time = std::chrono::high_resolution_clock::now();

        // Set DIC_input
        DIC_input = DIC_analysis_input(imgs,                               // Images
                                       ROI2D(roi),                         // ROI
                                       1,                                  // scalefactor
                                       INTERP::QUINTIC_BSPLINE_PRECOMPUTE, // Interpolation
                                       SUBREGION::CIRCLE,                  // Subregion shape
                                       43,                                 // Subregion radius
                                       16,                                 // # of threads
                                       DIC_analysis_config::NO_UPDATE,     // DIC configuration for reference image updates
                                       false);                             // Debugging enabled/disabled

        print_cost_time(start_time, "step 1");

        // Perform DIC_analysis
        DIC_output = DIC_analysis(DIC_input);

        print_cost_time(start_time, "step 2");

        // Convert DIC_output to Eulerian perspective
        // DIC_output = change_perspective(DIC_output, INTERP::QUINTIC_BSPLINE_PRECOMPUTE);

        print_cost_time(start_time, "step 3");

        auto u = DIC_output.disps[0].get_u().get_array();
        auto v = DIC_output.disps[0].get_v().get_array();

        std::ofstream file("output.txt"); // 打开输出文件

        // 定义矩形框的位置和大小
        // int x_min = 1375; // 矩形框的左上角 x 坐标
        // int y_min = 758;  // 矩形框的左上角 y 坐标
        // int x_max = 1558; // 矩形框的右下角 x 坐标
        // int y_max = 933;  // 矩形框的右下角 y 坐标

        if (file.is_open())
        {
            for (Image2D::difference_type p1 = x_min; p1 <= x_max; ++p1)
            {
                for (Image2D::difference_type p2 = y_min; p2 <= y_max; ++p2)
                {
                    auto p = u(p2, p1);
                    auto q = v(p2, p1);

                    if (std::isnan(p) || std::abs(p) <= 1e-3)
                    {
                        p = 0;
                    }
                    if (std::isnan(q) || std::abs(q) <= 1e-3)
                    {
                        q = 0;
                    }

                    if (std::abs(p) >= 20)
                    {
                        p=0;
                    }
                    if(std::abs(q) >= 20)
                    {
                        q=0;
                    }

                    // std::cout << p2 << " " << p1 << " " << p << " " << q << std::endl;

//                    file << p2 << " " << p1 << " " << p << " " << q << std::endl;
                    file << p1 << " " << p2 << " " << p << " " << q << std::endl;
                }
            }
            file.close(); // 关闭文件
            std::cout << "写入完成" << std::endl;
        }
        else
        {
            std::cout << "无法打开文件" << std::endl;
            return -1;
        }

        print_cost_time(start_time, "step 4");
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
        return -1;
    }

    return 0;
}

int test()
{
    std::string strRefImage = "/home/lxr/workdir/others/ncorr_2D_matlab/1mm/1.jpg";
    std::string strCurImage = "/home/lxr/workdir/others/ncorr_2D_matlab/1mm/13.jpg";

    // 定义矩形框的位置和大小
    int x_min = 1375; // 矩形框的左上角 x 坐标
    int y_min = 758;  // 矩形框的左上角 y 坐标
    int x_max = 1558; // 矩形框的右下角 x 坐标
    int y_max = 933;  // 矩形框的右下角 y 坐标

    cv::Mat cv_img = cv::imread((strRefImage), cv::IMREAD_GRAYSCALE);
    Array2D<bool> A(cv_img.rows, cv_img.cols);

    for (Image2D::difference_type p1 = x_min; p1 <= x_max; ++p1)
    {
        for (Image2D::difference_type p2 = y_min; p2 <= y_max; ++p2)
        {
            A(p2, p1) = true;
        }
    }
    ncorr_surface_real(strRefImage, strCurImage, x_min, y_min, x_max, y_max);

    return 0;
}

PYBIND11_MODULE(pyncorr, m)
{
    m.doc() = "pyncorr";
    // pybind11::class_<Adjust>(m, "Adjust").def(pybind11::init(), py::return_value_policy::reference);
    m.def("ncorr_surface_real", &ncorr_surface_real, py::return_value_policy::reference);
    m.def("test", &test, py::return_value_policy::reference);
}

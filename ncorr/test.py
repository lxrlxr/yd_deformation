import sys
import math
import traceback

sys.path.append("/home/lxr/workdir/yd_deformation/ncorr/lib")


try:
    import pyncorr

    strRefImage = "/home/lxr/workdir/others/ncorr_2D_matlab/1mm/1.jpg"
    strCurImage = "/home/lxr/workdir/others/ncorr_2D_matlab/1mm/13.jpg"
    x_min = 1375
    y_min = 758
    x_max = 1558
    y_max = 933

    # pyncorr.test()
    out = pyncorr.ncorr_surface_real(strRefImage, strCurImage, x_min, x_max,y_min, y_max)
    a = 21
except:
    traceback.print_exc()

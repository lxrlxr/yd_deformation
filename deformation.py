# export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libffi.so.7
import os
import os.path as osp
import sys
import argparse
import logging
import time

from src.deformation import Deformation
from utils.logger import setup_logger

logger = setup_logger(project_name='deformation_main')

def load_configuration():
    """
    解析命令行参数，获取配置文件路径
    """
    parser = argparse.ArgumentParser(description="变形算法运行器")
    parser.add_argument('--config', type=str, required=False, default='config.json', help='配置文件路径')
    args = parser.parse_args()
    return args.config


def initialize_deformation(config_path):
    """
    初始化Deformation对象
    参数:
        config_path (str): 配置文件路径
    返回:
        deformation (Deformation): 初始化的Deformation对象
    """
    deformation = Deformation(config_path)
    logger.info(f"使用配置文件初始化Deformation: {config_path}")
    return deformation


def execute_algorithm(deformation):
    """
    执行指定的算法
    参数:
        deformation (Deformation): Deformation对象
    """
    try:
        algorithm = deformation.cfg.algorithm.lower()
        algorithm_func_name = f'run_{algorithm}'

        if hasattr(deformation, algorithm_func_name):
            algorithm_func = getattr(deformation, algorithm_func_name)
            logger.info(f"运行{algorithm.upper()}算法")
            start_time = time.time()
            algorithm_func()
            end_time = time.time()
            logger.info(f"{algorithm.upper()}算法在{end_time - start_time:.2f}秒内完成")
        else:
            logger.error(f"未知算法: {algorithm}")
            raise ValueError(f"不支持的算法: {algorithm}")

    except KeyError as e:
        logger.error(f"配置错误: 缺少键 {e}", exc_info=True)
        raise
    except ValueError as e:
        logger.error(f"值错误: {e}", exc_info=True)
        raise
    except Exception as e:
        logger.error("发生意外错误", exc_info=True)
        raise


def main():
    """
    主函数，执行程序入口
    """
    try:
        config_path = load_configuration()
        deformation = initialize_deformation(config_path)
        execute_algorithm(deformation)
    except Exception as e:
        logger.error("主执行过程中发生致命错误", exc_info=True)
        sys.exit(1)


if __name__ == '__main__':
    main()


#ifndef ADJUST_H
#define ADJUST_H
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <string>
#include <Eigen/Core>
#include "eigen3/Eigen/Dense"
#include <opencv2/core.hpp>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/transforms.h>
using namespace cv;
using namespace std;
using namespace pcl;

class Adjust
{
public:
    bool Init(std::string _mapfilepath);
    void calibrate(std::vector<cv::Point2i> roi_lt_rb_corner,
                   std::vector<std::vector<float>> &outpoints, int erode_pixels);
    void locate_camera(std::string _yamlfilepath);
    std::vector<float> get_distortion_point(float x, float y);

private:
    std::string color_lidar_yamlfilepath;
    cv::Mat colorprojection_matrix, colordistortion_matrix;
    pcl::PointCloud<pcl::PointXYZRGB> pc_in_cam;
    cv::Mat color_lidar_exRT;
    cv::Size colorimagesize;
    pcl::PointCloud<pcl::PointXYZRGB> pc;

private:
    void projectpointcloud(
        pcl::PointCloud<pcl::PointXYZRGB> point_cloud,
        cv::Mat projection_matrix,
        cv::Rect frame,
        std::vector<std::vector<float>> &outpoints, cv::Point2i left_top_p, int erode_pixels);
    void UpdateXYZ(pcl::PointCloud<pcl::PointXYZRGB> &in_pc, pcl::PointCloud<pcl::PointXYZRGB> &out_pc,
                   cv::Mat in_color_lidar_exRT);
};

#endif
#ifndef TOOLS_H
#define TOOLS_H
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <dirent.h>
#include <Eigen/Core>
#include "eigen3/Eigen/Dense"
#include <opencv2/core.hpp>
#include "opencv2/core/eigen.hpp"
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/passthrough.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/transforms.h>
//2D投影
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/project_inliers.h>
using namespace cv;
using namespace std;
using namespace pcl;
void ReadCalibrationFile(std::string _choose_file,
                         cv::Mat &out_RT, cv::Mat &out_intrinsic, cv::Mat &out_dist_coeff, cv::Size &imagesize)
{
    //std::cout << "_choose_file: " << _choose_file << std::endl;
    cv::FileStorage fs(_choose_file, cv::FileStorage::READ);
    if (!fs.isOpened())
    {
        std::cout << "Cannot open file calibration file" << _choose_file << std::endl;
    }
    else
    {
        fs["CameraMat"] >> out_intrinsic;
        fs["DistCoeff"] >> out_dist_coeff;
        fs["CameraExtrinsicMat"] >> out_RT;
        fs["ImageSize"] >> imagesize;
    }
}
void SaveCalibrationFile(std::string angle,cv::Mat in_extrinsic, cv::Mat in_intrinsic, cv::Mat in_dist_coeff, cv::Size in_size, float error)
{
    std::string path_filename_str;
    path_filename_str = "./adjust"+std::string(angle)+".yaml";

    cv::FileStorage fs(path_filename_str.c_str(), cv::FileStorage::WRITE);
    if (!fs.isOpened())
    {
        fprintf(stderr, "%s : cannot open file\n", path_filename_str.c_str());
        exit(EXIT_FAILURE);
    }

    fs << "CameraExtrinsicMat" << in_extrinsic;
    fs << "CameraMat" << in_intrinsic;
    fs << "DistCoeff" << in_dist_coeff;
    fs << "ImageSize" << in_size;
    fs << "ReprojectionError" << error;
    fs << "DistModel"
       << "plumb_bob";
    std::cout<<"wroten yaml file in: "<<path_filename_str<<std::endl;
}
void SplitString(const std::string &s, std::vector<std::string> &v, const std::string &c)
{
    std::string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;
    while (std::string::npos != pos2)
    {
        v.push_back(s.substr(pos1, pos2 - pos1));

        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }
    if (pos1 != s.length())
        v.push_back(s.substr(pos1));
};
void readFileList(std::string path, std::vector<std::string> &files, std::string ext)
{
    struct dirent *ptr;
    DIR *dir;
    dir = opendir(path.c_str());
    while ((ptr = readdir(dir)) != NULL)
    {
        if (ptr->d_name[0] == '.')
            continue;
        std::vector<std::string> strVec;
        SplitString(ptr->d_name, strVec, ".");

        if (strVec.size() >= 2)
        {
            std::string p;
            if (strVec[strVec.size() - 1].compare(ext) == 0)
                files.push_back(p.assign(path).append("/").append(ptr->d_name));
        }
        else
        {
            std::string p;
            readFileList(p.assign(path).append("/").append(ptr->d_name), files, ext);
        }
    }
    closedir(dir);
};
int YDGetPCDDataFilePath(std::string strFileDirectory,
                         std::vector<std::string> &pcdfiles)
{
    readFileList(strFileDirectory, pcdfiles, "pcd");
    return pcdfiles.size();
};
int YDGetYAMLFilePath(std::string strFileDirectory,
                         std::vector<std::string> &yamlfiles)
{
    readFileList(strFileDirectory, yamlfiles, "yaml");
    return yamlfiles.size();
}
int YDGetIMGFilePath(std::string strFileDirectory,
                         std::vector<std::string> &imgfiles)
{
    readFileList(strFileDirectory, imgfiles, "jpg");
    readFileList(strFileDirectory, imgfiles, "png");
    readFileList(strFileDirectory, imgfiles, "jpeg");
    readFileList(strFileDirectory, imgfiles, "bmp");
    readFileList(strFileDirectory, imgfiles, "JPG");
    return imgfiles.size();
}
void XYZtoXYfilter(pcl::PointCloud<pcl::PointXYZ> in_pc,pcl::PointCloud<pcl::PointXYZ> &out_pc)
{
    out_pc.clear();
    // Create a set of planar coefficients with X=Y=0,Z=1
    pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>(in_pc));
    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients());
    coefficients->values.resize(4);
    coefficients->values[0] = 0;
    coefficients->values[1] = 0;
    coefficients->values[2] = 1;
    coefficients->values[3] = 0;

    // Create the filtering object
    pcl::ProjectInliers<pcl::PointXYZ> proj;
    proj.setModelType(pcl::SACMODEL_PLANE);
    proj.setInputCloud(in_cloud_ptr);
    proj.setModelCoefficients(coefficients);
    proj.filter(out_pc);
}
void calib_xoy_wh(pcl::PointCloud<pcl::PointXYZ> xyz_in_xoy,float &withlength_in_xoy,float &heighlength_in_xoy)
{
    float minwith=99999.0f;
    float maxwith=-99999.0f;
    float minheigh=99999.0f;
    float maxheigh=-99999.0f;
    for(auto pt=xyz_in_xoy.begin();pt<xyz_in_xoy.end();pt++)
    {
        if(pt->x<minwith)
            minwith=pt->x;
        if(pt->x>maxwith)
            maxwith=pt->x;
        if(pt->y<minheigh)
            minheigh=pt->y;
        if(pt->y>maxheigh)
            maxheigh=pt->y;
    }
    withlength_in_xoy=abs(maxwith-minwith);
    heighlength_in_xoy=abs(maxheigh-minheigh);

}
void Point_Cloud_PassThrough_filter(std::string Axis, pcl::PointCloud<pcl::PointXYZ>::Ptr in_pc,
                                    float limit_min, float limit_max,
                                    pcl::PointCloud<pcl::PointXYZ> &out_pc)
{
    std::vector<int> inliers_indicies;
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud(in_pc);
    pass.setFilterFieldName(Axis);
    pass.setFilterLimits(limit_min, limit_max);
    pass.filter(inliers_indicies);
    pcl::copyPointCloud<pcl::PointXYZ>(*in_pc, inliers_indicies, out_pc);
}

string int2str(int num)
{
    ostringstream oss;
    if (oss << num)
    {
        string str(oss.str());
        return str;
    }
    else
    {
        cout << "[float2str] - Code error" << endl;
        exit(0);
    }
}

#endif
三个主要函数
# 1.输入地图路径，载入地图pcd
bool Init(std::string _mapfilepath);
# 2.输入标定文件路径，定位相机位姿
void locate_camera(std::string _yamlfilepath);
# 3.计算关注区域内点云集合
void calibrate(std::vector<cv::Point2i> roi_lt_rb_corner,
                   std::string savepath,std::string &roi_pc_path,
                   pcl::PointCloud<pcl::PointXYZ> &roi_pointcloud);
# 输入参数注释
std::vector<cv::Point2i> roi_lt_rb_corner//关注区域对角像素坐标，左上角，右下角
std::string savepath//保存数据根目录
std::string &roi_pc_path//关注区域内点云的保存路径
pcl::PointCloud<pcl::PointXYZ> &roi_pointcloud//关注区域内点云，点次序按照关注区域像素的行顺序存储，点云数量=像素宽度X像素高度，未找到对应点云的像素点对应的三维坐标为（0,0,0）

详细使用可参考main.cpp

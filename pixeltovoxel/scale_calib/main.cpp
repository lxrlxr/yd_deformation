#include "adjust.h"
using namespace cv;
using namespace std;
//./scale_calib /home/wxy/study/calib/scale_calib/data/map.pcd
int main(int argc, char **argv)
{
    std::string mapfilepath = "/home/lxr/workdir/yd_deformation/database/pointcloud/0.02.pcd";
    std::string yamlfile = "/home/lxr/workdir/yd_deformation/database/4-7-17320001.yaml";
    std::vector<cv::Point2i> roi_lt_rb_corner;
    roi_lt_rb_corner.push_back(cv::Point2i(1532, 82));
    roi_lt_rb_corner.push_back(cv::Point2i(1635, 216));
    std::string outrootpath = "/home/lxr/workdir/yd_deformation/database";
    std::string roi_pc_path;
    std::vector<std::vector<float>> outpoints;

    Adjust adjustor;
    if (!adjustor.Init(mapfilepath))
    {
        return -1;
    }
    adjustor.locate_camera(yamlfile);
    adjustor.calibrate(roi_lt_rb_corner, outpoints);

    std::cout << "result roi_pc_path: " << roi_pc_path << endl;

    for (int i = 0; i < 20; i++)
    {
        std::vector<float> output = adjustor.get_distortion_point(400., 500.);
    }
    // pcl::io::savePCDFile(roi_pc_path, roi_pointcloud);
    return 0;
}
#include "adjust.h"
#include "tools.h"
bool Adjust::Init(std::string _mapfilepath)
{
    if (pcl::io::loadPCDFile(_mapfilepath, pc) == -1)
    {
        PCL_ERROR("Couldn't read Velodyne pcd files\n");
        return false;
    }
    return true;
}
void Adjust::locate_camera(std::string _yamlfilepath)
{
    color_lidar_yamlfilepath = _yamlfilepath;
    ReadCalibrationFile(color_lidar_yamlfilepath, color_lidar_exRT, colorprojection_matrix, colordistortion_matrix, colorimagesize);
    UpdateXYZ(pc, pc_in_cam, color_lidar_exRT);
}
void Adjust::calibrate(std::vector<cv::Point2i> roi_lt_rb_corner,
                       std::vector<std::vector<float>> &outpoints, int erode_pixels)
{
    std::cout << "calibrate start  colorprojection_matrix.shape:" << colorprojection_matrix.size().width << "    y:" << colorprojection_matrix.size().height << std::endl;

    double dur;
    clock_t start, end;
    start = clock();

    cv::Point2i left_top_p = roi_lt_rb_corner[0];
    cv::Point2i right_bottom_p = roi_lt_rb_corner[1];
    std::vector<cv::Point2f> undiscorner, discorner;
    discorner.push_back(cv::Point2f(left_top_p.x, left_top_p.y));
    cv::Mat unused;
    cv::undistortPoints(discorner, undiscorner, colorprojection_matrix, colordistortion_matrix, unused, colorprojection_matrix);

    int roi_corner_x = undiscorner[0].x;
    int roi_corner_y = undiscorner[0].y;
    int roi_with = abs(right_bottom_p.x - left_top_p.x);
    int roi_heigh = abs(right_bottom_p.y - left_top_p.y);
    cv::Mat projection_matrix;
    colorprojection_matrix.copyTo(projection_matrix);
    projectpointcloud(pc_in_cam, projection_matrix,
                      cv::Rect(roi_corner_x, roi_corner_y, roi_with, roi_heigh), outpoints, left_top_p, erode_pixels);
    end = clock();
    dur = (double)(end - start);
    printf(" uses Time:%f s\n", (dur / CLOCKS_PER_SEC));
}

void Adjust::projectpointcloud(pcl::PointCloud<pcl::PointXYZRGB> point_cloud,
                               cv::Mat projection_matrix,
                               cv::Rect frame,
                               std::vector<std::vector<float>> &outpoints, cv::Point2i left_top_p, int erode_pixels)
{
    cv::Mat range_image = cv::Mat(frame.height, frame.width, CV_32F, numeric_limits<float>::max());
    int cornerx = frame.x;
    int cornery = frame.y;
    projection_matrix.convertTo(projection_matrix, CV_32FC1);

    float fx = projection_matrix.at<float>(0, 0);
    float fy = projection_matrix.at<float>(1, 1);
    float cx = projection_matrix.at<float>(0, 2);
    float cy = projection_matrix.at<float>(1, 2);
    for (auto pt = point_cloud.begin();
         pt < point_cloud.end(); pt++)
    {
        if (pt->z < 0)
            continue;
        float u = pt->x * fx / pt->z + cx;
        float v = pt->y * fy / pt->z + cy;
        cv::Point xy(u, v);
        if (xy.inside(frame))
        {
            float range = sqrt(pow(pt->x, 2) + pow(pt->y, 2) + pow(pt->z, 2));
            int col = round(u), row = round(v);
            if (range < range_image.at<float>(col - cornerx, row - cornery))
            {
                range_image.at<float>(col - cornerx, row - cornery) = range; //给每一像素点增加最小的深度值，构建深度图
            }
        }
    }
    // int erode_pixels = 6; //这个值越大，去除遮挡力度越大
    cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT,
                                                cv::Size(2 * erode_pixels + 1, 2 * erode_pixels + 1));
    cv::Mat eroded = cv::Mat();
    cv::erode(range_image, eroded, element);

    int iIndex = 0;
    for (auto pt = point_cloud.begin();
         pt < point_cloud.end(); pt++)
    {
        if (pt->z < 0)
            continue;
        float u = pt->x * fx / pt->z + cx;
        float v = pt->y * fy / pt->z + cy;
        cv::Point xy(u, v);
        if (xy.inside(frame))
        {
            float range = sqrt(pow(pt->x, 2) + pow(pt->y, 2) + pow(pt->z, 2));
            int row = round(u), col = round(v);
            if (range > eroded.at<float>(row - cornerx, col - cornery))
            {
                continue;
            }
            else
            {
                std::vector<float> outpoint;
                outpoint.push_back(u);
                outpoint.push_back(v);
                outpoint.push_back(pt->x);
                outpoint.push_back(pt->y);
                outpoint.push_back(pt->z);
                outpoints.push_back(outpoint);
                iIndex++;
            }
        }
    }
    std::cout << "height:" << frame.height << endl;
    std::cout << "width:" << frame.width << endl;
    std::cout << "outpoints.size:" << outpoints.size() << endl;
}
void Adjust::UpdateXYZ(pcl::PointCloud<pcl::PointXYZRGB> &in_pc, pcl::PointCloud<pcl::PointXYZRGB> &out_pc,
                       cv::Mat in_color_lidar_exRT)
{
    cv::Mat color_lidar_exRT = in_color_lidar_exRT.clone();
    cv::Mat color_lidar_exRT_inv = cv::Mat::zeros(4, 4, CV_64F);
    Eigen::Matrix4d transform;
    Eigen::Affine3d temp;
    cv::cv2eigen(color_lidar_exRT, transform);
    temp.matrix() = transform;
    transform = temp.inverse().matrix();
    cv::eigen2cv(transform, color_lidar_exRT_inv);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr transformed_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
    pcl::transformPointCloud(in_pc, out_pc, transform);
}

std::vector<float> Adjust::get_distortion_point(float x, float y)
{
    std::vector<cv::Point2f> undiscorner, discorner;
    discorner.push_back(cv::Point2f(x, y));
    cv::Mat unused;
    // std::cout << "x:" << x << "    y:" << y << std::endl;
    // std::cout << "colorprojection_matrix.shape:" << colorprojection_matrix.size().width << "    y:" << colorprojection_matrix.size().height << std::endl;
    cv::undistortPoints(discorner, undiscorner, colorprojection_matrix, colordistortion_matrix, unused, colorprojection_matrix);
    std::vector<float> output;
    output.push_back(undiscorner[0].x);
    output.push_back(undiscorner[0].y);
    // std::cout << "output x:" << undiscorner[0].x << "   output y:" << undiscorner[0].y << std::endl;
    return output;
}
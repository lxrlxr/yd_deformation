import sys
import math
import traceback

sys.path.append("/home/lxr/workdir/yd_deformation/pixeltovoxel/lib")


def cal_scale(point1, point2):
    pixel_len = (point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2
    pixel_len = math.sqrt(pixel_len)
    voxel_len = (point1[2] - point2[2]) ** 2 + (point1[3] - point2[3]) ** 2 + (point1[4] - point2[4]) ** 2
    voxel_len = math.sqrt(voxel_len)
    scale = voxel_len / pixel_len
    return scale


try:
    import py3d

    pcd_path = "/home/lxr/workdir/yd_deformation/database/pointcloud/0.02.pcd"
    yaml_path = "/home/lxr/workdir/yd_deformation/database/4-7-17320001.yaml"
    adjustor = py3d.Init(pcd_path)

    out = py3d.get_3d_mat(adjustor, yaml_path, 1532, 82, 1635, 216)
    scale = cal_scale(out[1], out[2])
    sorted(out, key=lambda k: out[0], reverse=True)
    a = 21
except:
    traceback.print_exc()

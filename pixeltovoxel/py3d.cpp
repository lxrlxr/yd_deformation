#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <sys/time.h>
#include "time.h"
#include "scale_calib/adjust.h"
using namespace cv;
using namespace std;

namespace py = pybind11;

Adjust Init(string strMapPath)
{
    Adjust adjustor;
    adjustor.Init(strMapPath);
    cout << "init pd3d sucess" << endl;
    return adjustor;
}

std::vector<std::vector<float>> get_3d_mat(Adjust adjustor, string strYamlPath, int iLx, int iLy, int iRx, int iRy, std::vector<std::vector<float>> &inputs, int erode_pixels)
{
    struct timeval t1, t2;
    double timeuse;
    gettimeofday(&t1, NULL);

    std::vector<std::vector<float>> outpoints;

    adjustor.locate_camera(strYamlPath);

    std::vector<cv::Point2i> roi_lt_rb_corner;
    roi_lt_rb_corner.push_back(cv::Point2i(iLx, iLy));
    roi_lt_rb_corner.push_back(cv::Point2i(iRx, iRy));

    adjustor.calibrate(roi_lt_rb_corner, outpoints, erode_pixels);

    cout << "inputs.size():" << inputs.size() << endl;
    for (int i = 0; i < inputs.size(); i++)
    {
        float x = inputs[i][0];
        float y = inputs[i][1];
        std::vector<float> output = adjustor.get_distortion_point(x, y);
        outpoints.push_back(output);
    }

    gettimeofday(&t2, NULL);
    timeuse = (t2.tv_sec - t1.tv_sec) + (double)(t2.tv_usec - t1.tv_usec) / (1000 * 1000);
    cout << "compute 3d imformation cost time(s):" << timeuse << endl;

    return outpoints;
}

PYBIND11_MODULE(py3d, m)
{
    m.doc() = "py3d";
    pybind11::class_<Adjust>(m, "Adjust")
        .def(pybind11::init(), py::return_value_policy::reference);
    m.def("Init", &Init, py::return_value_policy::reference);
    m.def("get_3d_mat", &get_3d_mat, py::return_value_policy::reference);
}
